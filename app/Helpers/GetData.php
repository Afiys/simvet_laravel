<?php

namespace App\Helpers;
 
/**
 *  Name         : Get Data From Database.
 *  Description  : This class for Get Data setting and count.
 *  @copyright   : Badri Zaki
 *  @version     : 1.8
 *  @author      : Badri Zaki - badrizaki@gmail.com
**/

abstract class GetData
{
	static private $setting;
	static private $count;
	static private $data;

    public static function setting()
    {
        if (!self::$setting)
        {
        	if (!self::$data)
        	{
        		self::$data = new \stdClass();
            	self::$data->count = [];
        	}

            $settingData = \App\Models\Setting::where('show', 1)->orderBy('order', 'ASC');
            $settingData = $settingData->get();
            if ($settingData)
            {
                $settingData = $settingData->toArray();
                foreach ($settingData as $key => $value)
                {
                	$name = $value['key'];
                    self::$data->$name = $value;
                }
            }
        	self::$setting = true;
        }
        return self::$data;
    }

    public static function count()
    {
        if (!self::$count)
        {
        	if (!self::$data)
        	{
        		self::$data = new \stdClass();
            	self::$data->count = [];
        	}

            $totalVeteran = new \App\Models\Veteran();
            $totalVeteran = $totalVeteran->count();
            self::$data->veteran = $totalVeteran;
        	self::$count = true;
        }
        return self::$data;
    }
}