<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class AuthRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  \Role  $roles
     * @return mixed
     * on route Route::post('url', 'Controller@method')->middleware('AuthRole:SUP,MKT');
     */
    public function handle($request, Closure $next, ...$roles)
    {
        $this->Auth = new \Auth();
        $this->Config = new \Config();
        // print_r($roles);die;
        if (in_array('ALL', $roles))
        {
            return $next($request);
        }
        else {
            if (isset($this->Auth::user()->roleCode) && is_array($roles))
            {
                if (in_array('ALL', $roles))
                {
                    return $next($request);
                }
                else {
                    if (!in_array($this->Auth::user()->roleCode, $roles)) return Redirect::to('/');
                }
            }
            else {
                if (in_array('ALL', $roles))
                {
                    return $next($request);
                }
                else {
                    return Redirect::to('/');
                }
            }
        }
        return $next($request);
    }
}