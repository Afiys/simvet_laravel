<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Customer;
use App\Mail\Register;
use Mail;
use Config;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['customerType'] = '';
        if (isset($data['isShipper']) || isset($data['isCarrier']))
            $data['customerType'] = '1';

        return Validator::make($data, [
            'name'          => 'required|string|max:50',
            'company'       => 'required|string',
            'typeOfCompany' => 'required|string',
            'other'         => 'required_if:typeOfCompany,OTHERS',
            // 'company' => 'required|string|unique:customers,name',
            'email'         => 'required|string|email|max:50|unique:users|confirmed',
            'username'      => 'required|string|max:50|unique:users',
            'password'      => 'required|string|min:6|confirmed',
            'customerType'  => 'required',
            'accept_terms'  => 'required',
            'g-recaptcha-response' => 'required',
        ],
        [
            'customerType.required' => 'Please select shipper, carrier or both',
            'g-recaptcha-response.required' => 'Captcha is required',
        ]
        );
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $data['isShipper']      = isset($data['isShipper'])?$data['isShipper']:'0';
        $data['isCarrier']      = isset($data['isCarrier'])?$data['isCarrier']:'0';
        $data['companyCode']    = generateCustomerCode($data['company']);
        $data['typeOfCompany']  = $data['typeOfCompany'];
        $data['otherType']      = isset($data['other'])?$data['other']:'';

        $result = Customer::create([
            'customerCode'  => $data['companyCode'],
            'name'          => $data['company'],
            'isShipper'     => $data['isShipper'],
            'isCarrier'     => $data['isCarrier'],
            'typeOfCompany' => $data['typeOfCompany'],
            'otherType'     => $data['otherType'],
        ]);

        $customerId = $result->customerId;
        $result = User::create([
            'customerId' => $customerId,
            'name' => $data['name'],
            'username' => $data['username'],
            'email' => $data['email'],
            'role' => Config::get('role.member.code'),
            'emailConfirmed' => 1,
            'password' => bcrypt($data['password']),
        ]);
        Mail::to($data['email'])->send(new Register($data));

        return $result;
    }
}