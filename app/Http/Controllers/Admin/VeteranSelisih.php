<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class VeteranSelisih extends BaseController
{
    protected $redirectTo = 'veteranselisih'; // REDIRECT URL
    protected $redirectIndex = 'veteranselisih.index'; // View veteran
    protected $redirectEditable = 'veteranselisih.editable'; // vied add and update

    /**
    * Send data to view
    */
    protected function getList()
    {
        $data = [];
        $data['kodam'] = \App\Models\Kodam::get();
        $data['kodim'] = \App\Models\Kodim::get();

        $selectedId = [];
        $veteran = \App\Models\Veteran::select('idVeteran')->limit(1000)->get();
        if ($veteran)
        {
            $veteran = $veteran->pluck('idVeteran')->toArray();
            $selectedId = $veteran;
        }
        $data['veteran'] = json_encode($selectedId, true);
        return $data;
    }

    /**
    * list ajax for datatables
    */
    protected function listAjax(Request $request)
    {
        $item = new \App\Models\Veteran(); 
        $item = $item->leftJoin('provinsi', 'provinsi.idProvinsi', 'veteran.idProvinsi');
        $item = $item->leftJoin('kabupaten', 'kabupaten.idKabupaten', 'veteran.idKabupaten');
        $item = $item->leftJoin('kodam', 'kodam.idKodam', 'veteran.idKodam');
        $item = $item->leftJoin('kodim', 'kodim.idKodim', 'veteran.idKodim');
        $item = $item->select(['veteran.*', 'provinsi.namaProvinsi', 'kabupaten.namaKabupaten', 'kodam.namaKodam', 'kodim.namaKodim']);
        $item = $item->orderBy('veteran.npvBaru', 'ASC');
        // $item = $item->where('tanggalMeninggal', NULL);
        /*$item = $item->where('veteran.idProvinsi', '=', '');
        $item = $item->where('veteran.idKabupaten', '=', '');
        $item = $item->where('veteran.idKodam', '=', '');
        $item = $item->where('veteran.idKodim', '=', '');*/
        
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.idProvinsi', '=', '')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.idKabupaten', '=', '')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.idKodam', '=', '')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.idKodim', '=', '')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.predikat', '=', '')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.jenisKelamin', '=', '')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.jenisKelamin', '=', '-')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.tanggalLahir', '=', '')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        $item = $item->orWhere(function ($query)
        {
            $query->where('veteran.tanggalLahir', '=', '0000-00-00')
            ->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        });
        // $item = $item->orWhere('veteran.idProvinsi', '=', '');
        // $item = $item->orWhere('veteran.idKabupaten', '=', '');
        // $item = $item->orWhere('veteran.idKodam', '=', '');
        // $item = $item->orWhere('veteran.idKodim', '=', '');
        // $item = $item->orWhere('veteran.predikat', '=', '');
        // $item = $item->orWhere('veteran.jenisKelamin', '=', '');
        // $item = $item->orWhere('veteran.jenisKelamin', '=', '-');
        // $item = $item->orWhere('veteran.tanggalLahir', '=', '');
        // $item = $item->orWhere('veteran.tanggalLahir', '=', '0000-00-00');
        // $item = $item->where('veteran.tanggalMeninggal', '=', '0000-00-00');
        // $item = $item->where('veteran.idProvinsi', '=', '')->orWhereNull('idProvinsi');
        /*
        idProvinsi
        idKabupaten
        idKodam
        idKodim
        */
        return Datatables::of($item)
            ->filter(function ($query) use ($request)
            {
                if ($request->has('customSearch'))
                {
                    $params = parseDataSearch($request->customSearch);
                    $idKodam = isset($params['kodam']) ? $params['kodam'] :'';
                    if ($idKodam != "")
                        $query->where('veteran.idKodam', $idKodam);

                    $idKodim = isset($params['kodim']) ? $params['kodim'] :'';
                    if ($idKodim != "")
                        $query->where('veteran.idKodim', $idKodim);
                }
            })
            ->addColumn('namaProvinsi', function ($item)
            {
                $check = $this->Config::get('text.'.strtoupper($item->namaProvinsi), $item->namaProvinsi);;
                return $check;
            })
            ->addColumn('check', function ($item)
            {
                $check = '<input type="checkbox" name="check" class="checkRow" onChange="selectRow(this, \''.$item->idVeteran.'\')">';
                return $check;
            })
            ->addColumn('predikat', function ($item)
            {
                $predikat = $this->Config::get('app.pembela.'.$item->predikat, $item->predikat);;
                return $predikat;
            })
            ->addColumn('rh', function ($item)
            {
                $action = '-';

                if (in_array('ALL', $this->Config::get('menu.role.veteran.rh')))
                {
                    $action = '<a href="'.route('veteran.index').'/'.$item->idVeteran.'/rh" class="btn btn-xs btn-primary"><i class="fa fa-search-plus" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.veteran.rh')))
                    {
                        $action = '<a href="'.route('veteran.index').'/'.$item->idVeteran.'/rh" class="btn btn-xs btn-primary"><i class="fa fa-search-plus" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;';
                    }
                }
                return $action;
            })
            ->addColumn('action', function ($item)
            {
                $action = '';
                if (in_array('ALL', $this->Config::get('menu.role.veteran.update')))
                {
                    $action .= '<a href="'.route('veteran.index').'/'.$item->idVeteran.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.veteran.update')))
                    {
                        $action .= '<a href="'.route('veteran.index').'/'.$item->idVeteran.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                    }
                }
                // $action .= '<a href="'.route('veteran.index').'/'.$item->idVeteran.'" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';

                if (in_array('ALL', $this->Config::get('menu.role.veteran.destroy')))
                {
                    $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idVeteran.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.veteran.destroy')))
                    {
                        $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idVeteran.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                    }
                }
                if ($action == '') $action = '-';

                return $action;
            })
            ->addColumn('jenisKelamin', function ($item)
            {
                $jenisKelamin = $this->Config::get('app.jenisKelamin.'.$item->jenisKelamin, '');
                return $jenisKelamin;
            })
            ->addColumn('usia', function ($item)
            {
                $usia = getAge($item->tanggalLahir);
                return $usia.' Tahun';
            })
            ->editColumn('id', 'ID: {{$idVeteran}}')
            ->rawColumns(['rh', 'action', 'check']) // raw show html
            ->make(true);
    }

    protected function getLookUp()
    {
        $data['kodam'] = \App\Models\Kodam::get();
        return $data; // data
    }

    protected function find($id)
    {
        return \App\Models\Veteran::find($id);
    }

    public function rh(Request $request, $id)
    {
        $this->id       = $id;
        $item           = $this->find($id);
        $this->item     = $item;
        $lookupTable    = $this->getLookUp();
        if ($id > 0)
        {
            if (isset($item) && is_array($item) && count($item) == 0)
            {
                return Redirect::to($this->redirectPath());
            }
        }
        return view('veteran.rh', compact('item', 'id', 'lookupTable'));
    }

    protected function validateData(Request $request, $id)
    {
        /*return Validator::make($request->all(), [
            // 'namaVeteran' => 'required|string',
            // 'image' => 'mimetypes:image/jpeg,image/jpg,,image/png',
            // 'npvBaru' => 'required|string|unique:veteran,npvBaru,'.$request->id.',idVeteran',// unique with value $datavalue
        ],
        [
            // 'namaVeteran.required'   => 'Nama tidak boleh kosong',
            // 'npvBaru.required'   => 'NPV Baru tidak boleh kosong',
            // 'npvBaru.unique' => 'NPV Baru sudah ada/salah',
        ]);*/
        /*if($id > 0) return null;*/
        /*return Validator::make($request->all(), [
            'image'         => 'required|mimetypes:image/jpeg,image/jpg',
            'sample1'       => 'required_without_all:sample2|numeric',
            'sample2'       => 'required_without_all:sample1|numeric',
            'string'        => 'required|string',
            'numeric'       => 'required|numeric|max:100',
            'nullable'      => 'nullable|numeric',
            'uniqueField'   => 'nullable|max:20|unique:nametables',
            'email'         => 'required|string|email|max:50|unique:users,email,dataValue',// unique with value $datavalue
            'password'      => 'required|string|min:6|confirmed', // with field name password_confirm
        ],
        [
            // custome message
            'numeric.required'   => 'Please fill numeric',
            'uniqueField.unique' => 'Must unique',
        ]);*/
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\Veteran();
        
        if($id > 0)
        {
            $item = $item->find($id);
            $item->userIdUpdated = $this->Auth::user()->id;
        }
        else {
            $item->userIdCreated = $this->Auth::user()->id;
        }

        if ($request->image)
        {
            $image          = $request->image;
            $imageName      = $request->npvBaru.'.'.$image->getClientOriginalExtension();
            // $imageName      = time().$image->getClientOriginalName();
            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $destinationPath= $this->Config::get('app.directory.veteran');
            $result         = $image->move($destinationPath, $imageName);
            $imageUrl       = $destinationPath."/".$imageName;
            $item->imageUrl = $imageUrl;
        }
        else if ($request->fileBrowseMedia && $request->fileBrowseMedia != '')
        {
            $item->imageUrl = $request->fileBrowseMedia;
        }

		$item->idProvinsi = $request->idProvinsi;
		$item->idKabupaten = $request->idKabupaten;
		$item->idKodam = $request->idKodam;
		$item->idKodim = $request->idKodim;
		$item->namaVeteran = $request->namaVeteran;
		$item->tempatLahir = $request->tempatLahir;
		$item->tanggalLahir = formatDate($request->tanggalLahir, 'Y-m-d');
		$item->jenisKelamin = $request->jenisKelamin;
        $item->NIK = $request->NIK;
		$item->NIP = $request->NIP;
		$item->noPendaftaran = $request->noPendaftaran;
		$item->tanggalPendaftaran = formatDate($request->tanggalPendaftaran, 'Y-m-d');
		$item->npvLama = $request->npvLama;
		$item->npvBaru = $request->npvBaru;
		$item->noKeputusan = $request->noKeputusan;
		$item->tanggalKeputusan = formatDate($request->tanggalKeputusan, 'Y-m-d');
		$item->predikat = $request->predikat;
		$item->golongan = $request->golongan;
		$item->tahunMasaBakti = $request->tahunMasaBakti;
		$item->bulanMasaBakti = $request->bulanMasaBakti;
        $item->tanggalMeninggal = $request->tanggalMeninggal;

        if (isset($request->statusMeninggal) && $request->statusMeninggal != "")
        {
            $item->statusMeninggal = strtolower($request->statusMeninggal);
        }
        else {
            if (isset($request->tanggalMeninggal) && $request->tanggalMeninggal != '')
              $item->statusMeninggal = 'Ya';
            else
              $item->statusMeninggal = 'Tidak';
        }

		$item->namaAhliWaris = $request->namaAhliWaris;
		$item->tanggalLahirAhliWaris = formatDate($request->tanggalLahirAhliWaris, 'Y-m-d');
		$item->NIKAhliWaris = $request->NIKAhliWaris;
		$item->tanggalMeninggalAhliWaris = formatDate($request->tanggalMeninggalAhliWaris, 'Y-m-d');
		$item->alamat = $request->alamat;
		$item->rt = $request->rt;
		$item->rw = $request->rw;
		$item->kelurahan = $request->kelurahan;
		$item->kecamatan = $request->kecamatan;
		$item->noKepTuvet = $request->noKepTuvet;
		$item->tglKepTuvet = $request->tglKepTuvet;
		$item->noKepDahor = $request->noKepDahor;
		$item->tglKepDahor = $request->tglKepDahor;
		$item->noKepTundayatu = $request->noKepTundayatu;
		$item->tglKepTundayatu = $request->tglKepTundayatu;
        $item->nomorRekening = $request->nomorRekening;
		$item->tempatPembayaran = $request->tempatPembayaran;
        $item->noKarip = $request->noKarip;
		$item->PTTaspen = $request->PTTaspen;
        $item->keterangan = $request->keterangan;
        $item->save();
    }

    public function downloadSample()
    {
        $data = $this->Config::get('sampleExcel', '');

        $dataBabinminvetcaddam = [];
        $babinminvetcaddam = \App\Models\Kodam::get();
        if ($babinminvetcaddam)
        {
            $babinminvetcaddam = $babinminvetcaddam->toArray();
            foreach ($babinminvetcaddam as $key => $value)
            {
                $dataBabinminvetcaddam[$key]['ID'] = $value['idKodam'];
                $dataBabinminvetcaddam[$key]['BABINMINVETCADDAM'] = $value['namaKodam'];
            }
        }

        $dataKanminvetcad = [];
        $kanminvetcad = new \App\Models\Kodim;
        $kanminvetcad = $kanminvetcad->leftJoin('kodam', 'kodam.idKodam', 'kodim.idKodam');
        $kanminvetcad = $kanminvetcad->select('kodim.*', 'kodam.namaKodam', 'kodam.idKodam');
        $kanminvetcad = $kanminvetcad->get();
        if ($kanminvetcad)
        {
            $kanminvetcad = $kanminvetcad->toArray();
            foreach ($kanminvetcad as $key => $value)
            {
                $dataKanminvetcad[$key]['ID KANMINVETCAD'] = $value['idKodim'];
                $dataKanminvetcad[$key]['KANMINVETCAD'] = $value['namaKodim'];
                $dataKanminvetcad[$key]['ID BABINMINVETCADDAM'] = $value['idKodam'];
                $dataKanminvetcad[$key]['BABINMINVETCADDAM'] = $value['namaKodam'];
            }
        }

        $dataPropinsi = [];
        $propinsi = \App\Models\Provinsi::get();
        if ($propinsi)
        {
            $propinsi = $propinsi->toArray();
            foreach ($propinsi as $key => $value)
            {
                $dataPropinsi[$key]['ID'] = $value['idProvinsi'];
                $dataPropinsi[$key]['PROPINSI'] = $value['namaProvinsi'];
            }
        }

        $dataKabupaten = [];
        $kabupaten = new \App\Models\Kabupaten;
        $kabupaten = $kabupaten->leftJoin('provinsi', 'kabupaten.idProvinsi', 'provinsi.idProvinsi');
        $kabupaten = $kabupaten->select('kabupaten.*', 'provinsi.namaProvinsi', 'provinsi.idProvinsi');
        $kabupaten = $kabupaten->get();
        if ($kabupaten)
        {
            $kabupaten = $kabupaten->toArray();
            foreach ($kabupaten as $key => $value)
            {
                $dataKabupaten[$key]['ID KABUPATEN'] = $value['idKabupaten'];
                $dataKabupaten[$key]['KABUPATEN'] = $value['namaKabupaten'];
                $dataKabupaten[$key]['ID PROPINSI'] = $value['idProvinsi'];
                $dataKabupaten[$key]['PROPINSI'] = $value['namaProvinsi'];
            }
        }

        return Excel::create('Sample Data Veteran', function($excel) use ($data, $dataBabinminvetcaddam, $dataKanminvetcad, $dataPropinsi, $dataKabupaten)
        {
            $excel->sheet('Data Veteran', function($sheet) use ($data)
            {
                $sheet->fromArray($data);
            });
            $excel->sheet('Data Babinminvetcaddam', function($sheet) use ($dataBabinminvetcaddam)
            {
                $sheet->fromArray($dataBabinminvetcaddam);
            });
            $excel->sheet('Data Kanminvetcad', function($sheet) use ($dataKanminvetcad)
            {
                $sheet->fromArray($dataKanminvetcad);
            });
            $excel->sheet('Data Propinsi', function($sheet) use ($dataPropinsi)
            {
                $sheet->fromArray($dataPropinsi);
            });
            $excel->sheet('Data Kabupaten', function($sheet) use ($dataKabupaten)
            {
                $sheet->fromArray($dataKabupaten);
            });
        })->download('xlsx');
    }

    public function import(Request $request)
    {
        return $this->import2($request);die;

        ini_set('max_execution_time', '300');
        if($request->hasFile('fileExcel'))
        {
            $path = $request->fileExcel->getRealPath();
            // $path = $request->file('fileExcel')->getRealPath();
            $data = Excel::load($path, function($reader) {})->formatDates( true, 'Y-m-d' )->get();
            if(!empty($data) && $data->count())
            {
                $data = $data->toArray();
                $sheetData = $data[0];
                foreach ($sheetData as $key => $value)
                {
                    if(!empty($value))
                    {
                        if (isset($value['tanggal_lahir']) && strlen($value['tanggal_lahir']) == 4)
                            $value['tanggal_lahir'] = '01-01-'.$value['tanggal_lahir'];

                        $kabupaten = isset($value['kabupaten']) ? $value['kabupaten'] :'';
                        if (!is_numeric($kabupaten))
                        {
                            $kabupaten = str_replace('Kab.', '', trim($kabupaten));
                            $kab = \App\Models\Kabupaten::where('namaKabupaten', 'like', '%'.$kabupaten.'%')->get()->first();
                            if ($kab)
                            {
                                $kab = $kab->toArray();
                                $kabupaten = isset($kab['idKabupaten'])?$kab['idKabupaten']:'';
                            }
                        }

                        $propinsi = isset($value['propinsi']) ? trim($value['propinsi']) :'';
                        if (!is_numeric($propinsi))
                        {
                            if (strtoupper($propinsi) == 'A C E H')
                                $propinsi = str_replace(' ', '', trim($propinsi));

                            $prov = \App\Models\Provinsi::where('namaProvinsi', 'like', '%'.$propinsi.'%')->get()->first();
                            if ($prov)
                            {
                                $prov = $prov->toArray();
                                $propinsi = isset($prov['idProvinsi'])?$prov['idProvinsi']:'';
                            }
                        }

                        /*if (isset($value['jenis_kelamin']))
                        {
                            if (strtoupper($value['jenis_kelamin']) == 'L') {
                                $value['jenis_kelamin'] = '1';
                            }
                            elseif (strtoupper($value['jenis_kelamin']) == 'P') {
                                $value['jenis_kelamin'] = '2';
                            }
                        }*/

                        $insert[] = [
                            'namaVeteran' => isset($value['nama'])?$value['nama']:'',
                            'tempatLahir' => isset($value['tempat_lahir'])?$value['tempat_lahir']:'',
                            'tanggalLahir' => isset($value['tanggal_lahir'])?formatDate($value['tanggal_lahir'], 'Y-m-d'):'',
                            'jenisKelamin' => isset($value['jenis_kelamin'])?$value['jenis_kelamin']:'',
                            'NIK' => isset($value['nik'])?$value['nik']:'',
                            'noPendaftaran' => isset($value['nomor_pendaftran'])?$value['nomor_pendaftran']:'',
                            'tanggalPendaftaran' => isset($value['tanggal_pendaftaran'])?formatDate($value['tanggal_pendaftaran'], 'Y-m-d'):'',
                            'npvLama' => isset($value['npv_lama'])?$value['npv_lama']:'',
                            'npvBaru' => isset($value['npv_baru'])?$value['npv_baru']:'',
                            'noKeputusan' => isset($value['nomor_surat_keputusan'])?$value['nomor_surat_keputusan']:'',
                            'tanggalKeputusan' => isset($value['tanggal_surat_keputusan'])?formatDate($value['tanggal_surat_keputusan'], 'Y-m-d'):'',
                            'predikat' => isset($value['predikat'])?$value['predikat']:'',
                            'golongan' => isset($value['golongan'])?$value['golongan']:'',
                            'tahunMasaBakti' => isset($value['masa_bakti_tahun'])?$value['masa_bakti_tahun']:'',
                            'bulanMasaBakti' => isset($value['masa_bakti_bulan'])?$value['masa_bakti_bulan']:'',
                            'tanggalMeninggal' => isset($value['tanggal_meninggal'])?formatDate($value['tanggal_meninggal'], 'Y-m-d'):'',
                            'statusMeninggal' => (isset($value['tanggal_meninggal']) && $value['tanggal_meninggal'] != '')?'Ya':'Tidak',
                            'namaAhliWaris' => isset($value['nama_ahli_waris'])?$value['nama_ahli_waris']:'',
                            'tanggalLahirAhliWaris' => isset($value['tgl_lahir_ahli_waris'])?formatDate($value['tgl_lahir_ahli_waris'], 'Y-m-d'):'',
                            'NIKAhliWaris' => isset($value['nik_ahli_waris'])?$value['nik_ahli_waris']:'',
                            'tanggalMeninggalAhliWaris' => isset($value['tanggal_meninggal_ahli_waris'])?formatDate($value['tanggal_meninggal_ahli_waris'], 'Y-m-d'):'',

                            'alamat' => isset($value['jalan'])?$value['jalan']:'',
                            'rt' => isset($value['rt'])?$value['rt']:'',
                            'rw' => isset($value['rw'])?$value['rw']:'',
                            'kelurahan' => isset($value['desakelurahan'])?$value['desakelurahan']:'',
                            'kecamatan' => isset($value['kecamatan'])?$value['kecamatan']:'',
                            'noKepTuvet' => isset($value['no_kep_tuvet'])?$value['no_kep_tuvet']:'',
                            'tglKepTuvet' => isset($value['tgl_kep_tuvet'])?formatDate($value['tgl_kep_tuvet'], 'Y-m-d'):'',
                            'noKepDahor' => isset($value['no_kep_dahor'])?$value['no_kep_dahor']:'',
                            'tglKepDahor' => isset($value['tgl_kep_dahor'])?formatDate($value['tgl_kep_dahor'], 'Y-m-d'):'',
                            'noKepTundayatu' => isset($value['no_kep_tundayatu'])?$value['no_kep_tundayatu']:'',
                            'tglKepTundayatu' => isset($value['tgl_kep_tundayatu'])?formatDate($value['tgl_kep_tundayatu'], 'Y-m-d'):'',
                            'nomorRekening' => isset($value['no_rekening'])?$value['no_rekening']:'',
                            'noKarip' => isset($value['no_karip'])?$value['no_karip']:'',
                            'PTTaspen' => isset($value['pt_taspen'])?$value['pt_taspen']:'',
                            'keterangan' => isset($value['keterangan'])?$value['keterangan']:'',
                            'userId' => isset($this->Auth::user()->id)?$this->Auth::user()->id:'',
                            'userIdCreated' => isset($this->Auth::user()->id)?$this->Auth::user()->id:'',
                            'imageUrl' => isset($value['npv_baru'])?$value['npv_baru'].'.jpg':'',

                            // RELATION
                            'idKodam' => isset($value['babinminvetcaddam'])?$value['babinminvetcaddam']:'',
                            'idKodim' => isset($value['kanminvetcad'])?$value['kanminvetcad']:'',
                            'idKabupaten' => isset($kabupaten)?$kabupaten:'',
                            'idProvinsi' => isset($propinsi)?$propinsi:'',
                        ];
                    }
                }

                if(!empty($insert))
                {
                    \App\Models\Veteran::insert($insert);

                    Session::flash('message', 'Selamat, Import data Veteran berhasil.');
                    return back()->with('success','Import data Veteran berhasil.');
                }
            }
        }
        return back()->with('error','Please Check your file, Something is wrong there.');
    }

    public function import2(Request $request)
    {
        ini_set('max_execution_time', 0);
        if($request->hasFile('fileExcel'))
        {
            $path = $request->fileExcel->getRealPath();
            // $path = $request->file('fileExcel')->getRealPath();
            // $data = Excel::load($path, function($reader) {})->formatDates( true, 'Y-m-d' )->get();

            Excel::filter('chunk')->load($path)->chunk(5, function($results)
            {
                if ($results)
                {
                    $results = $results->toArray();
                    $sheetVeteran = $results[0];
                    foreach($sheetVeteran as $value)
                    {
                        if(!empty($value))
                        {
                            if (isset($value['tanggal_lahir']) && strlen($value['tanggal_lahir']) == 4)
                                $value['tanggal_lahir'] = '01-01-'.$value['tanggal_lahir'];

                            $kabupaten = isset($value['kabupaten']) ? $value['kabupaten'] :'';
                            if (!is_numeric($kabupaten) && $kabupaten != "" && $kabupaten != '-')
                            {
                                $kabupaten = checkTextAlternative($kabupaten);
                                // $kabupaten = str_replace('Kab.', '', trim($kabupaten));
                                $kab = \App\Models\Kabupaten::where('namaKabupaten', 'like', '%'.$kabupaten.'%')->get()->first();
                                if ($kab)
                                {
                                    $kab = $kab->toArray();
                                    $kabupaten = isset($kab['idKabupaten'])?$kab['idKabupaten']:'';
                                }
                            }

                            $propinsi = isset($value['propinsi']) ? trim($value['propinsi']) :'';
                            if (!is_numeric($propinsi) && $propinsi != "" && $propinsi != "-")
                            {
                                $propinsi = checkTextAlternative($propinsi);
                                if (strtoupper($propinsi) == 'A C E H')
                                    $propinsi = str_replace(' ', '', trim($propinsi));

                                $prov = \App\Models\Provinsi::where('namaProvinsi', 'like', '%'.$propinsi.'%')->get()->first();
                                if ($prov)
                                {
                                    $prov = $prov->toArray();
                                    $propinsi = isset($prov['idProvinsi'])?$prov['idProvinsi']:'';
                                }
                            }

                            $babinminvetcaddam = isset($value['babinminvetcaddam']) ? trim($value['babinminvetcaddam']) :'';
                            if (!is_numeric($babinminvetcaddam) && $babinminvetcaddam != "" && $babinminvetcaddam != "-")
                            {
                                $babinminvetcaddam = checkTextAlternative($babinminvetcaddam);

                                $kodam = \App\Models\Kodam::where('namaKodam', 'like', '%'.$babinminvetcaddam.'%')->get()->first();
                                if ($kodam)
                                {
                                    $kodam = $kodam->toArray();
                                    $babinminvetcaddam = isset($kodam['idKodam'])?$kodam['idKodam']:'';
                                }
                            }
                            
                            $kanminvetcad = isset($value['kanminvetcad']) ? trim($value['kanminvetcad']) :'';
                            if (!is_numeric($kanminvetcad) && $kanminvetcad != "" && $kanminvetcad != "-")
                            {
                                $kanminvetcad = checkTextAlternative($kanminvetcad);

                                $kodim = \App\Models\Kodim::where('namaKodim', 'like', '%'.$kanminvetcad.'%')->get()->first();
                                if ($kodim)
                                {
                                    $kodim = $kodim->toArray();
                                    $kanminvetcad = isset($kodim['idKodim'])?$kodim['idKodim']:'';
                                }
                            }

                            $jenis_kelamin = isset($value['jenis_kelamin']) ? $value['jenis_kelamin'] : '';
                            $jenisKelamin = checkTextAlternative($jenis_kelamin);
                            
                            $predikat = isset($value['predikat']) ? $value['predikat'] :'';
                            $predikat = checkTextAlternative($predikat);

                            if (isset($value['meninggal']) && $value['meninggal'] != '')
                            {
                                $statusMeninggal = strtolower($value['meninggal']);
                            }
                            else {
                                $statusMeninggal = (isset($value['tanggal_meninggal']) && $value['tanggal_meninggal'] != '')?'ya':'tidak';
                            }

                            $tahunMasaBakti = isset($value['masa_bakti_tahun'])?checkTextAlternative($value['masa_bakti_tahun']):'';
                            if (!is_numeric($tahunMasaBakti)) $tahunMasaBakti = 0;
                            $bulanMasaBakti = isset($value['masa_bakti_bulan'])?checkTextAlternative($value['masa_bakti_bulan']):'';
                            if (!is_numeric($bulanMasaBakti)) $bulanMasaBakti = 0;

                            $insert[] = [
                                'namaVeteran' => isset($value['nama'])?$value['nama']:'',
                                'tempatLahir' => isset($value['tempat_lahir'])?$value['tempat_lahir']:'',
                                'tanggalLahir' => isset($value['tanggal_lahir'])?formatDate($value['tanggal_lahir'],'Y-m-d'):'',
                                'jenisKelamin' => $jenisKelamin,
                                'NIK' => isset($value['nik'])?$value['nik']:'',
                                'noPendaftaran' => isset($value['nomor_pendaftran'])?$value['nomor_pendaftran']:'',
                                'tanggalPendaftaran' => isset($value['tanggal_pendaftaran'])?formatDate($value['tanggal_pendaftaran'], 'Y-m-d'):'',
                                'npvLama' => isset($value['npv_lama'])?$value['npv_lama']:'',
                                'npvBaru' => isset($value['npv_baru'])?$value['npv_baru']:'',
                                'noKeputusan' => isset($value['nomor_surat_keputusan'])?$value['nomor_surat_keputusan']:'',
                                'tanggalKeputusan' => isset($value['tanggal_surat_keputusan'])?formatDate($value['tanggal_surat_keputusan'], 'Y-m-d'):'',
                                'predikat' => $predikat,
                                'golongan' => isset($value['golongan'])?$value['golongan']:'',
                                'tahunMasaBakti' => $tahunMasaBakti,
                                'bulanMasaBakti' => $bulanMasaBakti,
                                'tanggalMeninggal' => isset($value['tanggal_meninggal'])?formatDate($value['tanggal_meninggal'], 'Y-m-d'):'',
                                'statusMeninggal' => $statusMeninggal,
                                'namaAhliWaris' => isset($value['nama_ahli_waris'])?$value['nama_ahli_waris']:'',
                                'tanggalLahirAhliWaris' => isset($value['tgl_lahir_ahli_waris'])?formatDate($value['tgl_lahir_ahli_waris'], 'Y-m-d'):'',
                                'NIKAhliWaris' => isset($value['nik_ahli_waris'])?$value['nik_ahli_waris']:'',
                                'tanggalMeninggalAhliWaris' => isset($value['tanggal_meninggal_ahli_waris'])?formatDate($value['tanggal_meninggal_ahli_waris'], 'Y-m-d'):'',
                                
                                'alamat' => isset($value['jalan'])?$value['jalan']:'',
                                'rt' => isset($value['rt'])?$value['rt']:'',
                                'rw' => isset($value['rw'])?$value['rw']:'',
                                'kelurahan' => isset($value['desakelurahan'])?$value['desakelurahan']:'',
                                'kecamatan' => isset($value['kecamatan'])?$value['kecamatan']:'',
                                'noKepTuvet' => isset($value['no_kep_tuvet'])?$value['no_kep_tuvet']:'',
                                'tglKepTuvet' => isset($value['tgl_kep_tuvet'])?formatDate($value['tgl_kep_tuvet'], 'Y-m-d'):'',
                                'noKepDahor' => isset($value['no_kep_dahor'])?$value['no_kep_dahor']:'',
                                'tglKepDahor' => isset($value['tgl_kep_dahor'])?formatDate($value['tgl_kep_dahor'], 'Y-m-d'):'',
                                'noKepTundayatu' => isset($value['no_kep_tundayatu'])?$value['no_kep_tundayatu']:'',
                                'tglKepTundayatu' => isset($value['tgl_kep_tundayatu'])?formatDate($value['tgl_kep_tundayatu'], 'Y-m-d'):'',
                                'nomorRekening' => isset($value['no_rekening'])?$value['no_rekening']:'',
                                'tempatPembayaran' => isset($value['tempat_pembayaran'])?$value['tempat_pembayaran']:'',
                                'noKarip' => isset($value['no_karip'])?$value['no_karip']:'',
                                'PTTaspen' => isset($value['pt_taspen'])?$value['pt_taspen']:'',
                                'keterangan' => isset($value['keterangan'])?$value['keterangan']:'',
                                'userId' => isset($this->Auth::user()->id)?$this->Auth::user()->id:'',
                                'userIdCreated' => isset($this->Auth::user()->id)?$this->Auth::user()->id:'',
                                'imageUrl' => isset($value['npv_baru'])?$value['npv_baru'].'.jpg':'',

                                // RELATION
                                'idKodam' => isset($babinminvetcaddam) ? $babinminvetcaddam :'',
                                'idKodim' => isset($kanminvetcad) ? $kanminvetcad :'',
                                'idKabupaten' => isset($kabupaten) ? $kabupaten :'',
                                'idProvinsi' => isset($propinsi) ? $propinsi :'',
                            ];
                        }
                    }

                    if(!empty($insert))
                    {
                        \App\Models\Veteran::insert($insert);

                        Session::flash('message', 'Selamat, Import data Veteran berhasil.');
                        return back()->with('success','Import data Veteran berhasil.');
                    }
                }
            });
            // })->formatDates( true, 'Y-m-d' );
        }
        return back()->with('error','Please Check your file, Something is wrong there.');
    }

    public function export(Request $request)
    {
        $fieldName = [
            'namaVeteran as NAMA',
            'tempatLahir as TEMPAT LAHIR',
            'tanggalLahir as TANGGAL LAHIR',
            'jenisKelamin as JENIS KELAMIN',
            'NIK as NIK',
            'noPendaftaran as NOMOR PENDAFTARAN',
            'tanggalPendaftaran as TANGGAL PENDAFTARAN',
            'npvLama as NPV LAMA',
            'npvBaru as NPV BARU',
            'noKeputusan as NOMOR SURAT KEPUTUSAN',
            'tanggalKeputusan as TANGGAL SURAT KEPUTUSAN',
            'predikat as PREDIKAT',
            'golongan as GOLONGAN',
            'tahunMasaBakti as TAHUN MASA BAKTI',
            'bulanMasaBakti as BULAN MASA BAKTI',
            'tanggalMeninggal as TANGGAL MENINGGAL',
            'namaAhliWaris as NAMA AHLI WARIS',
            'tanggalLahirAhliWaris as TANGGAL LAHIR AHLI WARIS',
            'NIKAhliWaris as NIK AHLI WARIS',
            'tanggalMeninggalAhliWaris as TANGGAL MENINGGAL AHLI WARIS',
            'tanggalMeninggalAhliWaris as TANGGAL MENINGGAL AHLI WARIS',
            'alamat as JALAN',
            'rt as RT',
            'rw as RW',
            'kelurahan as DESA/KELURAHAN',
            'kecamatan as KECAMATAN',
            'kabupaten.namaKabupaten as KABUPATEN',
            'provinsi.namaProvinsi as PROPINSI',
            'noKepTuvet as NO. KEP TUVET',
            'tglKepTuvet as TGL. KEP TUVET',
            'noKepDahor as NO. KEP DAHOR',
            'tglKepDahor as TGL. KEP DAHOR',
            'noKepTundayatu as NO. KEP TUNDAYATU',
            'tglKepTundayatu as TGL. KEP TUNDAYATU',
            'nomorRekening as KTR/BANK BAYAR DAN NOMOR REKENING',
            'PTTaspen as PT TASPEN',
            'keterangan as KETERANGAN',
        ];
        $data = \App\Models\Veteran::select($fieldName);
        $data = $data->leftJoin('kabupaten', 'kabupaten.idKabupaten', 'veteran.idKabupaten');
        $data = $data->leftJoin('provinsi', 'provinsi.idProvinsi', 'veteran.idProvinsi');
        $data = $data->get();
        if (isset($data))
        {
            $data = $data->toArray();
            return Excel::create('DATA VETERAN', function($excel) use ($data)
            {
                $excel->sheet('Data Veteran', function($sheet) use ($data)
                {
                    $sheet->fromArray($data);
                });
            })->download('xlsx');
        }
    }

    public function deleteSelected(Request $request)
    {
        $item = \App\Models\Veteran::whereIn('idVeteran', $request->selectedId)->delete();
        if ($item)
        {
            Session::flash('message', 'Successfully deleted the item!');
            $response = [
                'statusCode' => '200',
                'message' => 'Successfully deleted the item!'
            ];
            return response()->json($response); 
        }
    }
}