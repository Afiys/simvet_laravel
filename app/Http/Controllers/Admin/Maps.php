<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Maps extends Controller
{
	public function index()
	{
        $list = "";
        return view('setting.map', compact('list'));
	}
}
