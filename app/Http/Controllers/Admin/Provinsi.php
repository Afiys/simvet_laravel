<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;

class Provinsi extends BaseController
{
    protected $redirectTo = 'provinsi'; // REDIRECT URL
    protected $redirectIndex = 'provinsi.index'; // View provinsi
    protected $redirectEditable = 'provinsi.editable'; // vied add and update

    /**
    * Send data to view
    */
    protected function getList()
    {
        // return \App\Models\Provinsi::all(); 
    }

    /**
    * list ajax for datatables
    */
    protected function listAjax(Request $request)
    {
        $item = new \App\Models\Provinsi();
        $item = $item->leftJoin('kodam', 'kodam.idKodam', 'provinsi.idKodam');
        $item = $item->select(['provinsi.*', 'kodam.namaKodam']);

        return Datatables::of($item)
            ->addColumn('action', function ($item)
            {
                $action = '';
                if (in_array('ALL', $this->Config::get('menu.role.provinsi.update')))
                {
                    $action .= '<a href="'.route('provinsi.index').'/'.$item->idProvinsi.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.provinsi.update')))
                    {
                        $action .= '<a href="'.route('provinsi.index').'/'.$item->idProvinsi.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                    }
                }

                // $action .= '<a href="'.route('provinsi.index').'/'.$item->idProvinsi.'" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';

                if (in_array('ALL', $this->Config::get('menu.role.provinsi.destroy')))
                {
                    $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idProvinsi.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.provinsi.destroy')))
                    {
                        $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idProvinsi.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                    }
                }
                if ($action == '') $action = '-';
                
                return $action;
            })
            ->editColumn('id', 'ID: {{$idProvinsi}}')
            ->rawColumns(['action']) // raw show html
            ->make(true);
    }

    protected function getLookUp()
    {
        $data['kodam'] = \App\Models\Kodam::get();
        return $data; // data
    }

    protected function find($id)
    {
        return \App\Models\Provinsi::find($id);
    }

    protected function validateData(Request $request, $id)
    {
        /*if($id > 0) return null;*/
        return Validator::make($request->all(), [
            'namaProvinsi' => 'required|string',
            'idKodam' => 'required|string',
        ],
        [
            'namaProvinsi.required'   => 'Nama provinsi tidak boleh kosong',
            'idKodam.required'   => 'Silahkan pilih kodam',
        ]);
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\Provinsi();
        
        if($id > 0) $item = $item->find($id);

        /*
        if ($request->image)
        {
            $image          = $request->image;
            $imageName      = time().$image->getClientOriginalName();
            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $destinationPath= $this->Config::get('app.directory.article');
            $result         = $image->move($destinationPath, $imageName);
            $imageUrl       = $destinationPath."/".$imageName;
            $item->imageUrl = $imageUrl;
        }
        */

        $item->namaProvinsi = $request->namaProvinsi;
		$item->idKodam = $request->idKodam;
        $item->save();
    }

    public function getJson(Request $request)
    {
        $option = "";
        $provinsi = \App\Models\Provinsi::get();
        $provinsi = $provinsi->where('idKodam', $request->id);
        if ($provinsi)
        {
            $provinsi = $provinsi->toArray();
            if ($provinsi && count($provinsi) > 0)
            {
                $option .= '<option value="">Pilih Provinsi</option>';
                foreach ($provinsi as $key => $item)
                {
                    $option .= '<option value="'.$item['idProvinsi'].'">'.$item['namaProvinsi'].'</option>';
                }
            }
            else {
                $option .= '<option value="">Data Provinsi kosong</option>';
            }
            $response = [
                'statusCode' => '200',
                'message' => 'Successfully get data!',
                'data' => $provinsi,
                'option' => $option
            ];
        }
        else {
            $provinsi = [];
            $option .= '<option value="">Data Provinsi kosong</option>';
            $response = [
                'statusCode' => '404',
                'message' => 'Data kosong !',
                'data' => $provinsi,
                'option' => $option
            ];
        }
        return response()->json($response);
    }
}