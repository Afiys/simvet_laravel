<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;

class User extends BaseController
{
    protected $redirectTo = 'user'; // REDIRECT URL
    protected $redirectIndex = 'user.index'; // View user
    protected $redirectEditable = 'user.editable'; // vied add and update

    /**
    * Send data to view
    */
    protected function getList()
    {
        // return \App\Models\User::all(); 
    }

    /**
    * list ajax for datatables
    */
    protected function listAjax(Request $request)
    {
        $item = new \App\Models\User(); 
        $item = $item->select(['*']);

        return Datatables::of($item)
            ->addColumn('action', function ($item)
            {
                $action = '';
                $action .= '<a href="'.route('user.index').'/'.$item->id.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                // $action .= '<a href="'.route('user.index').'/'.$item->id.'" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';
                $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->id.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                return $action;
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->rawColumns(['action']) // raw show html
            ->make(true);
    }

    protected function getLookUp()
    {
        $data = array();
        return $data; // data
    }

    protected function find($id)
    {
        return \App\Models\User::find($id);
    }

    protected function validateData(Request $request, $id)
    {
        /*if($id > 0) return null;*/
        return Validator::make($request->all(), [
            'name'          => 'required|string',
            'username'      => 'required|string|max:50|unique:users,username,'.$id,
            // 'password'      => 'required|string|min:4', // with field name password_confirm
            'roleCode'      => 'required|string',

            
            // 'image'         => 'required|mimetypes:image/jpeg,image/jpg',
            // 'sample1'       => 'required_without_all:sample2|numeric',
            // 'sample2'       => 'required_without_all:sample1|numeric',
            // 'numeric'       => 'required|numeric|max:100',
            // 'nullable'      => 'nullable|numeric',
            // 'email'         => 'required|string|email|max:50|unique:users,email,dataValue',// unique with value $datavalue
            // 'password'      => 'required|string|min:6|confirmed', // with field name password_confirm
        ],
        [
            // custom message
            'name.required'   => 'Nama tidak boleh kosong',
            'roleCode.required'   => 'Silahkan pilih role',
            'username.unique' => 'Username sudah digunakan',
        ]);
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\User();
        
        if($id > 0) $item = $item->find($id);

        /*
        if ($request->image)
        {
            $image          = $request->image;
            $imageName      = time().$image->getClientOriginalName();
            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $destinationPath= $this->Config::get('app.directory.article');
            $result         = $image->move($destinationPath, $imageName);
            $imageUrl       = $destinationPath."/".$imageName;
            $item->imageUrl = $imageUrl;
        }
        */

		$item->name = $request->name;
		$item->username = $request->username;
		$item->email = $request->email;
        
        if ($request->password)
            $item->password = bcrypt($request->password);
		
        $item->roleCode = $request->roleCode;
        $item->save();
    }
}