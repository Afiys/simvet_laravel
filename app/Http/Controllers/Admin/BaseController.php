<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class BaseController extends Controller
{
    use RESTfull;

    protected $id = 0;
    protected $item = null;
    protected $visitor;

    public function __construct()
    {
        // $this->middleware('auth');
        // $this->middleware('AuthRole:ADM', ['only' => ['babinminvetcaddam.create']]);
        $this->Auth     = new \Auth();
        $this->Config   = new \Config();
        $this->icon     = $this->Config::get('icon');
        $this->visitor  = [];
        $this->setVisitorInfo();
    }

    protected function setVisitorInfo()
    {
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        $record = geoip()->getLocation($ip);

        $this->visitor['ipAddress']     = $record->ip;
        $this->visitor['latitude']      = $record->lat;
        $this->visitor['longitude']     = $record->lon;
        $this->visitor['country']       = $record->country;
        $this->visitor['city']          = $record->city;
        $this->visitor['timezone']      = $record->timezone;
    }

    public function index()
    {
        $list = $this->getList();
        return view($this->redirectIndexPath(), compact('list'));
    }

    public function listData(Request $request)
    {
        return $this->listAjax($request);
    }

    public function create()
    {
        return $this->edit(0);
    }

    public function edit($id)
    {
        $this->id       = $id;
        $item           = $this->find($id);
        $this->item     = $item;
        $lookupTable    = $this->getLookUp();
        if ($id > 0)
        {
            if (isset($item) && is_array($item) && count($item) == 0)
            {
                return Redirect::to($this->redirectPath());
            }
        }
        return view($this->redirectEditablePath(), compact('item', 'id', 'lookupTable'));
    }
    
    public function store(Request $request)
    {
        return $this->save($request, 0);
    }

    public function update(Request $request, $id)
    {
        return $this->save($request, $id);
    }

    public function show($id)
    {
        $this->id       = $id;
        $item           = $this->find($id);
        $this->item     = $item;
        $lookupTable    = $this->getLookUp();
        if ($id > 0)
        {
            if (isset($item) && is_array($item) && count($item) == 0)
            // if (count($item) == 0)
            {
                return Redirect::to($this->redirectPath());
            }
        }
        return view($this->redirectShowPath(), compact('item', 'id', 'lookupTable'));
    }

    public function destroy($id)
    {
        $this->id   = $id;
        $item       = $this->find($id);
        $item->delete();

        Session::flash('message', 'Successfully deleted the item!');
        $response = [
            'statusCode' => '200',
            'message' => 'Successfully deleted the item!'
        ];
        return response()->json($response); 
        // return Redirect::to($this->redirectPath());
    }

    protected function getList()
    {
        return null;
    }

    protected function listAjax(Request $request)
    {
        return null;
    }

    protected function getLookUp()
    {
        return null;
    }
    
    protected function find($id) {
        return null;
    }

    public function save(Request $request, $id)
    {
        $validator = $this->validateData($request, $id);
        if ($validator != null && $validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        } else {
            $this->saveData($request, $id);
            Session::flash('message', 'Data berhasil di simpan');
            return Redirect::to($this->redirectPath());
        }
    }

    protected function validateData(Request $request, $id)
    {
        return null;
    }

    protected function saveData(Request $request, $id) { }
}