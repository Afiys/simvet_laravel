<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use PDF;

class Setting extends BaseController
{
	public function index()
	{
		$item = [];
        $setting = \App\Models\Setting::where('show', 1)->orderBy('order', 'ASC')->get();
        if ($setting)
        {
        	$setting = $setting->toArray();
        	$item = $setting;
        }

        return view('setting.editable', compact('item'));
	}

    public function settingStore(Request $request)
    {
    	$reqData = $request->all();
    	unset($reqData['_token']);
    	foreach ($reqData as $key => $value)
    	{
    		// IMAGE
    		$type = isset($value['type']) ? $value['type'] : '';
    		if ($type == 'image')
    		{
    			$image = isset($value['data']) ? $value['data'] :'';
		        if ($image != '')
		        {
		            $imageName      = 'setting_'.time().'.'.$image->getClientOriginalExtension();
		            // $imageName      = time().$image->getClientOriginalName();
		            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
		            $destinationPath= $this->Config::get('app.directory.setting');
		            $result         = $image->move($destinationPath, $imageName);
		            $imageUrl       = $destinationPath."/".$imageName;
	        		$result = \App\Models\Setting::where('key', $key)->update(['value' => $imageUrl]);
		        }
    		}
    		// TEXT
    		elseif ($type == 'text')
    		{
	    		if ($value['data'] != $value['old'])
	    		{
	        		$result = \App\Models\Setting::where('key', $key)->update(['value' => $value['data']]);
	    		}
    		}
    		// LONG TEXT
    		elseif ($type == 'textarea')
    		{
	    		if ($value['data'] != $value['old'])
	    		{
	        		$result = \App\Models\Setting::where('key', $key)->update(['longText' => nl2br($value['data'])]);
	    		}
    		}
    	}

        Session::flash('message', 'Successfully saved the item!');
        return Redirect::to(Route('setting.index'));
    }

}