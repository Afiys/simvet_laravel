<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;

class Kodam extends BaseController
{
    protected $redirectTo = 'babinminvetcaddam'; // REDIRECT URL
    protected $redirectIndex = 'kodam.index'; // View kodam
    protected $redirectEditable = 'kodam.editable'; // vied add and update

    /**
    * Send data to view
    */
    protected function getList()
    {
        // return \App\Models\Kodam::all(); 
    }

    /**
    * list ajax for datatables
    */
    protected function listAjax(Request $request)
    {
        $item = new \App\Models\Kodam(); 
        $item = $item->select(['*']);

        return Datatables::of($item)
            ->addColumn('action', function ($item)
            {
                $action = '';

                if (in_array('ALL', $this->Config::get('menu.role.babinminvetcaddam.update')))
                {
                    $action .= '<a href="'.route('babinminvetcaddam.index').'/'.$item->idKodam.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.babinminvetcaddam.update')))
                    {
                        $action .= '<a href="'.route('babinminvetcaddam.index').'/'.$item->idKodam.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                    }
                }

                // $action .= '<a href="'.route('babinminvetcaddam.index').'/'.$item->idKodam.'" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';
                
                if (in_array('ALL', $this->Config::get('menu.role.babinminvetcaddam.destroy')))
                {
                    $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idKodam.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.babinminvetcaddam.destroy')))
                    {
                        $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idKodam.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                    }
                }
                if ($action == '') $action = '-';

                return $action;
            })
            ->editColumn('id', 'ID: {{$idKodam}}')
            ->rawColumns(['action']) // raw show html
            ->make(true);
    }

    protected function getLookUp()
    {
        $data = array();
        return $data; // data
    }

    protected function find($id)
    {
        return \App\Models\Kodam::find($id);
    }

    protected function validateData(Request $request, $id)
    {
        /*if($id > 0) return null;*/
        return Validator::make($request->all(), [
            'namaKodam'        => 'required|string',
        ],
        [
            // custome message
            'namaKodam.required'   => 'Babiminvetcaddam tidak boleh kosong',
        ]);
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\Kodam();
        
        if($id > 0) $item = $item->find($id);

        /*
        if ($request->image)
        {
            $image          = $request->image;
            $imageName      = time().$image->getClientOriginalName();
            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $destinationPath= $this->Config::get('app.directory.article');
            $result         = $image->move($destinationPath, $imageName);
            $imageUrl       = $destinationPath."/".$imageName;
            $item->imageUrl = $imageUrl;
        }
        */

		$item->namaKodam = $request->namaKodam;
        $item->save();
    }
}