<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;

class Kodim extends BaseController
{
    protected $redirectTo = 'kanminvetcad'; // REDIRECT URL
    protected $redirectIndex = 'kodim.index'; // View kodim
    protected $redirectEditable = 'kodim.editable'; // vied add and update

    /**
    * Send data to view
    */
    protected function getList()
    {
        $data = [];
        $data['kodam'] = \App\Models\Kodam::get();
        return $data;
    }

    /**
    * list ajax for datatables
    */
    protected function listAjax(Request $request)
    {
        $item = new \App\Models\Kodim(); 
        $item = $item->leftJoin('kodam', 'kodam.idKodam', 'kodim.idKodam');
        $item = $item->select(['kodim.*', 'kodam.namaKodam']);

        return Datatables::of($item)
            ->filter(function ($query) use ($request)
            {
                if ($request->has('customSearch'))
                {
                    $params = parseDataSearch($request->customSearch);
                    $idKodam = isset($params['kodam']) ? $params['kodam'] :'';
                    if ($idKodam != "")
                        $query->where('kodim.idKodam', $idKodam);
                }
            })
            ->addColumn('action', function ($item)
            {
                $action = '';
                if (in_array('ALL', $this->Config::get('menu.role.kanminvetcad.update')))
                {
                    $action .= '<a href="'.route('kanminvetcad.index').'/'.$item->idKodim.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.kanminvetcad.update')))
                    {
                        $action .= '<a href="'.route('kanminvetcad.index').'/'.$item->idKodim.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                    }
                }

                // $action .= '<a href="'.route('kanminvetcad.index').'/'.$item->idKodim.'" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';

                if (in_array('ALL', $this->Config::get('menu.role.kanminvetcad.destroy')))
                {
                    $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idKodim.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.kanminvetcad.destroy')))
                    {
                        $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idKodim.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                    }
                }
                if ($action == '') $action = '-';

                return $action;
            })
            ->editColumn('id', 'ID: {{$idKodim}}')
            ->rawColumns(['action']) // raw show html
            ->make(true);
    }

    protected function getLookUp()
    {
        $data['kodam'] = \App\Models\Kodam::get();
        return $data; // data
    }

    protected function find($id)
    {
        return \App\Models\Kodim::find($id);
    }

    protected function validateData(Request $request, $id)
    {
        /*if($id > 0) return null;*/
        return Validator::make($request->all(), [
            'namaKodim' => 'required|string',
            'idKodam' => 'required|numeric',
        ],
        [
            // custome message
            'namaKodim.required' => 'Nama Kanminvetcad tidak boleh kosong',
            'idKodam.required' => 'Silahkan pilih Babiminvetcaddam',
        ]);
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\Kodim();
        
        if($id > 0) $item = $item->find($id);

        /*
        if ($request->image)
        {
            $image          = $request->image;
            $imageName      = time().$image->getClientOriginalName();
            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $destinationPath= $this->Config::get('app.directory.article');
            $result         = $image->move($destinationPath, $imageName);
            $imageUrl       = $destinationPath."/".$imageName;
            $item->imageUrl = $imageUrl;
        }
        */

		$item->namaKodim = $request->namaKodim;
		$item->idKodam = $request->idKodam;
        $item->save();
    }

    public function getJson(Request $request)
    {
        $option = "";
        $kodim = \App\Models\Kodim::get();
        $kodim = $kodim->where('idKodam', $request->id);
        if ($kodim)
        {
            $kodim = $kodim->toArray();
            if ($kodim && count($kodim) > 0)
            {
                $option .= '<option value="">Pilih Kanminvetcad</option>';
                foreach ($kodim as $key => $item)
                {
                    $option .= '<option value="'.$item['idKodim'].'">'.$item['namaKodim'].'</option>';
                }
            }
            else {
                $option .= '<option value="">Data Kanminvetcad kosong</option>';
            }
            $response = [
                'statusCode' => '200',
                'message' => 'Successfully get data!',
                'data' => $kodim,
                'option' => $option
            ];
        }
        else {
            $kodim = [];
            $option .= '<option value="">Data Kanminvetcad kosong</option>';
            $response = [
                'statusCode' => '404',
                'message' => 'Data kosong !',
                'data' => $kodim,
                'option' => $option
            ];
        }
        return response()->json($response);
    }
}