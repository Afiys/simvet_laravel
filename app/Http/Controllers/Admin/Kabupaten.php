<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;

class Kabupaten extends BaseController
{
    protected $redirectTo = 'kabupaten'; // REDIRECT URL
    protected $redirectIndex = 'kabupaten.index'; // View kabupaten
    protected $redirectEditable = 'kabupaten.editable'; // vied add and update

    /**
    * Send data to view
    */
    protected function getList()
    {
        // return \App\Models\Kabupaten::all(); 
    }

    /**
    * list ajax for datatables
    */
    protected function listAjax(Request $request)
    {
        $item = new \App\Models\Kabupaten(); 
        $item = $item->leftJoin('provinsi', 'provinsi.idProvinsi', 'kabupaten.idProvinsi');
        $item = $item->select(['kabupaten.*', 'provinsi.namaProvinsi']);

        return Datatables::of($item)
            ->addColumn('action', function ($item)
            {
                $action = '';
                if (in_array('ALL', $this->Config::get('menu.role.kabupaten.update')))
                {
                    $action .= '<a href="'.route('kabupaten.index').'/'.$item->idKabupaten.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.kabupaten.update')))
                    {
                        $action .= '<a href="'.route('kabupaten.index').'/'.$item->idKabupaten.'/edit" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['edit'].'"></i> Edit</a>&nbsp;&nbsp;&nbsp;';
                    }
                }

                // $action .= '<a href="'.route('kabupaten.index').'/'.$item->idKabupaten.'" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';

                if (in_array('ALL', $this->Config::get('menu.role.kabupaten.destroy')))
                {
                    $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idKabupaten.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                }
                else {
                    if (isset($this->Auth::user()->roleCode) && in_array($this->Auth::user()->roleCode, $this->Config::get('menu.role.kabupaten.destroy')))
                    {
                        $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idKabupaten.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                    }
                }
                if ($action == '') $action = '-';

                return $action;
            })
            ->editColumn('id', 'ID: {{$idKabupaten}}')
            ->rawColumns(['action']) // raw show html
            ->make(true);
    }

    protected function getLookUp()
    {
        $data['provinsi'] = \App\Models\Provinsi::get();
        return $data; // data
    }

    protected function find($id)
    {
        return \App\Models\Kabupaten::find($id);
    }

    protected function validateData(Request $request, $id)
    {
        /*if($id > 0) return null;*/
        return Validator::make($request->all(), [
            'namaKabupaten' => 'required|string',
            'idProvinsi' => 'required|string',
        ],
        [
            // custome message
            'namaKabupaten.required'   => 'Nama kabupaten tidak boleh kosong',
            'idProvinsi.required' => 'Silahkan pilih provinsi',
        ]);
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\Kabupaten();
        
        if($id > 0) $item = $item->find($id);

        /*
        if ($request->image)
        {
            $image          = $request->image;
            $imageName      = time().$image->getClientOriginalName();
            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $destinationPath= $this->Config::get('app.directory.article');
            $result         = $image->move($destinationPath, $imageName);
            $imageUrl       = $destinationPath."/".$imageName;
            $item->imageUrl = $imageUrl;
        }
        */

		$item->namaKabupaten = $request->namaKabupaten;
		$item->idProvinsi = $request->idProvinsi;
        $item->save();
    }

    public function getJson(Request $request)
    {
        $option = "";
        $kabupaten = \App\Models\Kabupaten::get();
        $kabupaten = $kabupaten->where('idProvinsi', $request->id);
        if ($kabupaten)
        {
            $kabupaten = $kabupaten->toArray();
            if ($kabupaten && count($kabupaten) > 0)
            {
                $option .= '<option value="">Pilih Kabupaten</option>';
                foreach ($kabupaten as $key => $item)
                {
                    $option .= '<option value="'.$item['idKabupaten'].'">'.$item['namaKabupaten'].'</option>';
                }
            }
            else {
                $option .= '<option value="">Data Kabupaten kosong</option>';
            }
            $response = [
                'statusCode' => '200',
                'message' => 'Successfully get data!',
                'data' => $kabupaten,
                'option' => $option
            ];
        }
        else {
            $kabupaten = [];
            $option .= '<option value="">Data Kabupaten kosong</option>';
            $response = [
                'statusCode' => '404',
                'message' => 'Data kosong !',
                'data' => $kabupaten,
                'option' => $option
            ];
        }
        return response()->json($response);
    }
}