<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Datatables;

class KritikSaran extends BaseController
{
    protected $redirectTo = 'kritiksaran'; // REDIRECT URL
    protected $redirectIndex = 'kritiksaran.index'; // View kritiksaran
    protected $redirectEditable = 'kritiksaran.editable'; // vied add and update
    protected $redirectShow =  'kritiksaran.jawaban';

    public function index()
    {
        /*if (!$this->Auth::id())
        {
            return Redirect::to('kritiksaran/create');
        }
        else {*/
            $list = $this->getList();
            return view($this->redirectIndexPath(), compact('list'));
        // }
    }

    protected function getList()
    {
        // return \App\Models\KritikSaran::all(); 
    }

    /**
    * list ajax for datatables
    */
    protected function listAjax(Request $request)
    {
        $item = new \App\Models\KritikSaran(); 
        $item = $item->select(['*']);

        return Datatables::of($item)
            ->addColumn('created_at', function ($item)
            {
                $created_at = formatDate($item->created_at, 'd-m-Y H:i:s');
                return $created_at;
            })
            ->addColumn('action', function ($item)
            {
                $action = '';
                $action .= '<a href="'.route('kritiksaran.index').'/'.$item->idKritikSaran.'/show" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';
                // $action .= '<a href="'.route('kritiksaran.index').'/'.$item->idKritikSaran.'" class="btn btn-xs btn-primary"><i class="'.$this->icon['tables']['detail'].'"></i> Detail</a>&nbsp;&nbsp;&nbsp;';
                // $action .= '<a href="#" class="btn btn-xs btn-primary" onClick="listManager.delete(\''.$item->idKritikSaran.'\')"><i class="'.$this->icon['tables']['delete'].'"></i> Delete</a>';
                return $action;
            })
            ->editColumn('id', 'ID: {{$idKritikSaran}}')
            ->rawColumns(['action']) // raw show html
            ->make(true);
    }

    protected function getLookUp()
    {
        $data = array();
        return $data; // data
    }

    protected function find($id)
    {
        return \App\Models\KritikSaran::find($id);
    }

    protected function validateData(Request $request, $id)
    {
        if (!$id)
        {
            return Validator::make($request->all(), [
                'g-recaptcha-response' => 'required',
            ],
            [
                'g-recaptcha-response.required' => 'Harap isi Captcha',
            ]
            );
        }
        /*if($id > 0) return null;*/
        /*return Validator::make($request->all(), [
            'image'         => 'required|mimetypes:image/jpeg,image/jpg',
            'sample1'       => 'required_without_all:sample2|numeric',
            'sample2'       => 'required_without_all:sample1|numeric',
            'string'        => 'required|string',
            'numeric'       => 'required|numeric|max:100',
            'nullable'      => 'nullable|numeric',
            'uniqueField'   => 'nullable|max:20|unique:nametables',
            'email'         => 'required|string|email|max:50|unique:users,email,dataValue',// unique with value $datavalue
            'password'      => 'required|string|min:6|confirmed', // with field name password_confirm
        ],
        [
            // custome message
            'numeric.required'   => 'Please fill numeric',
            'uniqueField.unique' => 'Must unique',
        ]);*/
    }

    public function save(Request $request, $id)
    {
        $validator = $this->validateData($request, $id);
        if ($validator != null && $validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else {
            if (!$this->Auth::id())
            {
                $this->saveData($request, $id);
                Session::flash('message', 'Data berhasil di kirim!');
                return Redirect::to('kritiksaran/create');
            }
            else {
                $this->saveData($request, $id);
                Session::flash('message', 'Data berhasil di kirim');
                return Redirect::to($this->redirectPath());
            }
        }
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\KritikSaran();
        
        if($id > 0) $item = $item->find($id);

        /*
        if ($request->image)
        {
            $image          = $request->image;
            $imageName      = time().$image->getClientOriginalName();
            // $imageName      = md5($image->getClientOriginalName().time()).'.'.$image->getClientOriginalExtension();
            $destinationPath= $this->Config::get('app.directory.article');
            $result         = $image->move($destinationPath, $imageName);
            $imageUrl       = $destinationPath."/".$imageName;
            $item->imageUrl = $imageUrl;
        }
        */

		$item->nama = $request->nama;
		$item->email = $request->email;
		$item->noTelp = $request->noTelp;
        $item->message = $request->message;
		if ($request->jawaban && $request->jawaban != "")
        {
            $item->jawaban = $request->jawaban;
            $item->statusJawaban = 'sudah';
        }
        $item->save();
    }
}