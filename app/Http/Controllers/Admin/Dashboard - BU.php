<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use PDF;

class Dashboard extends BaseController
{
    /*public function __construct()
    {
        $this->Auth     = new \Auth();
        $this->Config   = new \Config();
        $this->icon     = $this->Config::get('icon');
    }*/

	public function index()
	{
        $kodam = \App\Models\Kodam::get();
        $list['kodam'] = $kodam;
        $provinsi = \App\Models\Provinsi::get();
        $list['provinsi'] = $provinsi;
        return view('home', compact('list'));
	}

	public function getDataRekapKodam($params = array())
	{
        $list = [];
        $total = [];
        $totalIndonesia = [];
        $showHidup = true; // hanya show yang hidup
		if (isset($params['idKodam']) && strtoupper($params['idKodam']) != 'ALL')
		{
			$kodims = \App\Models\Kodim::select(['idKodim', 'namaKodim']);
			$kodims = $kodims->where('idKodam', $params['idKodam']);
			$kodims = $kodims->get();
			$total['WILAYAH'] = 'JUMLAH';
			$total['PKRI_SUDAH'] = 0;
			$total['TKR'] = 0;
			$total['DKR'] = 0;
			$total['SRJ'] = 0;
			$total['PERDAMAIAN'] = 0;
			$total['ANM'] = 0; // VET ANUMERTA
			$total['L'] = 0;
			$total['P'] = 0;
			$total['< 60'] = 0;
			$total['60 - 70'] = 0;
			$total['> 70'] = 0;
			$total['JUMLAH'] = 0;
			if ($kodims)
			{
				$kodims = $kodims->toArray();
				$key = 0;
                $PKRI_DAHOR_SUDAH_TOTAL = 0;
                $TKR_DAHOR_SUDAH_TOTAL = 0;
                $DKR_DAHOR_SUDAH_TOTAL = 0;
                $SRJ_DAHOR_SUDAH_TOTAL = 0;
                $PKRI_TUVET_SUDAH_TOTAL = 0;
                $TKR_TUVET_SUDAH_TOTAL = 0;
                $DKR_TUVET_SUDAH_TOTAL = 0;
                $SRJ_TUVET_SUDAH_TOTAL = 0;
                $PKRI_DAHOR_BELUM_TOTAL = 0;
                $TKR_DAHOR_BELUM_TOTAL = 0;
                $DKR_DAHOR_BELUM_TOTAL = 0;
                $SRJ_DAHOR_BELUM_TOTAL = 0;
                $PKRI_TUVET_BELUM_TOTAL = 0;
                $TKR_TUVET_BELUM_TOTAL = 0;
                $DKR_TUVET_BELUM_TOTAL = 0;
                $SRJ_TUVET_BELUM_TOTAL = 0;
				$jumlahTotal = 0;
				foreach ($kodims as $kodim)
				{
					$list[$key]['WILAYAH'] = $kodim['namaKodim'];
					$list[$key]['PKRI_DAHOR_SUDAH'] = 0;
	                $list[$key]['TKR_DAHOR_SUDAH'] = 0;
	                $list[$key]['DKR_DAHOR_SUDAH'] = 0;
	                $list[$key]['SRJ_DAHOR_SUDAH'] = 0;
	                $list[$key]['PKRI_TUVET_SUDAH'] = 0;
	                $list[$key]['TKR_TUVET_SUDAH'] = 0;
	                $list[$key]['DKR_TUVET_SUDAH'] = 0;
	                $list[$key]['SRJ_TUVET_SUDAH'] = 0;
	                $list[$key]['PKRI_DAHOR_BELUM'] = 0;
	                $list[$key]['TKR_DAHOR_BELUM'] = 0;
	                $list[$key]['DKR_DAHOR_BELUM'] = 0;
	                $list[$key]['SRJ_DAHOR_BELUM'] = 0;
	                $list[$key]['PKRI_TUVET_BELUM'] = 0;
	                $list[$key]['TKR_TUVET_BELUM'] = 0;
	                $list[$key]['DKR_TUVET_BELUM'] = 0;
	                $list[$key]['SRJ_TUVET_BELUM'] = 0;

					$PKRI_DAHOR_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI"');
					$PKRI_DAHOR_SUDAH = $PKRI_DAHOR_SUDAH->whereRaw("noKepDahor <> ''")->whereNotNull('noKepDahor');
					if ($showHidup)
					{
						$PKRI_DAHOR_SUDAH = $PKRI_DAHOR_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $PKRI_DAHOR_SUDAH = $PKRI_DAHOR_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$PKRI_DAHOR_SUDAH = $PKRI_DAHOR_SUDAH->count();
					if ($PKRI_DAHOR_SUDAH > 0) $list[$key]['PKRI_DAHOR_SUDAH'] = $PKRI_DAHOR_SUDAH;
					$PKRI_DAHOR_SUDAH_TOTAL = $PKRI_DAHOR_SUDAH_TOTAL + $PKRI_DAHOR_SUDAH;

					$TKR_DAHOR_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR")');
					$TKR_DAHOR_SUDAH = $TKR_DAHOR_SUDAH->whereRaw("noKepDahor <> ''")->whereNotNull('noKepDahor');
					if ($showHidup)
					{
						$TKR_DAHOR_SUDAH = $TKR_DAHOR_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $TKR_DAHOR_SUDAH = $TKR_DAHOR_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$TKR_DAHOR_SUDAH = $TKR_DAHOR_SUDAH->count();
					if ($TKR_DAHOR_SUDAH > 0) $list[$key]['TKR_DAHOR_SUDAH'] = $TKR_DAHOR_SUDAH;
					$TKR_DAHOR_SUDAH_TOTAL = $TKR_DAHOR_SUDAH_TOTAL + $TKR_DAHOR_SUDAH;

					$DKR_DAHOR_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR")');
					$DKR_DAHOR_SUDAH = $DKR_DAHOR_SUDAH->whereRaw("noKepDahor <> ''")->whereNotNull('noKepDahor');
					if ($showHidup)
					{
						$DKR_DAHOR_SUDAH = $DKR_DAHOR_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $DKR_DAHOR_SUDAH = $DKR_DAHOR_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$DKR_DAHOR_SUDAH = $DKR_DAHOR_SUDAH->count();
					if ($DKR_DAHOR_SUDAH > 0) $list[$key]['DKR_DAHOR_SUDAH'] = $DKR_DAHOR_SUDAH;
					$DKR_DAHOR_SUDAH_TOTAL = $DKR_DAHOR_SUDAH_TOTAL + $DKR_DAHOR_SUDAH;

					$SRJ_DAHOR_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ")');
					$SRJ_DAHOR_SUDAH = $SRJ_DAHOR_SUDAH->whereRaw("noKepDahor <> ''")->whereNotNull('noKepDahor');
					if ($showHidup)
					{
						$SRJ_DAHOR_SUDAH = $SRJ_DAHOR_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $SRJ_DAHOR_SUDAH = $SRJ_DAHOR_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$SRJ_DAHOR_SUDAH = $SRJ_DAHOR_SUDAH->count();
					if ($SRJ_DAHOR_SUDAH > 0) $list[$key]['SRJ_DAHOR_SUDAH'] = $SRJ_DAHOR_SUDAH;
					$SRJ_DAHOR_SUDAH_TOTAL = $SRJ_DAHOR_SUDAH_TOTAL + $SRJ_DAHOR_SUDAH;

					$PKRI_TUVET_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI"');
					$PKRI_TUVET_SUDAH = $PKRI_TUVET_SUDAH->whereRaw("noKepTuvet <> ''")->whereNotNull('noKepTuvet');
					if ($showHidup)
					{
						$PKRI_TUVET_SUDAH = $PKRI_TUVET_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $PKRI_TUVET_SUDAH = $PKRI_TUVET_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$PKRI_TUVET_SUDAH = $PKRI_TUVET_SUDAH->count();
					if ($PKRI_TUVET_SUDAH > 0) $list[$key]['PKRI_TUVET_SUDAH'] = $PKRI_TUVET_SUDAH;
					$PKRI_TUVET_SUDAH_TOTAL = $PKRI_TUVET_SUDAH_TOTAL + $PKRI_TUVET_SUDAH;

					$TKR_TUVET_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR")');
					$TKR_TUVET_SUDAH = $TKR_TUVET_SUDAH->whereRaw("noKepTuvet <> ''")->whereNotNull('noKepTuvet');
					if ($showHidup)
					{
						$TKR_TUVET_SUDAH = $TKR_TUVET_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $TKR_TUVET_SUDAH = $TKR_TUVET_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$TKR_TUVET_SUDAH = $TKR_TUVET_SUDAH->count();
					if ($TKR_TUVET_SUDAH > 0) $list[$key]['TKR_TUVET_SUDAH'] = $TKR_TUVET_SUDAH;
					$TKR_TUVET_SUDAH_TOTAL = $TKR_TUVET_SUDAH_TOTAL + $TKR_TUVET_SUDAH;

					$DKR_TUVET_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR")');
					$DKR_TUVET_SUDAH = $DKR_TUVET_SUDAH->whereRaw("noKepTuvet <> ''")->whereNotNull('noKepTuvet');
					if ($showHidup)
					{
						$DKR_TUVET_SUDAH = $DKR_TUVET_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $DKR_TUVET_SUDAH = $DKR_TUVET_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$DKR_TUVET_SUDAH = $DKR_TUVET_SUDAH->count();
					if ($DKR_TUVET_SUDAH > 0) $list[$key]['DKR_TUVET_SUDAH'] = $DKR_TUVET_SUDAH;
					$DKR_TUVET_SUDAH_TOTAL = $DKR_TUVET_SUDAH_TOTAL + $DKR_TUVET_SUDAH;

					$SRJ_TUVET_SUDAH = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ")');
					$SRJ_TUVET_SUDAH = $SRJ_TUVET_SUDAH->whereRaw("noKepTuvet <> ''")->whereNotNull('noKepTuvet');
					if ($showHidup)
					{
						$SRJ_TUVET_SUDAH = $SRJ_TUVET_SUDAH->where('tanggalMeninggal', '0000-00-00');
						// $SRJ_TUVET_SUDAH = $SRJ_TUVET_SUDAH->where('statusMeninggal', 'Tidak');
					}
					$SRJ_TUVET_SUDAH = $SRJ_TUVET_SUDAH->count();
					if ($SRJ_TUVET_SUDAH > 0) $list[$key]['SRJ_TUVET_SUDAH'] = $SRJ_TUVET_SUDAH;
					$SRJ_TUVET_SUDAH_TOTAL = $SRJ_TUVET_SUDAH_TOTAL + $SRJ_TUVET_SUDAH;

					## BELUM
					$PKRI_DAHOR_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI"');
					$PKRI_DAHOR_BELUM = $PKRI_DAHOR_BELUM->whereRaw("noKepDahor = ''");
					if ($showHidup)
					{
						$PKRI_DAHOR_BELUM = $PKRI_DAHOR_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $PKRI_DAHOR_BELUM = $PKRI_DAHOR_BELUM->where('statusMeninggal', 'Tidak');
					}
					$PKRI_DAHOR_BELUM = $PKRI_DAHOR_BELUM->count();
					if ($PKRI_DAHOR_BELUM > 0) $list[$key]['PKRI_DAHOR_BELUM'] = $PKRI_DAHOR_BELUM;
					$PKRI_DAHOR_BELUM_TOTAL = $PKRI_DAHOR_BELUM_TOTAL + $PKRI_DAHOR_BELUM;

					$TKR_DAHOR_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR")');
					$TKR_DAHOR_BELUM = $TKR_DAHOR_BELUM->whereRaw("noKepDahor = ''");
					if ($showHidup)
					{
						$TKR_DAHOR_BELUM = $TKR_DAHOR_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $TKR_DAHOR_BELUM = $TKR_DAHOR_BELUM->where('statusMeninggal', 'Tidak');
					}
					$TKR_DAHOR_BELUM = $TKR_DAHOR_BELUM->count();
					if ($TKR_DAHOR_BELUM > 0) $list[$key]['TKR_DAHOR_BELUM'] = $TKR_DAHOR_BELUM;
					$TKR_DAHOR_BELUM_TOTAL = $TKR_DAHOR_BELUM_TOTAL + $TKR_DAHOR_BELUM;

					$DKR_DAHOR_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR")');
					$DKR_DAHOR_BELUM = $DKR_DAHOR_BELUM->whereRaw("noKepDahor = ''");
					if ($showHidup)
					{
						$DKR_DAHOR_BELUM = $DKR_DAHOR_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $DKR_DAHOR_BELUM = $DKR_DAHOR_BELUM->where('statusMeninggal', 'Tidak');
					}
					$DKR_DAHOR_BELUM = $DKR_DAHOR_BELUM->count();
					if ($DKR_DAHOR_BELUM > 0) $list[$key]['DKR_DAHOR_BELUM'] = $DKR_DAHOR_BELUM;
					$DKR_DAHOR_BELUM_TOTAL = $DKR_DAHOR_BELUM_TOTAL + $DKR_DAHOR_BELUM;

					$SRJ_DAHOR_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ")');
					$SRJ_DAHOR_BELUM = $SRJ_DAHOR_BELUM->whereRaw("noKepDahor = ''");
					if ($showHidup)
					{
						$SRJ_DAHOR_BELUM = $SRJ_DAHOR_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $SRJ_DAHOR_BELUM = $SRJ_DAHOR_BELUM->where('statusMeninggal', 'Tidak');
					}
					$SRJ_DAHOR_BELUM = $SRJ_DAHOR_BELUM->count();
					if ($SRJ_DAHOR_BELUM > 0) $list[$key]['SRJ_DAHOR_BELUM'] = $SRJ_DAHOR_BELUM;
					$SRJ_DAHOR_BELUM_TOTAL = $SRJ_DAHOR_BELUM_TOTAL + $SRJ_DAHOR_BELUM;

					$PKRI_TUVET_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI"');
					$PKRI_TUVET_BELUM = $PKRI_TUVET_BELUM->whereRaw("noKepTuvet = ''");
					if ($showHidup)
					{
						$PKRI_TUVET_BELUM = $PKRI_TUVET_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $PKRI_TUVET_BELUM = $PKRI_TUVET_BELUM->where('statusMeninggal', 'Tidak');
					}
					$PKRI_TUVET_BELUM = $PKRI_TUVET_BELUM->count();
					if ($PKRI_TUVET_BELUM > 0) $list[$key]['PKRI_TUVET_BELUM'] = $PKRI_TUVET_BELUM;
					$PKRI_TUVET_BELUM_TOTAL = $PKRI_TUVET_BELUM_TOTAL + $PKRI_TUVET_BELUM;

					$TKR_TUVET_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR")');
					$TKR_TUVET_BELUM = $TKR_TUVET_BELUM->whereRaw("noKepTuvet = ''");
					if ($showHidup)
					{
						$TKR_TUVET_BELUM = $TKR_TUVET_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $TKR_TUVET_BELUM = $TKR_TUVET_BELUM->where('statusMeninggal', 'Tidak');
					}
					$TKR_TUVET_BELUM = $TKR_TUVET_BELUM->count();
					if ($TKR_TUVET_BELUM > 0) $list[$key]['TKR_TUVET_BELUM'] = $TKR_TUVET_BELUM;
					$TKR_TUVET_BELUM_TOTAL = $TKR_TUVET_BELUM_TOTAL + $TKR_TUVET_BELUM;

					$DKR_TUVET_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR")');
					$DKR_TUVET_BELUM = $DKR_TUVET_BELUM->whereRaw("noKepTuvet = ''");
					if ($showHidup)
					{
						$DKR_TUVET_BELUM = $DKR_TUVET_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $DKR_TUVET_BELUM = $DKR_TUVET_BELUM->where('statusMeninggal', 'Tidak');
					}
					$DKR_TUVET_BELUM = $DKR_TUVET_BELUM->count();
					if ($DKR_TUVET_BELUM > 0) $list[$key]['DKR_TUVET_BELUM'] = $DKR_TUVET_BELUM;
					$DKR_TUVET_BELUM_TOTAL = $DKR_TUVET_BELUM_TOTAL + $DKR_TUVET_BELUM;

					$SRJ_TUVET_BELUM = \App\Models\Veteran::where('idKodim', $kodim['idKodim'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ")');
					$SRJ_TUVET_BELUM = $SRJ_TUVET_BELUM->whereRaw("noKepTuvet = ''");
					if ($showHidup)
					{
						$SRJ_TUVET_BELUM = $SRJ_TUVET_BELUM->where('tanggalMeninggal', '0000-00-00');
						// $SRJ_TUVET_BELUM = $SRJ_TUVET_BELUM->where('statusMeninggal', 'Tidak');
					}
					$SRJ_TUVET_BELUM = $SRJ_TUVET_BELUM->count();
					if ($SRJ_TUVET_BELUM > 0) $list[$key]['SRJ_TUVET_BELUM'] = $SRJ_TUVET_BELUM;
					$SRJ_TUVET_BELUM_TOTAL = $SRJ_TUVET_BELUM_TOTAL + $SRJ_TUVET_BELUM;

					$key++;
				}

				$list[$key]['WILAYAH'] = 'JUMLAH';
				$list[$key]['PKRI_DAHOR_SUDAH'] = $PKRI_DAHOR_SUDAH_TOTAL;
                $list[$key]['TKR_DAHOR_SUDAH'] = $TKR_DAHOR_SUDAH_TOTAL;
                $list[$key]['DKR_DAHOR_SUDAH'] = $DKR_DAHOR_SUDAH_TOTAL;
                $list[$key]['SRJ_DAHOR_SUDAH'] = $SRJ_DAHOR_SUDAH_TOTAL;
                $list[$key]['PKRI_TUVET_SUDAH'] = $PKRI_TUVET_SUDAH_TOTAL;
                $list[$key]['TKR_TUVET_SUDAH'] = $TKR_TUVET_SUDAH_TOTAL;
                $list[$key]['DKR_TUVET_SUDAH'] = $DKR_TUVET_SUDAH_TOTAL;
                $list[$key]['SRJ_TUVET_SUDAH'] = $SRJ_TUVET_SUDAH_TOTAL;
                $list[$key]['PKRI_DAHOR_BELUM'] = $PKRI_DAHOR_BELUM_TOTAL;
                $list[$key]['TKR_DAHOR_BELUM'] = $TKR_DAHOR_BELUM_TOTAL;
                $list[$key]['DKR_DAHOR_BELUM'] = $DKR_DAHOR_BELUM_TOTAL;
                $list[$key]['SRJ_DAHOR_BELUM'] = $SRJ_DAHOR_BELUM_TOTAL;
                $list[$key]['PKRI_TUVET_BELUM'] = $PKRI_TUVET_BELUM_TOTAL;
                $list[$key]['TKR_TUVET_BELUM'] = $TKR_TUVET_BELUM_TOTAL;
                $list[$key]['DKR_TUVET_BELUM'] = $DKR_TUVET_BELUM_TOTAL;
                $list[$key]['SRJ_TUVET_BELUM'] = $SRJ_TUVET_BELUM_TOTAL;

			}
		}
		else {
			// JIKA ALL
			$kodams = \App\Models\Kodam::select(['idKodam', 'namaKodam'])->get();
			if (isset($params['idKodam']) && strtoupper($params['idKodam']) != 'ALL')
				$kodams = $kodams->where('idKodam', $params['idKodam']);

			$total['WILAYAH'] = 'JUMLAH';
			$total['PKRI'] = 0;
			$total['TKR'] = 0;
			$total['DKR'] = 0;
			$total['SRJ'] = 0;
			$total['PERDAMAIAN'] = 0;
			$total['ANM'] = 0; // VET ANUMERTA
			$total['L'] = 0;
			$total['P'] = 0;
			$total['< 60'] = 0;
			$total['60 - 70'] = 0;
			$total['> 70'] = 0;
			$total['JUMLAH'] = 0;
			if ($kodams)
			{
				$kodams = $kodams->toArray();
				$key = 0;
				$PKRITotal = 0;
				$TKRTotal = 0;
				$DKRTotal = 0;
				$SRJTotal = 0;
				$PERDAMAIANTotal = 0;
				$ANMTotal = 0;
				$LTotal = 0;
				$PTotal = 0;
				$ageBelowTotal = 0;
				$ageBetweenTotal = 0;
				$ageAboveTotal = 0;
				$jumlahTotal = 0;
				foreach ($kodams as $kodam)
				{
					$list[$key]['WILAYAH'] = $kodam['namaKodam'];
					$list[$key]['PKRI'] = 0;
					$list[$key]['TKR'] = 0;
					$list[$key]['DKR'] = 0;
					$list[$key]['SRJ'] = 0;
					$list[$key]['PERDAMAIAN'] = 0;
					$list[$key]['ANM'] = 0; // VET ANUMERTA
					$list[$key]['L'] = 0;
					$list[$key]['P'] = 0;
					$list[$key]['< 60'] = 0;
					$list[$key]['60 - 70'] = 0;
					$list[$key]['> 70'] = 0;
					$list[$key]['JUMLAH'] = 0;

					/* PREDIKAT */
					$PKRI = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI" ');
					if ($showHidup)
					{
						$PKRI = $PKRI->where('tanggalMeninggal', '0000-00-00');
						// $PKRI = $PKRI->where('statusMeninggal', 'Tidak');
					}
					$PKRI = $PKRI->count();
					if ($PKRI > 0) $list[$key]['PKRI'] = $PKRI;
					$PKRITotal = $PKRITotal + $PKRI;

					$TKR = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR") ');
					if ($showHidup)
					{
						$TKR = $TKR->where('tanggalMeninggal', '0000-00-00');
						// $TKR = $TKR->where('statusMeninggal', 'Tidak');
					}
					$TKR = $TKR->count();
					if ($TKR > 0) $list[$key]['TKR'] = $TKR;
					$TKRTotal = $TKRTotal + $TKR;

					$DKR = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR") ');
					if ($showHidup)
					{
						$DKR = $DKR->where('tanggalMeninggal', '0000-00-00');
						// $DKR = $DKR->where('statusMeninggal', 'Tidak');
					}
					$DKR = $DKR->count();
					if ($DKR > 0) $list[$key]['DKR'] = $DKR;
					$DKRTotal = $DKRTotal + $DKR;

					$SRJ = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ") ');
					if ($showHidup)
					{
						$SRJ = $SRJ->where('tanggalMeninggal', '0000-00-00');
						// $SRJ = $SRJ->where('statusMeninggal', 'Tidak');
					}
					$SRJ = $SRJ->count();
					if ($SRJ > 0) $list[$key]['SRJ'] = $SRJ;
					$SRJTotal = $SRJTotal + $SRJ;

					$PERDAMAIAN = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('REPLACE(predikat, " ", "") = "PERDAMAIAN" ');
					if ($showHidup)
					{
						$PERDAMAIAN = $PERDAMAIAN->where('tanggalMeninggal', '0000-00-00');
						// $PERDAMAIAN = $PERDAMAIAN->where('statusMeninggal', 'Tidak');
					}
					$PERDAMAIAN = $PERDAMAIAN->count();
					if ($PERDAMAIAN > 0) $list[$key]['PERDAMAIAN'] = $PERDAMAIAN;
					$PERDAMAIANTotal = $PERDAMAIANTotal + $PERDAMAIAN;

					// BELUM JELAS (Kalo di itung masuk ke predikat)
					$ANM = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('(REPLACE(predikat, " ", "") = "ANM" OR REPLACE(predikat, " ", "") = "'.$this->Config::get('app.ANM').'") ');
					if ($showHidup)
					{
						$ANM = $ANM->where('tanggalMeninggal', '0000-00-00');
						// $ANM = $ANM->where('statusMeninggal', 'Tidak');
					}
					$ANM = $ANM->count();
					if ($ANM > 0) $list[$key]['ANM'] = $ANM;
					$ANMTotal = $ANMTotal + $ANM;

					/* JENIS KELAMIN */
					$L = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->where('jenisKelamin', 'L');
					if ($showHidup)
					{
						$L = $L->where('tanggalMeninggal', '0000-00-00');
						// $L = $L->where('statusMeninggal', 'Tidak');
					}
					$L = $L->count();
					if ($L > 0) $list[$key]['L'] = $L;
					$LTotal = $LTotal + $L;


					$P = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->where('jenisKelamin', 'P');
					if ($showHidup)
					{
						$P = $P->where('tanggalMeninggal', '0000-00-00');
						// $P = $P->where('statusMeninggal', 'Tidak');
					}
					$P = $P->count();
					if ($P > 0) $list[$key]['P'] = $P;
					$PTotal = $PTotal + $P;

					/* USIA */
					$ageBelow = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) < \'60\'');
					if ($showHidup)
					{
						$ageBelow = $ageBelow->where('tanggalMeninggal', '0000-00-00');
						// $ageBelow = $ageBelow->where('statusMeninggal', 'Tidak');
					}
					$ageBelow = $ageBelow->count();
					if ($ageBelow > 0) $list[$key]['< 60'] = $ageBelow;
					$ageBelowTotal = $ageBelowTotal + $ageBelow;

					$ageBetween = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) >= \'60\'')->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) <= \'70\'');
					if ($showHidup)
					{
						$ageBetween = $ageBetween->where('tanggalMeninggal', '0000-00-00');
						// $ageBetween = $ageBetween->where('statusMeninggal', 'Tidak');
					}
					$ageBetween = $ageBetween->count();
					if ($ageBetween > 0) $list[$key]['60 - 70'] = $ageBetween;
					$ageBetweenTotal = $ageBetweenTotal + $ageBetween;

					$ageAbove = \App\Models\Veteran::where('idKodam', $kodam['idKodam'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) > \'70\'');
					if ($showHidup)
					{
						$ageAbove = $ageAbove->where('tanggalMeninggal', '0000-00-00');
						// $ageAbove = $ageAbove->where('statusMeninggal', 'Tidak');
					}
					$ageAbove = $ageAbove->count();
					if ($ageAbove > 0) $list[$key]['> 70'] = $ageAbove;
					$ageAboveTotal = $ageAboveTotal + $ageAbove;


					/*if (($P + $L) > 0)
						$list[$key]['JUMLAH'] = $P + $L;*/

					// if (($ageBelow + $ageBetween + $ageAbove) > 0)
					// {
						/*$jumlahTotalpredikat = $PKRI + $TKR + $DKR + $SRJ + $PERDAMAIAN + $ANM;
						$jumlahTotalJeniskelamin = $P + $L;
						$jumlahTotalUsia = $ageBelow + $ageBetween + $ageAbove;
						if ($jumlahTotalpredikat > $jumlahTotalJeniskelamin && $jumlahTotalpredikat > $jumlahTotalUsia)
						{
							$list[$key]['JUMLAH'] = $jumlahTotalpredikat;
						}
						elseif ($jumlahTotalJeniskelamin > $jumlahTotalpredikat && $jumlahTotalJeniskelamin > $jumlahTotalUsia)
						{
							$list[$key]['JUMLAH'] = $jumlahTotalJeniskelamin;
						}
						elseif ($jumlahTotalUsia > $jumlahTotalpredikat && $jumlahTotalUsia > $jumlahTotalJeniskelamin) {
							$list[$key]['JUMLAH'] = $jumlahTotalUsia;
						}*/

						$totalJumlah = \App\Models\Veteran::where('idKodam', $kodam['idKodam']);
						if ($showHidup)
						{
							$totalJumlah = $totalJumlah->where('tanggalMeninggal', '0000-00-00');
							// $ageAbove = $ageAbove->where('statusMeninggal', 'Tidak');
						}
						$totalJumlah = $totalJumlah->count();
						$list[$key]['JUMLAH'] = $totalJumlah;
						// print_r($list[$key]['JUMLAH']);
						// $list[$key]['JUMLAH'] = $ageBelow + $ageBetween + $ageAbove;
						$jumlahTotal = $jumlahTotal + $list[$key]['JUMLAH'];
					// }

					$key++;
				}

				if (isset($params['typeData']) && $params['typeData'] == 'tabel')
				{
					$list[$key]['WILAYAH'] = 'JUMLAH';
					$list[$key]['PKRI'] = $PKRITotal;
					$list[$key]['TKR'] = $TKRTotal;
					$list[$key]['DKR'] = $DKRTotal;
					$list[$key]['SRJ'] = $SRJTotal;
					$list[$key]['PERDAMAIAN'] = $PERDAMAIANTotal;
					$list[$key]['ANM'] = $ANMTotal; // VET ANUMERTA
					$list[$key]['L'] = $LTotal;
					$list[$key]['P'] = $PTotal;
					$list[$key]['< 60'] = $ageBelowTotal;
					$list[$key]['60 - 70'] = $ageBetweenTotal;
					$list[$key]['> 70'] = $ageAboveTotal;
					$list[$key]['JUMLAH'] = $jumlahTotal;

					$total['WILAYAH'] = 'JUMLAH';
					$total['PKRI'] = $PKRITotal;
					$total['TKR'] = $TKRTotal;
					$total['DKR'] = $DKRTotal;
					$total['SRJ'] = $SRJTotal;
					$total['PERDAMAIAN'] = $PERDAMAIANTotal;
					$total['ANM'] = $ANMTotal; // VET ANUMERTA
					$total['L'] = $LTotal;
					$total['P'] = $PTotal;
					$total['< 60'] = $ageBelowTotal;
					$total['60 - 70'] = $ageBetweenTotal;
					$total['> 70'] = $ageAboveTotal;
					$total['JUMLAH'] = $jumlahTotal;
				}
				else {
					$totalIndonesia['WILAYAH'] = 'JUMLAH';
					$totalIndonesia['PKRI'] = $PKRITotal;
					$totalIndonesia['TKR'] = $TKRTotal;
					$totalIndonesia['DKR'] = $DKRTotal;
					$totalIndonesia['SRJ'] = $SRJTotal;
					$totalIndonesia['PERDAMAIAN'] = $PERDAMAIANTotal;
					$totalIndonesia['ANM'] = $ANMTotal; // VET ANUMERTA
					$totalIndonesia['L'] = $LTotal;
					$totalIndonesia['P'] = $PTotal;
					$totalIndonesia['< 60'] = $ageBelowTotal;
					$totalIndonesia['60 - 70'] = $ageBetweenTotal;
					$totalIndonesia['> 70'] = $ageAboveTotal;
					$totalIndonesia['JUMLAH'] = $jumlahTotal;

					// JIKA GRAFIK UNTUK GRAFIK KUNING
					unset($total);
					$total['data'] = [];
					$YEAREND = date('Y') + 1;
					$YEARSTART = $YEAREND - 5;
					$total['NAME'] = "JUMLAH VETERAN RI";
					for ($year=($YEARSTART + 1); $year <= $YEAREND; $year++)
					{
						$total['data'][$year]['Pertambahan'] = 0;
						$total['data'][$year]['Kematian'] = 0;
						$total['data'][$year]['Total'] = 0;

						$Pertambahan = \App\Models\Veteran::whereYear('tanggalKeputusan', '=', $year);
						// $Pertambahan = $Pertambahan->whereYear('tanggalMeninggal', '<', $year);
						$Pertambahan = $Pertambahan->count();
						if ($Pertambahan > 0)
							$total['data'][$year]['Pertambahan'] = $Pertambahan;

						$Kematian = \App\Models\Veteran::whereYear('tanggalMeninggal', '=', $year);
						// $Kematian = $Kematian->where('tanggalMeninggal', '0000-00-00');
						$Kematian = $Kematian->count();
						if ($Kematian > 0)
							$total['data'][$year]['Kematian'] = $Kematian;

						$Total = \App\Models\Veteran::whereYear('tanggalKeputusan', '<=', $year);
						// $Total = $Total->where('tanggalMeninggal', '0000-00-00');
						$Total = $Total->count();
						if ($Total > 0)
							$total['data'][$year]['Total'] = $Total;
					}
				}
			}
		}

		return array('list' => $list, 'total' => $total, 'totalIndonesia' => $totalIndonesia);
	}

	public function rekapPerkodam(Request $request)
	{
		$params = parseParameter($request->parameter);
		$params['typeData'] = isset($request->typeData)?$request->typeData:'';
		if (isset($params['idKodam']) && strtoupper($params['idKodam']) != 'ALL')
		{
			$params['titleRekap'] = 'REKAPITULASI DATA VETERAN RI YANG SUDAH/BELUM MENERIMA DAHOR DAN TUVET TA. '.date('Y');
		}
		else {
			$params['titleRekap'] = 'DIREKTORAT JENDERAL POTENSI PERTAHANAN DIREKTORAT VETERAN <br>REKAPITULASI KEKUATAN VETERAN YANG MASIH HIDUP TA. '.date('Y');
		}
		$result = $this->getDataRekapKodam($params);
		$list = $result['list'];
		$total = $result['total'];
		$totalIndonesia = $result['totalIndonesia'];
        return view('dashboard.rekap.kodam', compact('list', 'total', 'totalIndonesia', 'params'));
	}

	public function rekapPerkodamNewTab($tipeData = '', $id = '')
	{
		$params['idKodam'] = $id;
		$params['typeData'] = $tipeData;
		if (isset($params['idKodam']) && strtoupper($params['idKodam']) != 'ALL')
		{
			$params['titleRekap'] = 'REKAPITULASI DATA VETERAN RI YANG SUDAH/BELUM MENERIMA DAHOR DAN TUVET TA. '.date('Y');
		}
		else {
			$params['titleRekap'] = 'DIREKTORAT JENDERAL POTENSI PERTAHANAN DIREKTORAT VETERAN <br>REKAPITULASI KEKUATAN VETERAN YANG MASIH HIDUP TA. '.date('Y');
		}
		$result = $this->getDataRekapKodam($params);
		$list = $result['list'];
		$total = $result['total'];
		$totalIndonesia = $result['totalIndonesia'];
		// print_r($list);die;
        return view('dashboard.rekap.kodamnewtab', compact('list', 'total', 'totalIndonesia', 'params'));
	}

	public function rekapPerkodamPDF(Request $request)
	{
		$data['html'] = $request->html;
		$data['tanggalTtd'] = formatDate(date('Y-m-d'));
		$data['brigadir'] = 'Herman Djatmiko';
        // return view('pdf.rekapPerkodam', $data);
		$pdf = PDF::loadView('pdf.rekapPerkodam', $data)->setPaper('a4', 'landscape');
		$pdf = $pdf->setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
		return $pdf->download('Rekap.pdf');
	}

	public function getDataRekapProvinsiBUUUU($params = array())
	{
		$list = [];
        $showHidup = true; // hanya show yang hidup
		/*if (isset($params['idProvinsi']) && strtoupper($params['idProvinsi']) != 'ALL')
		{
		}
		else {*/
			// JIKA ALL
			$provinsis = \App\Models\Provinsi::select(['idProvinsi', 'namaProvinsi'])->get();
			if (isset($params['idProvinsi']) && strtoupper($params['idProvinsi']) != 'ALL')
				$provinsis = $provinsis->where('idProvinsi', $params['idProvinsi']);

			$total['WILAYAH'] = 'JUMLAH';
			$total['PKRI'] = 0;
			$total['TKR'] = 0;
			$total['DKR'] = 0;
			$total['SRJ'] = 0;
			$total['PERDAMAIAN'] = 0;
			$total['ANM'] = 0; // VET ANUMERTA
			$total['L'] = 0;
			$total['P'] = 0;
			$total['< 60'] = 0;
			$total['60 - 70'] = 0;
			$total['> 70'] = 0;
			$total['JUMLAH'] = 0;
			if ($provinsis)
			{
				$provinsis = $provinsis->toArray();
				$key = 0;
				$PKRITotal = 0;
				$TKRTotal = 0;
				$DKRTotal = 0;
				$SRJTotal = 0;
				$PERDAMAIANTotal = 0;
				$ANMTotal = 0;
				$LTotal = 0;
				$PTotal = 0;
				$ageBelowTotal = 0;
				$ageBetweenTotal = 0;
				$ageAboveTotal = 0;
				$jumlahTotal = 0;
				foreach ($provinsis as $provinsi)
				{
					$list[$key]['WILAYAH'] = $provinsi['namaProvinsi'];
					$list[$key]['PKRI'] = 0;
					$list[$key]['TKR'] = 0;
					$list[$key]['DKR'] = 0;
					$list[$key]['SRJ'] = 0;
					$list[$key]['PERDAMAIAN'] = 0;
					$list[$key]['ANM'] = 0; // VET ANUMERTA
					$list[$key]['L'] = 0;
					$list[$key]['P'] = 0;
					$list[$key]['< 60'] = 0;
					$list[$key]['60 - 70'] = 0;
					$list[$key]['> 70'] = 0;
					$list[$key]['JUMLAH'] = 0;

					/* PREDIKAT */
					$PKRI = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI" ');
					if ($showHidup)
					{
						$PKRI = $PKRI->where('tanggalMeninggal', '0000-00-00');
						// $PKRI = $PKRI->where('statusMeninggal', 'Tidak');
					}
					$PKRI = $PKRI->count();
					if ($PKRI > 0) $list[$key]['PKRI'] = $PKRI;
					$PKRITotal = $PKRITotal + $PKRI;

					$TKR = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR") ');
					if ($showHidup)
					{
						$TKR = $TKR->where('tanggalMeninggal', '0000-00-00');
						// $TKR = $TKR->where('statusMeninggal', 'Tidak');
					}
					$TKR = $TKR->count();
					if ($TKR > 0) $list[$key]['TKR'] = $TKR;
					$TKRTotal = $TKRTotal + $TKR;

					$DKR = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR") ');
					if ($showHidup)
					{
						$DKR = $DKR->where('tanggalMeninggal', '0000-00-00');
						// $DKR = $DKR->where('statusMeninggal', 'Tidak');
					}
					$DKR = $DKR->count();
					if ($DKR > 0) $list[$key]['DKR'] = $DKR;
					$DKRTotal = $DKRTotal + $DKR;

					$SRJ = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ") ');
					if ($showHidup)
					{
						$SRJ = $SRJ->where('tanggalMeninggal', '0000-00-00');
						// $SRJ = $SRJ->where('statusMeninggal', 'Tidak');
					}
					$SRJ = $SRJ->count();
					if ($SRJ > 0) $list[$key]['SRJ'] = $SRJ;
					$SRJTotal = $SRJTotal + $SRJ;

					$PERDAMAIAN = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('REPLACE(predikat, " ", "") = "PERDAMAIAN" ');
					if ($showHidup)
					{
						$PERDAMAIAN = $PERDAMAIAN->where('tanggalMeninggal', '0000-00-00');
						// $PERDAMAIAN = $PERDAMAIAN->where('statusMeninggal', 'Tidak');
					}
					$PERDAMAIAN = $PERDAMAIAN->count();
					if ($PERDAMAIAN > 0) $list[$key]['PERDAMAIAN'] = $PERDAMAIAN;
					$PERDAMAIANTotal = $PERDAMAIANTotal + $PERDAMAIAN;

					// BELUM JELAS (Kalo di itung masuk ke predikat)
					$ANM = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "ANM" OR REPLACE(predikat, " ", "") = "'.$this->Config::get('app.ANM').'") ');
					if ($showHidup)
					{
						$ANM = $ANM->where('tanggalMeninggal', '0000-00-00');
						// $ANM = $ANM->where('statusMeninggal', 'Tidak');
					}
					$ANM = $ANM->count();
					if ($ANM > 0) $list[$key]['ANM'] = $ANM;
					$ANMTotal = $ANMTotal + $ANM;

					/* JENIS KELAMIN */
					$L = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->where('jenisKelamin', 'L');
					if ($showHidup)
					{
						$L = $L->where('tanggalMeninggal', '0000-00-00');
						// $L = $L->where('statusMeninggal', 'Tidak');
					}
					$L = $L->count();
					if ($L > 0) $list[$key]['L'] = $L;
					$LTotal = $LTotal + $L;


					$P = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->where('jenisKelamin', 'P');
					if ($showHidup)
					{
						$P = $P->where('tanggalMeninggal', '0000-00-00');
						// $P = $P->where('statusMeninggal', 'Tidak');
					}
					$P = $P->count();
					if ($P > 0) $list[$key]['P'] = $P;
					$PTotal = $PTotal + $P;

					/* USIA */
					$ageBelow = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) < \'60\'');
					if ($showHidup)
					{
						$ageBelow = $ageBelow->where('tanggalMeninggal', '0000-00-00');
						// $ageBelow = $ageBelow->where('statusMeninggal', 'Tidak');
					}
					$ageBelow = $ageBelow->count();
					if ($ageBelow > 0) $list[$key]['< 60'] = $ageBelow;
					$ageBelowTotal = $ageBelowTotal + $ageBelow;

					$ageBetween = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) >= \'60\'')->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) <= \'70\'');
					if ($showHidup)
					{
						$ageBetween = $ageBetween->where('tanggalMeninggal', '0000-00-00');
						// $ageBetween = $ageBetween->where('statusMeninggal', 'Tidak');
					}
					$ageBetween = $ageBetween->count();
					if ($ageBetween > 0) $list[$key]['60 - 70'] = $ageBetween;
					$ageBetweenTotal = $ageBetweenTotal + $ageBetween;

					$ageAbove = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) > \'70\'');
					if ($showHidup)
					{
						$ageAbove = $ageAbove->where('tanggalMeninggal', '0000-00-00');
						// $ageAbove = $ageAbove->where('statusMeninggal', 'Tidak');
					}
					$ageAbove = $ageAbove->count();
					if ($ageAbove > 0) $list[$key]['> 70'] = $ageAbove;
					$ageAboveTotal = $ageAboveTotal + $ageAbove;


					/*if (($P + $L) > 0)
						$list[$key]['JUMLAH'] = $P + $L;*/

					if (($ageBelow + $ageBetween + $ageAbove) > 0)
					{
						$list[$key]['JUMLAH'] = $ageBelow + $ageBetween + $ageAbove;
						$jumlahTotal = $jumlahTotal + $list[$key]['JUMLAH'];
					}

					$key++;
				}

				if ($params['typeData'] == 'tabel' && $params['idProvinsi'] == 'all')
				{
					$list[$key]['WILAYAH'] = 'JUMLAH';
					$list[$key]['PKRI'] = $PKRITotal;
					$list[$key]['TKR'] = $TKRTotal;
					$list[$key]['DKR'] = $DKRTotal;
					$list[$key]['SRJ'] = $SRJTotal;
					$list[$key]['PERDAMAIAN'] = $PERDAMAIANTotal;
					$list[$key]['ANM'] = $ANMTotal; // VET ANUMERTA
					$list[$key]['L'] = $LTotal;
					$list[$key]['P'] = $PTotal;
					$list[$key]['< 60'] = $ageBelowTotal;
					$list[$key]['60 - 70'] = $ageBetweenTotal;
					$list[$key]['> 70'] = $ageAboveTotal;
					$list[$key]['JUMLAH'] = $jumlahTotal;
				}
				else {
					// JIKA GRAFIK UNTUK GRAFIK KUNING
					unset($total);
					$total['data'] = [];
					$YEAREND = date('Y') + 1;
					$YEARSTART = $YEAREND - 5;
					$total['NAME'] = "JUMLAH VETERAN RI";
					for ($year=($YEARSTART + 1); $year <= $YEAREND; $year++)
					{
						$total['data'][$year]['Pertambahan'] = 0;
						$total['data'][$year]['Kematian'] = 0;
						$total['data'][$year]['Total'] = 0;

						$Pertambahan = \App\Models\Veteran::whereYear('tanggalKeputusan', '=', $year);
						// $Pertambahan = $Pertambahan->whereYear('tanggalMeninggal', '<', $year);
						$Pertambahan = $Pertambahan->count();
						if ($Pertambahan > 0)
							$total['data'][$year]['Pertambahan'] = $Pertambahan;

						$Kematian = \App\Models\Veteran::whereYear('tanggalMeninggal', '=', $year);
						// $Kematian = $Kematian->where('tanggalMeninggal', '0000-00-00');
						$Kematian = $Kematian->count();
						if ($Kematian > 0)
							$total['data'][$year]['Kematian'] = $Kematian;

						$Total = \App\Models\Veteran::whereYear('tanggalKeputusan', '<=', $year);
						// $Total = $Total->where('tanggalMeninggal', '0000-00-00');
						$Total = $Total->count();
						if ($Total > 0)
							$total['data'][$year]['Total'] = $Total;
					}
				}
				$total['WILAYAH'] = 'JUMLAH';
				$total['PKRI'] = $PKRITotal;
				$total['TKR'] = $TKRTotal;
				$total['DKR'] = $DKRTotal;
				$total['SRJ'] = $SRJTotal;
				$total['PERDAMAIAN'] = $PERDAMAIANTotal;
				$total['ANM'] = $ANMTotal; // VET ANUMERTA
				$total['L'] = $LTotal;
				$total['P'] = $PTotal;
				$total['< 60'] = $ageBelowTotal;
				$total['60 - 70'] = $ageBetweenTotal;
				$total['> 70'] = $ageAboveTotal;
				$total['JUMLAH'] = $jumlahTotal;
			}
		// }
		return array('list' => $list, 'total' => $total);
	}

	public function getDataRekapProvinsi($params = array())
	{
		$list = [];
        $totalIndonesia = [];
        $showHidup = true; // hanya show yang hidup
		if (isset($params['idProvinsi']) && strtoupper($params['idProvinsi']) != 'ALL')
		{
			// JIKA ALL
			$kabupatenList = \App\Models\Kabupaten::select(['idKabupaten', 'namaKabupaten']);
			if (isset($params['idProvinsi']) && strtoupper($params['idProvinsi']) != 'ALL')
				$kabupatenList = $kabupatenList->where('idProvinsi', $params['idProvinsi']);
			$kabupatenList = $kabupatenList->orderBy('namaKabupaten');
			$kabupatenList = $kabupatenList->get();

			$total['WILAYAH'] = 'JUMLAH';
			$total['PKRI'] = 0;
			$total['TKR'] = 0;
			$total['DKR'] = 0;
			$total['SRJ'] = 0;
			$total['PERDAMAIAN'] = 0;
			$total['ANM'] = 0; // VET ANUMERTA
			$total['L'] = 0;
			$total['P'] = 0;
			$total['< 60'] = 0;
			$total['60 - 70'] = 0;
			$total['> 70'] = 0;
			$total['JUMLAH'] = 0;
			if ($kabupatenList)
			{
				$kabupatenList = $kabupatenList->toArray();

				$key = 0;
				$PKRITotal = 0;
				$TKRTotal = 0;
				$DKRTotal = 0;
				$SRJTotal = 0;
				$PERDAMAIANTotal = 0;
				$ANMTotal = 0;
				$LTotal = 0;
				$PTotal = 0;
				$ageBelowTotal = 0;
				$ageBetweenTotal = 0;
				$ageAboveTotal = 0;
				$jumlahTotal = 0;
				foreach ($kabupatenList as $kabupaten)
				{
					$list[$key]['WILAYAH'] = $kabupaten['namaKabupaten'];
					$list[$key]['PKRI'] = 0;
					$list[$key]['TKR'] = 0;
					$list[$key]['DKR'] = 0;
					$list[$key]['SRJ'] = 0;
					$list[$key]['PERDAMAIAN'] = 0;
					$list[$key]['ANM'] = 0; // VET ANUMERTA
					$list[$key]['L'] = 0;
					$list[$key]['P'] = 0;
					$list[$key]['< 60'] = 0;
					$list[$key]['60 - 70'] = 0;
					$list[$key]['> 70'] = 0;
					$list[$key]['JUMLAH'] = 0;

					/* PREDIKAT */
					$PKRI = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI" ');
					if ($showHidup)
					{
						$PKRI = $PKRI->where('tanggalMeninggal', '0000-00-00');
						// $PKRI = $PKRI->where('statusMeninggal', 'Tidak');
					}
					$PKRI = $PKRI->count();
					if ($PKRI > 0) $list[$key]['PKRI'] = $PKRI;
					$PKRITotal = $PKRITotal + $PKRI;

					$TKR = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR") ');
					if ($showHidup)
					{
						$TKR = $TKR->where('tanggalMeninggal', '0000-00-00');
						// $TKR = $TKR->where('statusMeninggal', 'Tidak');
					}
					$TKR = $TKR->count();
					if ($TKR > 0) $list[$key]['TKR'] = $TKR;
					$TKRTotal = $TKRTotal + $TKR;

					$DKR = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR") ');
					if ($showHidup)
					{
						$DKR = $DKR->where('tanggalMeninggal', '0000-00-00');
						// $DKR = $DKR->where('statusMeninggal', 'Tidak');
					}
					$DKR = $DKR->count();
					if ($DKR > 0) $list[$key]['DKR'] = $DKR;
					$DKRTotal = $DKRTotal + $DKR;

					$SRJ = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ") ');
					if ($showHidup)
					{
						$SRJ = $SRJ->where('tanggalMeninggal', '0000-00-00');
						// $SRJ = $SRJ->where('statusMeninggal', 'Tidak');
					}
					$SRJ = $SRJ->count();
					if ($SRJ > 0) $list[$key]['SRJ'] = $SRJ;
					$SRJTotal = $SRJTotal + $SRJ;

					$PERDAMAIAN = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('REPLACE(predikat, " ", "") = "PERDAMAIAN" ');
					if ($showHidup)
					{
						$PERDAMAIAN = $PERDAMAIAN->where('tanggalMeninggal', '0000-00-00');
						// $PERDAMAIAN = $PERDAMAIAN->where('statusMeninggal', 'Tidak');
					}
					$PERDAMAIAN = $PERDAMAIAN->count();
					if ($PERDAMAIAN > 0) $list[$key]['PERDAMAIAN'] = $PERDAMAIAN;
					$PERDAMAIANTotal = $PERDAMAIANTotal + $PERDAMAIAN;

					// BELUM JELAS (Kalo di itung masuk ke predikat)
					$ANM = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('(REPLACE(predikat, " ", "") = "ANM" OR REPLACE(predikat, " ", "") = "'.$this->Config::get('app.ANM').'") ');
					if ($showHidup)
					{
						$ANM = $ANM->where('tanggalMeninggal', '0000-00-00');
						// $ANM = $ANM->where('statusMeninggal', 'Tidak');
					}
					$ANM = $ANM->count();
					if ($ANM > 0) $list[$key]['ANM'] = $ANM;
					$ANMTotal = $ANMTotal + $ANM;

					/* JENIS KELAMIN */
					$L = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->where('jenisKelamin', 'L');
					if ($showHidup)
					{
						$L = $L->where('tanggalMeninggal', '0000-00-00');
						// $L = $L->where('statusMeninggal', 'Tidak');
					}
					$L = $L->count();
					if ($L > 0) $list[$key]['L'] = $L;
					$LTotal = $LTotal + $L;


					$P = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->where('jenisKelamin', 'P');
					if ($showHidup)
					{
						$P = $P->where('tanggalMeninggal', '0000-00-00');
						// $P = $P->where('statusMeninggal', 'Tidak');
					}
					$P = $P->count();
					if ($P > 0) $list[$key]['P'] = $P;
					$PTotal = $PTotal + $P;

					/* USIA */
					$ageBelow = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) < \'60\'');
					if ($showHidup)
					{
						$ageBelow = $ageBelow->where('tanggalMeninggal', '0000-00-00');
						// $ageBelow = $ageBelow->where('statusMeninggal', 'Tidak');
					}
					$ageBelow = $ageBelow->count();
					if ($ageBelow > 0) $list[$key]['< 60'] = $ageBelow;
					$ageBelowTotal = $ageBelowTotal + $ageBelow;

					$ageBetween = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) >= \'60\'')->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) <= \'70\'');
					if ($showHidup)
					{
						$ageBetween = $ageBetween->where('tanggalMeninggal', '0000-00-00');
						// $ageBetween = $ageBetween->where('statusMeninggal', 'Tidak');
					}
					$ageBetween = $ageBetween->count();
					if ($ageBetween > 0) $list[$key]['60 - 70'] = $ageBetween;
					$ageBetweenTotal = $ageBetweenTotal + $ageBetween;

					$ageAbove = \App\Models\Veteran::where('idKabupaten', $kabupaten['idKabupaten'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) > \'70\'');
					if ($showHidup)
					{
						$ageAbove = $ageAbove->where('tanggalMeninggal', '0000-00-00');
						// $ageAbove = $ageAbove->where('statusMeninggal', 'Tidak');
					}
					$ageAbove = $ageAbove->count();
					if ($ageAbove > 0) $list[$key]['> 70'] = $ageAbove;
					$ageAboveTotal = $ageAboveTotal + $ageAbove;


					/*if (($P + $L) > 0)
						$list[$key]['JUMLAH'] = $P + $L;*/

					if (($ageBelow + $ageBetween + $ageAbove) > 0)
					{
						$list[$key]['JUMLAH'] = $ageBelow + $ageBetween + $ageAbove;
						$jumlahTotal = $jumlahTotal + $list[$key]['JUMLAH'];
					}

					$key++;
				}

				if ($params['typeData'] == 'tabel')
				// if ($params['typeData'] == 'tabel' && $params['idProvinsi'] == 'all')
				{
					$list[$key]['WILAYAH'] = 'JUMLAH';
					$list[$key]['PKRI'] = $PKRITotal;
					$list[$key]['TKR'] = $TKRTotal;
					$list[$key]['DKR'] = $DKRTotal;
					$list[$key]['SRJ'] = $SRJTotal;
					$list[$key]['PERDAMAIAN'] = $PERDAMAIANTotal;
					$list[$key]['ANM'] = $ANMTotal; // VET ANUMERTA
					$list[$key]['L'] = $LTotal;
					$list[$key]['P'] = $PTotal;
					$list[$key]['< 60'] = $ageBelowTotal;
					$list[$key]['60 - 70'] = $ageBetweenTotal;
					$list[$key]['> 70'] = $ageAboveTotal;
					$list[$key]['JUMLAH'] = $jumlahTotal;
				}
				else {
					// JIKA GRAFIK UNTUK GRAFIK KUNING
					unset($total);
					$total['data'] = [];
					$YEAREND = date('Y') + 1;
					$YEARSTART = $YEAREND - 5;
					$total['NAME'] = "JUMLAH VETERAN RI";
					for ($year=($YEARSTART + 1); $year <= $YEAREND; $year++)
					{
						$total['data'][$year]['Pertambahan'] = 0;
						$total['data'][$year]['Kematian'] = 0;
						$total['data'][$year]['Total'] = 0;

						$Pertambahan = \App\Models\Veteran::whereYear('tanggalKeputusan', '=', $year);
						// $Pertambahan = $Pertambahan->whereYear('tanggalMeninggal', '<', $year);
						$Pertambahan = $Pertambahan->count();
						if ($Pertambahan > 0)
							$total['data'][$year]['Pertambahan'] = $Pertambahan;

						$Kematian = \App\Models\Veteran::whereYear('tanggalMeninggal', '=', $year);
						// $Kematian = $Kematian->where('tanggalMeninggal', '0000-00-00');
						$Kematian = $Kematian->count();
						if ($Kematian > 0)
							$total['data'][$year]['Kematian'] = $Kematian;

						$Total = \App\Models\Veteran::whereYear('tanggalKeputusan', '<=', $year);
						// $Total = $Total->where('tanggalMeninggal', '0000-00-00');
						$Total = $Total->count();
						if ($Total > 0)
							$total['data'][$year]['Total'] = $Total;
					}
				}

				$total['WILAYAH'] = 'JUMLAH';
				$total['PKRI'] = $PKRITotal;
				$total['TKR'] = $TKRTotal;
				$total['DKR'] = $DKRTotal;
				$total['SRJ'] = $SRJTotal;
				$total['PERDAMAIAN'] = $PERDAMAIANTotal;
				$total['ANM'] = $ANMTotal; // VET ANUMERTA
				$total['L'] = $LTotal;
				$total['P'] = $PTotal;
				$total['< 60'] = $ageBelowTotal;
				$total['60 - 70'] = $ageBetweenTotal;
				$total['> 70'] = $ageAboveTotal;
				$total['JUMLAH'] = $jumlahTotal;
			}
		}
		else {
			// JIKA ALL
			$provinsis = \App\Models\Provinsi::select(['idProvinsi', 'namaProvinsi'])->get();
			if (isset($params['idProvinsi']) && strtoupper($params['idProvinsi']) != 'ALL')
				$provinsis = $provinsis->where('idProvinsi', $params['idProvinsi']);

			$total['WILAYAH'] = 'JUMLAH';
			$total['PKRI'] = 0;
			$total['TKR'] = 0;
			$total['DKR'] = 0;
			$total['SRJ'] = 0;
			$total['PERDAMAIAN'] = 0;
			$total['ANM'] = 0; // VET ANUMERTA
			$total['L'] = 0;
			$total['P'] = 0;
			$total['< 60'] = 0;
			$total['60 - 70'] = 0;
			$total['> 70'] = 0;
			$total['JUMLAH'] = 0;
			if ($provinsis)
			{
				$provinsis = $provinsis->toArray();
				$key = 0;
				$PKRITotal = 0;
				$TKRTotal = 0;
				$DKRTotal = 0;
				$SRJTotal = 0;
				$PERDAMAIANTotal = 0;
				$ANMTotal = 0;
				$LTotal = 0;
				$PTotal = 0;
				$ageBelowTotal = 0;
				$ageBetweenTotal = 0;
				$ageAboveTotal = 0;
				$jumlahTotal = 0;
				foreach ($provinsis as $provinsi)
				{
					$list[$key]['WILAYAH'] = $provinsi['namaProvinsi'];
					$list[$key]['PKRI'] = 0;
					$list[$key]['TKR'] = 0;
					$list[$key]['DKR'] = 0;
					$list[$key]['SRJ'] = 0;
					$list[$key]['PERDAMAIAN'] = 0;
					$list[$key]['ANM'] = 0; // VET ANUMERTA
					$list[$key]['L'] = 0;
					$list[$key]['P'] = 0;
					$list[$key]['< 60'] = 0;
					$list[$key]['60 - 70'] = 0;
					$list[$key]['> 70'] = 0;
					$list[$key]['JUMLAH'] = 0;

					/* PREDIKAT */
					$PKRI = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('REPLACE(predikat, " ", "") = "PKRI" ');
					if ($showHidup)
					{
						$PKRI = $PKRI->where('tanggalMeninggal', '0000-00-00');
						// $PKRI = $PKRI->where('statusMeninggal', 'Tidak');
					}
					$PKRI = $PKRI->count();
					if ($PKRI > 0) $list[$key]['PKRI'] = $PKRI;
					$PKRITotal = $PKRITotal + $PKRI;

					$TKR = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.TKR').'" OR REPLACE(predikat, " ", "") = "TKR") ');
					if ($showHidup)
					{
						$TKR = $TKR->where('tanggalMeninggal', '0000-00-00');
						// $TKR = $TKR->where('statusMeninggal', 'Tidak');
					}
					$TKR = $TKR->count();
					if ($TKR > 0) $list[$key]['TKR'] = $TKR;
					$TKRTotal = $TKRTotal + $TKR;

					$DKR = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.DKR').'" OR REPLACE(predikat, " ", "") = "DKR") ');
					if ($showHidup)
					{
						$DKR = $DKR->where('tanggalMeninggal', '0000-00-00');
						// $DKR = $DKR->where('statusMeninggal', 'Tidak');
					}
					$DKR = $DKR->count();
					if ($DKR > 0) $list[$key]['DKR'] = $DKR;
					$DKRTotal = $DKRTotal + $DKR;

					$SRJ = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "'.$this->Config::get('app.pembela.SRJ').'" OR REPLACE(predikat, " ", "") = "SRJ") ');
					if ($showHidup)
					{
						$SRJ = $SRJ->where('tanggalMeninggal', '0000-00-00');
						// $SRJ = $SRJ->where('statusMeninggal', 'Tidak');
					}
					$SRJ = $SRJ->count();
					if ($SRJ > 0) $list[$key]['SRJ'] = $SRJ;
					$SRJTotal = $SRJTotal + $SRJ;

					$PERDAMAIAN = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('REPLACE(predikat, " ", "") = "PERDAMAIAN" ');
					if ($showHidup)
					{
						$PERDAMAIAN = $PERDAMAIAN->where('tanggalMeninggal', '0000-00-00');
						// $PERDAMAIAN = $PERDAMAIAN->where('statusMeninggal', 'Tidak');
					}
					$PERDAMAIAN = $PERDAMAIAN->count();
					if ($PERDAMAIAN > 0) $list[$key]['PERDAMAIAN'] = $PERDAMAIAN;
					$PERDAMAIANTotal = $PERDAMAIANTotal + $PERDAMAIAN;

					// BELUM JELAS (Kalo di itung masuk ke predikat)
					$ANM = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(REPLACE(predikat, " ", "") = "ANM" OR REPLACE(predikat, " ", "") = "'.$this->Config::get('app.ANM').'") ');
					if ($showHidup)
					{
						$ANM = $ANM->where('tanggalMeninggal', '0000-00-00');
						// $ANM = $ANM->where('statusMeninggal', 'Tidak');
					}
					$ANM = $ANM->count();
					if ($ANM > 0) $list[$key]['ANM'] = $ANM;
					$ANMTotal = $ANMTotal + $ANM;

					/* JENIS KELAMIN */
					$L = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->where('jenisKelamin', 'L');
					if ($showHidup)
					{
						$L = $L->where('tanggalMeninggal', '0000-00-00');
						// $L = $L->where('statusMeninggal', 'Tidak');
					}
					$L = $L->count();
					if ($L > 0) $list[$key]['L'] = $L;
					$LTotal = $LTotal + $L;


					$P = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->where('jenisKelamin', 'P');
					if ($showHidup)
					{
						$P = $P->where('tanggalMeninggal', '0000-00-00');
						// $P = $P->where('statusMeninggal', 'Tidak');
					}
					$P = $P->count();
					if ($P > 0) $list[$key]['P'] = $P;
					$PTotal = $PTotal + $P;

					/* USIA */
					$ageBelow = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) < \'60\'');
					if ($showHidup)
					{
						$ageBelow = $ageBelow->where('tanggalMeninggal', '0000-00-00');
						// $ageBelow = $ageBelow->where('statusMeninggal', 'Tidak');
					}
					$ageBelow = $ageBelow->count();
					if ($ageBelow > 0) $list[$key]['< 60'] = $ageBelow;
					$ageBelowTotal = $ageBelowTotal + $ageBelow;

					$ageBetween = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) >= \'60\'')->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) <= \'70\'');
					if ($showHidup)
					{
						$ageBetween = $ageBetween->where('tanggalMeninggal', '0000-00-00');
						// $ageBetween = $ageBetween->where('statusMeninggal', 'Tidak');
					}
					$ageBetween = $ageBetween->count();
					if ($ageBetween > 0) $list[$key]['60 - 70'] = $ageBetween;
					$ageBetweenTotal = $ageBetweenTotal + $ageBetween;

					$ageAbove = \App\Models\Veteran::where('idProvinsi', $provinsi['idProvinsi'])->whereRaw('(DATEDIFF(CURRENT_DATE, STR_TO_DATE(tanggalLahir, \'%Y-%m-%d\'))/365) > \'70\'');
					if ($showHidup)
					{
						$ageAbove = $ageAbove->where('tanggalMeninggal', '0000-00-00');
						// $ageAbove = $ageAbove->where('statusMeninggal', 'Tidak');
					}
					$ageAbove = $ageAbove->count();
					if ($ageAbove > 0) $list[$key]['> 70'] = $ageAbove;
					$ageAboveTotal = $ageAboveTotal + $ageAbove;


					/*if (($P + $L) > 0)
						$list[$key]['JUMLAH'] = $P + $L;*/

					if (($ageBelow + $ageBetween + $ageAbove) > 0)
					{
						$list[$key]['JUMLAH'] = $ageBelow + $ageBetween + $ageAbove;
						$jumlahTotal = $jumlahTotal + $list[$key]['JUMLAH'];
					}

					$key++;
				}

				if ($params['typeData'] == 'tabel' && $params['idProvinsi'] == 'all')
				{
					$list[$key]['WILAYAH'] = 'JUMLAH';
					$list[$key]['PKRI'] = $PKRITotal;
					$list[$key]['TKR'] = $TKRTotal;
					$list[$key]['DKR'] = $DKRTotal;
					$list[$key]['SRJ'] = $SRJTotal;
					$list[$key]['PERDAMAIAN'] = $PERDAMAIANTotal;
					$list[$key]['ANM'] = $ANMTotal; // VET ANUMERTA
					$list[$key]['L'] = $LTotal;
					$list[$key]['P'] = $PTotal;
					$list[$key]['< 60'] = $ageBelowTotal;
					$list[$key]['60 - 70'] = $ageBetweenTotal;
					$list[$key]['> 70'] = $ageAboveTotal;
					$list[$key]['JUMLAH'] = $jumlahTotal;
				}
				else {

					// JIKA GRAFIK UNTUK GRAFIK KUNING
					unset($total);
					$total['data'] = [];
					$YEAREND = date('Y') + 1;
					$YEARSTART = $YEAREND - 5;
					$total['NAME'] = "JUMLAH VETERAN RI";
					for ($year=($YEARSTART + 1); $year <= $YEAREND; $year++)
					{
						$total['data'][$year]['Pertambahan'] = 0;
						$total['data'][$year]['Kematian'] = 0;
						$total['data'][$year]['Total'] = 0;

						$Pertambahan = \App\Models\Veteran::whereYear('tanggalKeputusan', '=', $year);
						// $Pertambahan = $Pertambahan->whereYear('tanggalMeninggal', '<', $year);
						$Pertambahan = $Pertambahan->count();
						if ($Pertambahan > 0)
							$total['data'][$year]['Pertambahan'] = $Pertambahan;

						$Kematian = \App\Models\Veteran::whereYear('tanggalMeninggal', '=', $year);
						// $Kematian = $Kematian->where('tanggalMeninggal', '0000-00-00');
						$Kematian = $Kematian->count();
						if ($Kematian > 0)
							$total['data'][$year]['Kematian'] = $Kematian;

						$Total = \App\Models\Veteran::whereYear('tanggalKeputusan', '<=', $year);
						// $Total = $Total->where('tanggalMeninggal', '0000-00-00');
						$Total = $Total->count();
						if ($Total > 0)
							$total['data'][$year]['Total'] = $Total;
					}
				}

				$totalIndonesia['WILAYAH'] = 'JUMLAH';
				$totalIndonesia['PKRI'] = $PKRITotal;
				$totalIndonesia['TKR'] = $TKRTotal;
				$totalIndonesia['DKR'] = $DKRTotal;
				$totalIndonesia['SRJ'] = $SRJTotal;
				$totalIndonesia['PERDAMAIAN'] = $PERDAMAIANTotal;
				$totalIndonesia['ANM'] = $ANMTotal; // VET ANUMERTA
				$totalIndonesia['L'] = $LTotal;
				$totalIndonesia['P'] = $PTotal;
				$totalIndonesia['< 60'] = $ageBelowTotal;
				$totalIndonesia['60 - 70'] = $ageBetweenTotal;
				$totalIndonesia['> 70'] = $ageAboveTotal;
				$totalIndonesia['JUMLAH'] = $jumlahTotal;

				$total['WILAYAH'] = 'JUMLAH';
				$total['PKRI'] = $PKRITotal;
				$total['TKR'] = $TKRTotal;
				$total['DKR'] = $DKRTotal;
				$total['SRJ'] = $SRJTotal;
				$total['PERDAMAIAN'] = $PERDAMAIANTotal;
				$total['ANM'] = $ANMTotal; // VET ANUMERTA
				$total['L'] = $LTotal;
				$total['P'] = $PTotal;
				$total['< 60'] = $ageBelowTotal;
				$total['60 - 70'] = $ageBetweenTotal;
				$total['> 70'] = $ageAboveTotal;
				$total['JUMLAH'] = $jumlahTotal;
			}
		}
		return array('list' => $list, 'total' => $total, 'totalIndonesia' => $totalIndonesia);
	}

	public function rekapPerProvinsi(Request $request)
	{
		$params = parseParameter($request->parameter);
		$params['typeData'] = isset($request->typeData)?$request->typeData:'';
		$params['titleRekap'] = 'DIREKTORAT JENDERAL POTENSI PERTAHANAN DIREKTORAT VETERAN <br>REKAPITULASI KEKUATAN VETERAN YANG MASIH HIDUP TA. 2018';
		$result = $this->getDataRekapProvinsi($params);
		$list = $result['list'];
		$total = $result['total'];
		$totalIndonesia = $result['totalIndonesia'];
        return view('dashboard.rekap.provinsi', compact('list', 'total', 'totalIndonesia', 'params'));
	}

	public function rekapPerProvinsiNewTab($tipeData = '', $id = '')
	{
		$params['idProvinsi'] = $id;
		$params['typeData'] = $tipeData;
		$params['titleRekap'] = 'DIREKTORAT JENDERAL POTENSI PERTAHANAN DIREKTORAT VETERAN <br>REKAPITULASI KEKUATAN VETERAN YANG MASIH HIDUP';
		// $params['titleRekap'] = 'DIREKTORAT JENDERAL POTENSI PERTAHANAN DIREKTORAT VETERAN <br>REKAPITULASI KEKUATAN VETERAN YANG MASIH HIDUP TA. 2018';
		$result = $this->getDataRekapProvinsi($params);
		$list = $result['list'];
		$total = $result['total'];
		$totalIndonesia = $result['totalIndonesia'];
        return view('dashboard.rekap.provinsinewtab', compact('list', 'total', 'totalIndonesia', 'params'));
	}

	public function rekapPerProvinsiPDF(Request $request)
	{
		$data['html'] = $request->html;
		$data['tanggalTtd'] = formatDate(date('Y-m-d'));
        // return view('pdf.rekapPerProvinsi', $data);
		$pdf = PDF::loadView('pdf.rekapPerProvinsi', $data)->setPaper('a4', 'landscape');
		$pdf = $pdf->setOptions(['dpi' => 300, 'defaultFont' => 'sans-serif']);
		return $pdf->download('Rekap.pdf');
	}

	public function search(Request $request)
	{
		if ($request->search)
		{
			$veteran = new \App\Models\Veteran();
			$veteran = $veteran->leftJoin('provinsi', 'provinsi.idProvinsi', 'veteran.idProvinsi');
			$veteran = $veteran->where('veteran.npvBaru', 'LIKE', '%'.$request->search.'%');
			$veteran = $veteran->orWhere('veteran.namaVeteran', 'LIKE', '%'.$request->search.'%');
			$veteran = $veteran->limit(100);
			$veteran = $veteran->get();
			if ($veteran)
			{
				$veteran = $veteran->toArray();
				if (count($veteran) == 1)
				{
	                return Redirect::to(url('veteran/'.$veteran[0]['idVeteran'].'/rh'));
				}
				else {
			        $list = $veteran;
			        return view('dashboard.search', compact('list'));
				}
			}
		}
		else {
			return Redirect::to(url('veteran'));
		}
	}
}
