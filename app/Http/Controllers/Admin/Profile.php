<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Datatables;

class Profile extends BaseController
{
    protected $redirectTo = 'edit-profile'; // REDIRECT URL
    protected $redirectIndex = 'profile.index'; // View profile
    protected $redirectEditable = 'profile.editable'; // vied add and update

    public function profile()
    {
        $id = $this->Auth::user()->id;
        $item = \App\Models\User::find($id);
        $this->item     = $item;
        if ($id > 0)
        {
            if (isset($item) && is_array($item) && count($item) == 0)
            {
                return Redirect::to($this->redirectPath());
            }
        }
        return view($this->redirectEditablePath(), compact('item', 'id', 'lookupTable'));
    }

    public function updateProfile(Request $request)
    {
        $id = $this->Auth::user()->id;
        $validator = $this->validateData($request, $id);
        if ($validator != null && $validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }
        else {
            $this->saveData($request, $id);
            Session::flash('message', 'Successfully saved the item!');
            return Redirect::to($this->redirectPath());
        }
    }
    
    protected function data()
    {
    	$id = $this->Auth::user()->id;
        return \App\Models\User::find($id);
    }

    protected function validateData(Request $request, $id)
    {
        /*if($id > 0) return null;*/
        return Validator::make($request->all(), [
            'name'		=> 'required|string',
            // 'username'	=> 'required|string|max:50|unique:users,username,'.$id,
        ], [
            'name.required' => 'Nama tidak boleh kosong'
        ]);
    }

    protected function saveData(Request $request, $id)
    {
        $item = new \App\Models\User();
        if($id > 0) $item = $item->find($id);

        if ($request->password)
        {
            $validator  = Validator($request->all(),[
                'password'      => 'required|string|min:4|confirmed',
            ],[
                'password.required' => 'Password lama tidak boleh kosong',
                'password.confirmed' => 'Password konfirmasi salah',
            ]);
            $validator->after(function($validator) use ($request) {
                $check  = auth()->validate([
                    'username'     => $this->Auth::user()->username,
                    'password'  => $request->current_password
                ]);

                if (!$check)
                    $validator->errors()->add('current_password', 'Password lama Anda salah');
            });
            if ($validator->fails())
            {
                return redirect()->back()->withInput()->withErrors($validator);
            }

            if ($request->password)
                $item->password = bcrypt($request->password);
        }

        $item->name = $request->name;
        // $item->username = $request->username;
        // $item->email = $request->email;
        // $item->roleCode = $request->roleCode;
        $item->save();
    }

    public function changePassword()
    {
        $id         = $this->Auth::user()->id;
        $item       = '';
        $lookupTable = [];
        return view('profile.changePassword', compact('item', 'id', 'lookupTable'));
    }

    public function changePasswordStore(Request $request)
    {
        $id         = $this->Auth::user()->id;
        $validator  = Validator($request->all(), [
            'password'      => 'required|string|min:6|confirmed',
        ]);
        $validator->after(function($validator) use ($request) {
            $check  = auth()->validate([
                'username'     => $this->Auth::user()->username,
                'password'  => $request->current_password
            ]);

            if (!$check)
                $validator->errors()->add('current_password', 'Your current password is incorrect, please try again.');
        });

        if ($validator->fails())
        {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $user           = new \App\Models\User();
        $user           = $user->find($id);
        $user->password = bcrypt($request->password);
        $user->save();

        $this->Auth::logout();
        Session::flush();

        Session::flash('message', 'Successfully update password!');
        return Redirect::to('veteran');
        // return Redirect::to('change-password');
    }
}
