<?php 

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Datatables;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class Media extends BaseController
{
    public function index()
    {
        $list = [];
        return view('media.index', compact('list'));
    }

    public function listMedia()
    {
        // echo public_path();
        // header('Content-type: application/json');
        $dir = "files";
        $response = $this->scan($dir);
        $result = array(
            "name" => "files",
            "type" => "folder",
            "path" => $dir,
            "items" => $response
        );
        return response()->json($result);
    }

    public function scan($dir)
    {
        $files = array();
        // Is there actually such a folder/file?
        if(file_exists($dir))
        {
            foreach(scandir($dir) as $f)
            {
                if(!$f || $f[0] == '.')
                {
                    continue; // Ignore hidden files
                }

                if(is_dir($dir . '/' . $f))
                {
                    // The path is a folder
                    $files[] = array(
                        "name" => $f,
                        "type" => "folder",
                        "path" => $dir . '/' . $f,
                        "items" => $this->scan($dir . '/' . $f) // Recursively get the contents of the folder
                    );
                }
                else {
                    // It is a file
                    $files[] = array(
                        "name" => $f,
                        "type" => "file",
                        "path" => $dir . '/' . $f,
                        "size" => filesize($dir . '/' . $f) // Gets the size of this file
                    );
                }
            }
        }
        return $files;
    }

    public function createFolder(Request $request)
    {
        $pathFolder = '';
        if ($request->pathFolder != '') $pathFolder = $request->pathFolder.'/';
        $dir = $pathFolder.$request->folderName;
        if (!is_dir($dir))
        {
            mkdir($dir);
        }
        return redirect()->back();
    }

    public function uploadFile(Request $request)
    {
        if ($request->file)
        {
            foreach ($request->file as $key => $file)
            {
                // $file          = $request->file;
                // $fileName        = $request->npvBaru.'.'.$file->getClientOriginalExtension();
                // $fileName      = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
                $fileName        = $file->getClientOriginalName();
                $destinationPath = $request->pathFolder;
                $result          = $file->move($destinationPath, $fileName);
                $fileUrl         = $destinationPath."/".$fileName;
            }
            Session::flash('message', 'Upload berhasil.');
            return Redirect::to(url('media#'.urlencode($request->pathFolder)));
        }
    }

    public function delete(Request $request)
    {
        $filename = $request->path;
        if (file_exists($filename))
        {
            if ($request->type == 'file')
                unlink($filename);

            if ($request->type == 'folder')
            {
                if (is_dir_empty($filename))
                {
                    rmdir($filename);
                }
                else{
                    $response = [
                        'statusCode' => '403',
                        'message' => 'Maaf folder tidak kosong'
                    ];
                    return response()->json($response); 
                }
            }
            $response = [
                'statusCode' => '200',
                'message' => 'File berhasil di hapus'
            ];
            return response()->json($response); 
        }
        else {
            $response = [
                'statusCode' => '200',
                'message' => "The file $filename does not exist"
            ];
            return response()->json($response); 
        }
    }

    public function modal()
    {
        $list = [];
        return view('media.modal', compact('list'));
    }
}