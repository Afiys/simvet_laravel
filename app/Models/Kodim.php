<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kodim extends Model
{
    protected $table = 'kodim';
    protected $primaryKey = 'idKodim';
    protected $fillable = [
		'namaKodim',
		'idKodam',
		'created_at',
		'updated_at',
    ];
}
