<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kodam extends Model
{
    protected $table = 'kodam';
    protected $primaryKey = 'idKodam';
    protected $fillable = [
		'namaKodam',
		'created_at',
		'updated_at',
    ];
}
