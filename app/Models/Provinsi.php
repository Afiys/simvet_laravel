<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Provinsi extends Model
{
    protected $table = 'provinsi';
    protected $primaryKey = 'idProvinsi';
    protected $fillable = [
		'namaProvinsi',
		'idKodam',
		'created_at',
		'updated_at',
    ];
}
