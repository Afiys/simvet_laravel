<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kabupaten extends Model
{
    protected $table = 'kabupaten';
    protected $primaryKey = 'idKabupaten';
    protected $fillable = [
		'namaKabupaten',
		'idProvinsi',
		'created_at',
		'updated_at',
    ];
}
