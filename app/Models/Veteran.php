<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Veteran extends Model
{
    protected $table = 'veteran';
    protected $primaryKey = 'idVeteran';
    protected $fillable = [
		'idProvinsi',
		'idKabupaten',
		'idKodam',
		'idKodim',
		'namaVeteran',
		'tempatLahir',
		'tanggalLahir',
		'jenisKelamin',
		'NIK',
		'noPendaftaran',
		'tanggalPendaftaran',
		'npvLama',
		'npvBaru',
		'noKeputusan',
		'tanggalKeputusan',
		'predikat',
		'golongan',
		'tahunMasaBakti',
		'bulanMasaBakti',
		'tanggalMeninggal',
		'namaAhliWaring',
		'tanggalLahirAhliWaris',
		'NIKAhliWaris',
		'alamat',
		'rt',
		'rw',
		'kelurahan',
		'kecamatan',
		'noKepTuvet',
		'tglKepTuvet',
		'noKepDahor',
		'tglKepDahor',
		'noKepTundayatu',
		'tglKepTundayatu',
		'nomorRekening',
		'karipPTTaspen',
		'keterangan',
		'created_at',
		'updated_at',
    ];
}
