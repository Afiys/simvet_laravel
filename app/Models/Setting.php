<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'settings';
    protected $primaryKey = '';
    protected $fillable = [
		'settingId',
		'type',
		'key',
		'value',
		'longText',
		'imageUrl',
		'created_at',
		'updated_at',
    ];
}
