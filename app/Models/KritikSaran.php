<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KritikSaran extends Model
{
    protected $table = 'kritiksaran';
    protected $primaryKey = 'idKritikSaran';
    protected $fillable = [
		'nama',
		'email',
		'noTelp',
		'message',
		'created_at',
		'updated_at',
    ];
}
