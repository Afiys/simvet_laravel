<ul class="nav sidebar-menu">
	@foreach (Config::get('menu.menu') as $key => $menu)
		@if($key === 'title')
			@if (in_array('ALL', $menu['role']))
				<li style="padding: 30px 0px 10px 30px;"><strong>{{ $menu['text'] }}</strong></li>
			@else
				@if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, $menu['role']))
					<li style="padding: 30px 0px 10px 30px;"><strong>{{ $menu['text'] }}</strong></li>
				@endif
			@endif
			
		@else

			@if (in_array('ALL', $menu['role']))
				<li class="@yield(''.$menu['page'])
					@if (isset($menu['submenu']) && count($menu['submenu']) > 0)
						dropdown
					@endif
				">
					<a
						@if (isset($menu['submenu']) && count($menu['submenu']) > 0)
						@else
							href="{{ url(''.$menu['url']) }}"
						@endif
					>
						<i class="{{ config('icon.sidebarAdmin.'.$menu['icon']) }}"></i>
						&nbsp;&nbsp;{{ $menu['text'] }}

						@if (isset($menu['submenu']))
						<span class="pull-right-container">
							@if (trim($__env->yieldContent(''.$menu['page'])) == 'active')
							<i class="fa fa-angle-down pull-right"></i>
							@else
							<i class="fa fa-angle-left pull-right"></i>
							@endif
						</span>
						@endif

					</a>
					@if (isset($menu['submenu']) && count($menu['submenu']) > 0)
					<ul class="dropdown">
						@foreach ($menu['submenu'] as $keySub => $submenu)
							@if (in_array('ALL', $submenu['role']))
								<li class="@yield(''.$submenu['page'])"><a href="{{ url(''.$submenu['url']) }}">{{ $submenu['text'] }}</a></li>
							@else
								@if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, $menu['role']))
									<li class="@yield(''.$submenu['page'])"><a href="{{ url(''.$submenu['url']) }}">{{ $submenu['text'] }}</a></li>
								@endif
							@endif
						@endforeach
					</ul>
					@endif
				</li>
			
			@else

				@if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, $menu['role']))
					<li class="@yield(''.$menu['page'])
						@if (isset($menu['submenu']) && count($menu['submenu']) > 0)
							dropdown
						@endif
					">
						<a
							@if (isset($menu['submenu']) && count($menu['submenu']) > 0)
							@else
								href="{{ url(''.$menu['url']) }}"
							@endif
						>
							<i class="{{ config('icon.sidebarAdmin.'.$menu['icon']) }}"></i>
							&nbsp;&nbsp;{{ $menu['text'] }}

							@if (isset($menu['submenu']))
							<span class="pull-right-container">
								@if (trim($__env->yieldContent(''.$menu['page'])) == 'active')
								<i class="fa fa-angle-down pull-right"></i>
								@else
								<i class="fa fa-angle-left pull-right"></i>
								@endif
							</span>
							@endif

						</a>
						@if (isset($menu['submenu']) && count($menu['submenu']) > 0)
						<ul class="dropdown">
							@foreach ($menu['submenu'] as $keySub => $submenu)
								@if (in_array('ALL', $submenu['role']))
									<li class="@yield(''.$submenu['page'])"><a href="{{ url(''.$submenu['url']) }}">{{ $submenu['text'] }}</a></li>
								@else
									@if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, $menu['role']))
										<li class="@yield(''.$submenu['page'])"><a href="{{ url(''.$submenu['url']) }}">{{ $submenu['text'] }}</a></li>
									@endif
								@endif
							@endforeach
						</ul>
						@endif
					</li>
				@endif

			@endif

		@endif
	@endforeach
	<li>
	    <div style="margin: 20px; margin-bottom: 0px; margin-top: 10px; padding: 20px; background-color: #ffffff; border: 1px solid #dedede;">
	    	<div class="row">
	    		<div class="col-sm-8">
			        <div class="title-section">TOTAL VETERAN</div>
			        <div class="total-summary blue"><strong>{{ GetData::count()->veteran }}</strong></div>
	    		</div>
	    		<div class="col-sm-4">
			        <div class="chart blue" style="font-size: 34px;"><i class="fa fa-bar-chart"></i></div>
	    		</div>
	    	</div>
	        <div class="clearfix"></div>
	    </div>
	    <!-- <div style="margin: 20px; margin-bottom: 0px; margin-top: 10px; padding: 20px; background-color: #ffffff; border: 1px solid #dedede;">
	    	<div class="row">
	    		<div class="col-sm-8">
			        <div class="title-section">TOTAL USER</div>
			        <div class="total-summary red"><strong>{{ GetData::count()->veteran }}</strong></div>
	    		</div>
	    		<div class="col-sm-4">
			        <div class="chart red" style="font-size: 34px;"><i class="fa fa-bar-chart"></i></div>
	    		</div>
	    	</div>
	        <div class="clearfix"></div>
	    </div> -->
	</li>
</ul>
@if (!Auth::id())
	<div style="position: absolute; bottom: 20px; padding: 0px 10px;">
		<div>
			{!! GetData::setting()->address['longText'] !!}
		</div>
		<div class="row" style="font-size: 26px;">
			<div class="col-md-1"><a href="{!! GetData::setting()->facebook['value'] !!}" target="_blank" style="color: #3b5998;"><i class="fa fa-facebook-square"></a></i></div>
			<div class="col-md-1"><a href="{!! GetData::setting()->twitter['value'] !!}" target="_blank" style="color: #00aced;"><i class="fa fa-twitter-square"></i></a></div>
			<div class="col-md-1"><a href="{!! GetData::setting()->instagram['value'] !!}" target="_blank" style="color: #8a3ab9;"><i class="fa fa-instagram"></i></a></div>
			<div class="col-md-1"><a href="{!! GetData::setting()->youtube['value'] !!}" target="_blank" style="color: #ff0000;"><i class="fa fa-youtube-square"></i></a></div>
		</div>
	</div>
@endif