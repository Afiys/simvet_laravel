@extends('layouts.admin')

@section('title', 'Kritik dan saran')
@section('kritiksaran', 'active')

@section('content')
<div class="container-fluid" id="container-content">
   	<div class="row row-offcanvas row-offcanvas-left">
		
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
			<ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
				<li>
					<a href="{{ route('kritiksaran.index') }}"><i class="{{ config('icon.sidebarAdmin.kodam') }}"></i>Kritik dan saran</a>&nbsp;&nbsp;&nbsp;
				</li>
				<li>
					<i class="{{ config('icon.'.($id == 0 ? 'add' : 'update')) }}" aria-hidden="true"></i>{{ ($id == 0 ? 'Tambah' : 'Edit') }}
				</li>
			</ol>
        </div>

		<div class="content">
			<div class="panel panel-primary">
				<div class="panel-heading">Kritik dan saran</div>
				<form method="POST" class="form-horizontal" action="{{ url(route('kritiksaran.index') . ($id == 0 ? '' : '/' . $id)) }}" enctype="multipart/form-data">
					<div class="panel-body">
			            @if (Session::has('message'))
			            <div class="alert alert-info">{{ Session::get('message') }}</div>
			            @endif
						@if ($errors->any())
						<div class="alert alert-danger">
						    <ul>
						        @foreach ($errors->all() as $error)
						            <li>{{ $error }}</li>
						        @endforeach
						    </ul>
						</div>
						@endif
					    {{ csrf_field() }}
					    @if($id != 0)
					    <input name="_method" type="hidden" value="PUT">
						@endif
						<div class="row">
							<div class="col-lg-12">

					            <div class="form-group">
					               <label for="nama" class="col-sm-2 control-label">Nama</label>
					               <div class="col-sm-8">
					                  <input type="text" name="nama" class="form-control" id="nama" placeholder="Nama" required="" value="{{ old('nama', isset($item) ? $item->nama : '') }}" readonly="" />
					               </div>
					            </div>

					      	</div>
							<div class="col-lg-12">

					            <div class="form-group">
					               <label for="email" class="col-sm-2 control-label">Email</label>
					               <div class="col-sm-8">
					                  <input type="text" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email', isset($item) ? $item->email : '') }}" readonly="" />
					               </div>
					            </div>

					      	</div>
							<div class="col-lg-12">

					            <div class="form-group">
					               <label for="noTelp" class="col-sm-2 control-label">No Telp (Optional)</label>
					               <div class="col-sm-8">
					                  <input type="text" name="noTelp" class="form-control" id="noTelp" placeholder="No Telp (Optional)" value="{{ old('noTelp', isset($item) ? $item->noTelp : '') }}" readonly="" />
					               </div>
					            </div>
					      	</div>

							<div class="col-lg-12">
					            <div class="form-group">
					               <label for="message" class="col-sm-2 control-label">Pesan</label>
					               <div class="col-sm-8">
					               		<textarea name="message" class="form-control" id="message" placeholder="Pesan" readonly="">{{ old('message', isset($item) ? $item->message : '') }}</textarea>
					               </div>
					            </div>
					      	</div>

							<div class="col-lg-12">
					            <div class="form-group">
					               <label for="jawaban" class="col-sm-2 control-label">Jawaban</label>
					               <div class="col-sm-8">
					               		<textarea name="jawaban" class="form-control" id="jawaban" placeholder="Jawaban" {{ (!Auth::id()) ? 'readonly' : '' }}>{{ old('jawaban', isset($item) ? $item->jawaban : '') }}</textarea>
					               </div>
					            </div>
					      	</div>

					    </div>
					</div>
					<div class="panel-footer">
						<div class="col-lg-2">
							<button class="btn btn-md btn-block btn-primary" type="submit">Save</button>
						</div>
						<div class="col-lg-2">
							<a href="{{ route('kritiksaran.index') }}" class="btn btn-md btn-block btn-default">Cancel</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
      	</div>
  	</div>
</div>
@endsection

@section('js')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
</script>
@endsection
