@extends('layouts.admin')

@section('title', 'Kritik dan saran')
@section('kritiksaran', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
                <li>
                    <i class="{{ config('icon.sidebarAdmin.kodam') }}"></i>Kritik dan saran
                </li>
            </ol>
        </div>

        <div class="content">
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            @if (!Auth::id())
            <div class="btn-container">
                <a href="{{ route('kritiksaran.create') }}" class="btn btn-add">Tambah Kritik dan Saran</a>
            </div>
            @endif
            
            @if (in_array('ALL', Config::get('menu.role.kritiksaran.create')))
            <!-- <div class="btn-container">
                <a href="{{ route('kritiksaran.create') }}" class="btn btn-add">Tambah Kritik dan Saran</a>
            </div> -->
            @else
                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.kritiksaran.create')))
                <!-- <div class="btn-container">
                    <a href="{{ route('kritiksaran.create') }}" class="btn btn-add">Add New</a>
                </div> -->
                @endif
            @endif
            <div class="container-fluid table-responsive" align="center-block">

                <table class="table table-striped table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No Telp</th>
                            <th>Tanggal</th>
                            <th>Jawaban</th>
                            <th>Kritik dan Saran</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    listManager.listUrl = "{{ route('kritiksaran.index') }}/list";
    listManager.deleteUrl = "{{ route('kritiksaran.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                { "data": "nama", "name": "nama" },
                                { "data": "email", "name": "email" },
                                { "data": "noTelp", "name": "noTelp" },
                                { "data": "created_at", "name": "created_at" },
                                { "data": "statusJawaban", "name": "statusJawaban" },
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"},
    ];
    listManager.ready();
});
</script>
@endsection