@extends('layouts.admin')

@section('title', 'Search')
@section('dashboard', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
<style>
    form .btn { width: 100%; }
</style>
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ url('') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
                <li>
                    <i class="{{ config('icon.sidebarAdmin.veteran') }}"></i>Search
                </li>
            </ol>
        </div>

        <div class="content">
            <div class="container-fluid table-responsive" align="center-block">

                <div class="form-group">
                    <div class="row">
                        <div class="col-sm-12">
                            <form action="{{ route('search') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="search" class="search form-control" placeholder="search" name="search" required="" onkeydown="if (event.keyCode == 13) { this.form.submit(); return false; }" value="{{ isset($_POST['search']) ? $_POST['search'] : '' }}">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <h2>Hasil pencarian : {{ isset($_POST['search']) ? $_POST['search'] : '' }}</h2>
                <div class="clearfix"></div>

                <table class="table table-striped table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>NPV</th>
                            <th>Provinsi</th>
                            <th>Nomor Karip</th>
                            <th>Jenis Kelamin</th>
                            <th>Predikat</th>
                            <th>RH</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($list)
                        @foreach ($list as $key => $data)
                        <tr>
                            <td>{{ $data['namaVeteran'] }}</td>
                            <td>{{ $data['npvBaru'] }}</td>
                            <td>{{ $data['namaProvinsi'] }}</td>
                            <td>{{ $data['noKarip'] }}</td>
                            <td>{{ $data['jenisKelamin'] }}</td>
                            <td>{{ $data['predikat'] }}</td>
                            <td><a href="{{ url('veteran/'.$data['idVeteran'].'/rh') }}" class="btn btn-xs btn-primary"><i class="fa fa-search-plus" aria-hidden="true"></i></a></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#dataTables').DataTable({
        "searching": false,
        "ordering": false,
        "bLengthChange": false
    });
    /*listManager.listUrl = "{{ route('veteran.index') }}/list";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                { "data": "npvBaru", "name": "veteran.npvBaru" },
                                // { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": "noKarip", "name": "veteran.noKarip" },
                                { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                { "data": "predikat", "name": "veteran.predikat" },
                                { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"},
    ];
    listManager.ready();*/
});
</script>
@endsection