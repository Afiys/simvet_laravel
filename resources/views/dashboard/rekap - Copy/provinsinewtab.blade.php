@extends('layouts.admin')

@section('title', 'Dashboard')
@section('dashboard', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
<style>
    .table { border: 1px solid #000000; }
    .table > thead > tr > th { text-align: center; vertical-align: middle; }
    .table > tbody > tr > td { text-align: right; vertical-align: middle; }
    .table > thead > tr > th, .table > tbody > tr > td { border: 1px solid #000000; padding-top: 0px !important; padding-bottom: 0px !important; }
    .title-section { text-align: center; }
</style>
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <!-- <img src="{{ url('assets/img/header.jpg') }}" width="100%"> -->
        <div class="content">
            <div class="container-fluid table-responsive" align="center-block">
                <div class="divTabel" style="display: none;">
                    <div style="padding: 20px; text-align: center;">
                        <form method="POST" action="{{ route('rekapPerkodamPDF') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="html" id="html" value="">
                            <button type="submit" class="btn btn-primary" id="savePDF">SAVE PDF</button>
                        </form>
                    </div><br>
                    <!-- <h1 class="title-section">TABEL</h1> -->
                    <div id="target">
                        <div style="text-align: center;">
                            <strong>{!! $params['titleRekap'] !!}</strong><br><br>
                        </div>
                        <table class="table table-striped table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2" style="border-right: 2px solid #000000;">Wilayah</th>
                                    <th rowspan="2">PKRI</th>
                                    <th colspan="3">Pembela</th>
                                    <th rowspan="2">Perdamaian</th>
                                    <th rowspan="2">ANM</th>
                                    <!-- <th rowspan="2">Vet<br>Anumerta</th> -->
                                    <th colspan="2">Jenis Kelamin</th>
                                    <th colspan="3">Usia</th>
                                    <th rowspan="2">Jumlah</th>
                                </tr>
                                <tr>
                                    <th>TKR</th>
                                    <th>DKR</th>
                                    <th>SRJ</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>< 60</th>
                                    <th>60 - 70</th>
                                    <th>> 70</th>
                                </tr>
                                <tr style="border-bottom: 2px solid #000000;">
                                    <th>1</th>
                                    <th style="border-right: 2px solid #000000;">2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($list as $key => $data)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="border-right: 2px solid #000000;"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="text-align: center;">{{ $key+1 }}</td>
                                    <td style="text-align: left;border-right: 2px solid #000000;">{{ $data['WILAYAH'] }}</td>
                                    <td>{{ $data['PKRI'] }}</td>
                                    <td>{{ $data['TKR'] }}</td>
                                    <td>{{ $data['DKR'] }}</td>
                                    <td>{{ $data['SRJ'] }}</td>
                                    <td>{{ $data['PERDAMAIAN'] }}</td>
                                    <td>{{ $data['ANM'] }}</td>
                                    <td>{{ $data['L'] }}</td>
                                    <td>{{ $data['P'] }}</td>
                                    <td>{{ $data['< 60'] }}</td>
                                    <td>{{ $data['60 - 70'] }}</td>
                                    <td>{{ $data['> 70'] }}</td>
                                    <td>{{ $data['JUMLAH'] }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="divGrafik" style="display: none;">
                    <div class="row">
                        <div  class="col-sm-12">
                            <!-- <h1 class="title-section">GRAFIK</h1> -->
                            <br>
                            <div style="text-align: center;">
                                <strong>DIREKTORAT JENDERAL POTENSI PERTAHANAN DIREKTORAT VETERAN <br>REKAPITULASI KEKUATAN VETERAN YANG MASIH HIDUP TW 1 TA. 2018</strong><br><br>
                            </div>
                            @foreach($list as $key => $data)
                            <div style="padding-bottom: 50px;">
                                <h3 class="title-section">{{ $data['WILAYAH'] }}</h3>
                                <div class="col-md-6">
                                    <canvas id="grafikJenisKelamin{{ $key+1 }}" width="100%" height="40px"></canvas>
                                </div>
                                <div class="col-md-6">
                                    <canvas id="grafikUsia{{ $key+1 }}" width="100%" height="30px"></canvas>
                                </div>
                                <br><br>
                                <div class="col-md-12">
                                    <canvas id="grafikPredikat{{ $key+1 }}" width="100%" height="40px"></canvas>
                                </div>
                                <div class="clearfix"></div>
                                <!-- <hr> -->
                                <div style="background-color: #cccccc; height: 5px; width: 100%;"></div>
                            </div>
                            @endforeach
                            @if ($total && strtoupper($params['idProvinsi']) == 'ALL')
                            <div style="padding-bottom: 50px;">
                                <h3 class="title-section">{{ isset($total['NAME']) ? $total['NAME'] : '' }}</h3>
                                <div class="col-md-12">
                                    <canvas id="grafikTotal" width="100%"></canvas>
                                </div>
                                <div class="clearfix"></div>
                                <div style="background-color: #cccccc; height: 5px; width: 100%;"></div>
                                <div class="clearfix"></div>
                            </div>
                            @endif
                            <!-- JUMLAH -->
                            <!-- <h3 class="title-section">JUMLAH</h3>
                            <div class="col-md-6">
                                <canvas id="grafikJenisKelamin" width="100%" height="40px"></canvas>
                            </div>
                            <div class="col-md-6">
                                <canvas id="grafikUsia" width="100%" height="30px"></canvas>
                            </div>
                            <br><br>
                            <div class="col-md-12">
                                <canvas id="grafikPredikat" width="100%" height="40px"></canvas>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

@stop

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('assets/components/Chart.js/dist/chart.js') }}"></script>
<script src="{{ url('assets/components/Chart.js/dist/Chart.bundle.min.js') }}"></script>
<script src="{{ url('assets/components/ImageMapster/dist/jquery.imagemapster.js') }}"></script>
<script type="Text/javascript">
    $(document).ready(function()
    {
        @if ($params['typeData'] == 'grafik')
        /* GRAPHICS */
        $('.divGrafik').show();

        @foreach($list as $key => $data)
            /*var ctx3 = document.getElementById("grafikJenisKelamin{{ $key+1 }}");
            var grafikJenisKelamin = new Chart(ctx3, {
                type: 'pie',
                data: {
                    labels: ["Perempuan", "Laki - Laki"],
                    // labels: ["Text 1", "Text 2", "Text 3", "Text 4", "Text 5"],
                    datasets: [{
                        label: 'JENIS KELAMIN',
                        data: [{{ $data['P'] }}, {{ $data['L'] }}],
                        // data: [12, 19, 3, 5, 2],
                        backgroundColor: [
                            'rgba(128, 0, 0, 1)',
                            'rgba(5, 57, 102, 0.8)',
                        ],
                        borderColor: [
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'JENIS KELAMIN'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            });

            var ctx4 = document.getElementById("grafikPredikat{{ $key+1 }}");
            ctx4.height = 20;
            var grafikPredikat = new Chart(ctx4, {
                type: 'horizontalBar',
                data: {
                    labels: ["PKRI", "TKR", "DKR", "SRJ", "Perdamaian", "Vet Anumerta"],
                    datasets: [{
                        label: 'PREDIKAT',
                        data: [{{ $data['PKRI'] }}, {{ $data['TKR'] }}, {{ $data['DKR'] }}, {{ $data['SRJ'] }}, {{ $data['PERDAMAIAN'] }}, {{ $data['ANM'] }}],
                        backgroundColor: [
                            'rgba(128, 0, 0, 1)',
                            'rgba(5, 57, 102, 0.8)',
                            'rgba(244, 23, 0, 0.6)',
                            'rgba(0, 191, 2, 0.6)',
                            'rgba(127, 191, 2, 0.6)',
                            'rgba(173, 32, 102, 1)'
                        ],
                        borderColor: [
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });

            var ctx5 = document.getElementById("grafikUsia{{ $key+1 }}");
            // ctx5.height = 20;
            var grafikUsia = new Chart(ctx5, {
                type: 'bar',
                data: {
                    labels: ["< 60", "60 - 70", "> 70"],
                    datasets: [{
                        label: 'USIA',
                        data: [{{ $data['< 60'] }}, {{ $data['60 - 70'] }}, {{ $data['> 70'] }}],
                        backgroundColor: [
                            'rgba(128, 0, 0, 1)',
                            'rgba(5, 57, 102, 0.8)',
                            'rgba(244, 23, 0, 0.6)'
                        ],
                        borderColor: [
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)',
                            'rgba(5, 57, 102, 0)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                    pan: {
                        enabled: true,
                        mode: 'y'
                    },
                    zoom: {
                        enabled: true,
                        mode: 'y',
                        limits: {
                            max: 10,
                            min: 1
                        }
                    },
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    }
                }
            });
            */

                    var ctx3 = document.getElementById("grafikJenisKelamin{{ $key+1 }}");
                    var grafikJenisKelamin = new Chart(ctx3, {
                        type: 'pie',
                        data: {
                            labels: ["Perempuan", "Laki - Laki"],
                            datasets: [{
                                label: 'JENIS KELAMIN',
                                data: [{{ $data['P'] }}, {{ $data['L'] }}],
                                backgroundColor: [
                                    'rgba(128, 0, 0, 1)',
                                    'rgba(5, 57, 102, 0.8)',
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)',
                                    'rgba(5, 57, 102, 0)',
                                ],
                                borderWidth: 1
                            }, {

                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'JENIS KELAMIN'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            }
                        }
                    });

                    var ctx4 = document.getElementById("grafikPredikat{{ $key+1 }}");
                    ctx4.height = 20;
                    var grafikPredikat = new Chart(ctx4, {
                        type: 'horizontalBar',
                        data: {
                            labels: ["PKRI", "TKR", "DKR", "SRJ", "Perdamaian", "Vet Anumerta"],
                            datasets: [{
                                label: 'PKRI',
                                data: [{{ $data['PKRI'] }}],
                                backgroundColor: 'rgba(128, 0, 0, 1)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'TKR',
                                data: [{{ $data['TKR'] }}],
                                backgroundColor: 'rgba(5, 57, 102, 0.8)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'DKR',
                                data: [{{ $data['DKR'] }}],
                                backgroundColor: 'rgba(244, 23, 0, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'SRJ',
                                data: [{{ $data['SRJ'] }}],
                                backgroundColor: 'rgba(0, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'PERDAMAIAN',
                                data: [{{ $data['PERDAMAIAN'] }}],
                                backgroundColor: 'rgba(127, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'ANM',
                                data: [{{ $data['ANM'] }}],
                                backgroundColor: 'rgba(173, 32, 102, 1)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'PREDIKAT'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });

                    var ctx5 = document.getElementById("grafikUsia{{ $key+1 }}");
                    // ctx5.height = 20;
                    var grafikUsia = new Chart(ctx5, {
                        type: 'bar',
                        data: {
                            labels: ["< 60", "60 - 70", "> 70"],
                            datasets: [{
                                label: '< 60',
                                data: [{{ $data['< 60'] }}],
                                backgroundColor: [
                                    'rgba(128, 0, 0, 1)'
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)'
                                ],
                                borderWidth: 1
                            }, {
                                label: '60 - 70',
                                data: [{{ $data['60 - 70'] }}],
                                backgroundColor: [
                                    'rgba(5, 57, 102, 0.8)'
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)'
                                ],
                                borderWidth: 1
                            }, {
                                label: '> 70',
                                data: [{{ $data['> 70'] }}],
                                backgroundColor: [
                                    'rgba(244, 23, 0, 0.6)'
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'USIA'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            pan: {
                                enabled: true,
                                mode: 'y'
                            },
                            zoom: {
                                enabled: true,
                                mode: 'y',
                                limits: {
                                    max: 10,
                                    min: 1
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
        @endforeach

        // GRAFIK TOTAL
        @if ($total && strtoupper($params['idProvinsi']) == 'ALL')
        var chartData = {
            labels: [
                @foreach ($total['data'] as $year => $data)
                '{{ $year }}',
                @endforeach
            ],
            datasets: [
            /*{
                type: 'line',
                label: 'Line',
                borderColor: 'rgba(5, 57, 102, 0.8)',
                borderWidth: 2,
                fill: false,
                data: [
                    2983,
                    877,
                    114219,
                    11421,
                    114242
                ]
            },*/
            {
                type: 'bar',
                label: 'Pertambahan',
                backgroundColor: 'rgba(0, 127, 255, 0.8)',
                data: [
                @foreach ($total['data'] as $year => $data)
                    {{ $data['Pertambahan'] }},
                @endforeach
                ],
                borderColor: 'white',
                borderWidth: 2
            },
            {
                type: 'bar',
                label: 'Kematian',
                backgroundColor: 'rgba(255, 127, 0, 0.8)',
                data: [
                @foreach ($total['data'] as $year => $data)
                    {{ $data['Kematian'] }},
                @endforeach
                ]
            },
            {
                type: 'bar',
                label: 'Total',
                backgroundColor: 'rgba(255, 242, 0, 0.8)',
                data: [
                @foreach ($total['data'] as $year => $data)
                    {{ $data['Total'] }},
                @endforeach
                ]
            }
            ]
        };

        var ctxTotal = document.getElementById("grafikTotal");
        // ctxTotal.height = 20;
        var grafikTotal = new Chart(ctxTotal, {
            type: 'bar',
            data: chartData,
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: '{{ isset($total['NAME']) ? $total['NAME'] : '' }}'
                },
                tooltips: {
                    mode: 'index',
                    intersect: true
                }
            }
        });
        @endif
        /*
        // JUMLAH
        var ctx3 = document.getElementById("grafikJenisKelamin");
        var grafikJenisKelamin = new Chart(ctx3, {
            type: 'pie',
            data: {
                labels: ["Perempuan", "Laki - Laki"],
                datasets: [{
                    label: 'JENIS KELAMIN',
                    data: [{{ $total['P'] }}, {{ $total['L'] }}],
                    backgroundColor: [
                        'rgba(128, 0, 0, 1)',
                        'rgba(5, 57, 102, 0.8)',
                    ],
                    borderColor: [
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'JENIS KELAMIN'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        });

        var ctx4 = document.getElementById("grafikPredikat");
            ctx4.height = 20;
        var grafikPredikat = new Chart(ctx4, {
            type: 'horizontalBar',
            data: {
                labels: ["PKRI", "TKR", "DKR", "SRJ", "Perdamaian", "Vet Anumerta"],
                datasets: [{
                    label: 'PREDIKAT',
                    data: [{{ $total['PKRI'] }}, {{ $total['TKR'] }}, {{ $total['DKR'] }}, {{ $total['SRJ'] }}, {{ $total['PERDAMAIAN'] }}, {{ $total['ANM'] }}],
                    backgroundColor: [
                        'rgba(128, 0, 0, 1)',
                        'rgba(5, 57, 102, 0.8)',
                        'rgba(244, 23, 0, 0.6)',
                        'rgba(0, 191, 2, 0.6)',
                        'rgba(127, 191, 2, 0.6)',
                        'rgba(173, 32, 102, 1)'
                    ],
                    borderColor: [
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });

        var ctx5 = document.getElementById("grafikUsia");
        var grafikUsia = new Chart(ctx5, {
            type: 'bar',
            data: {
                labels: ["< 60", "60 - 70", "> 70"],
                datasets: [{
                    label: 'USIA',
                    data: [{{ $total['< 60'] }}, {{ $total['60 - 70'] }}, {{ $total['> 70'] }}],
                    backgroundColor: [
                        'rgba(128, 0, 0, 1)',
                        'rgba(5, 57, 102, 0.8)',
                        'rgba(244, 23, 0, 0.6)'
                    ],
                    borderColor: [
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)',
                        'rgba(5, 57, 102, 0)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                pan: {
                    enabled: true,
                    mode: 'y'
                },
                zoom: {
                    enabled: true,
                    mode: 'y',
                    limits: {
                        max: 10,
                        min: 1
                    }
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });*/
        @endif

        @if ($params['typeData'] == 'tabel')
            $('.divTabel').show();
        @endif
        $('#html').val($('#target').html());
        /*$.ajax({
            url: '{{ route('rekapPerkodamPDF') }}',
            type: 'POST',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
                html : $('#target').html()
            },
         })
         .done(function() {
            console.log("success");
         })
         .fail(function() {
            console.log("error");
         })
         .always(function() {
            console.log("complete");
         });*/
          
    });
    jQuery(window).load(function ()
    {
        $('#html').val($('#target').html());
    });
</script>
@endsection