@extends('layouts.admin')

@section('title', 'Dashboard')
@section('dashboard', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
<style>
    .table { border: 1px solid #000000; }
    .table > thead > tr > th { text-align: center; vertical-align: middle; }
    .table > tbody > tr > td { text-align: right; vertical-align: middle; }
    .table > thead > tr > th, .table > tbody > tr > td { border: 1px solid #000000; padding-top: 0px !important; padding-bottom: 0px !important; }
    .title-section { text-align: center; }
</style>
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <!-- <img src="{{ url('assets/img/header.jpg') }}" width="100%"> -->
        <div class="content">
            <div class="container-fluid table-responsive" align="center-block">
                @if ($params['typeData'] == 'tabel')
                <div class="divTabel" style="display: none;">
                    <div style="padding: 20px; text-align: center;">
                        <form method="POST" action="{{ route('rekapPerkodamPDF') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="html" id="html" value="">
                            <button type="submit" class="btn btn-primary" id="savePDF">SAVE PDF</button>
                        </form>
                    </div><br>
                    <!-- <h1 class="title-section">TABEL</h1> -->
                    <div id="target">
                        <div style="text-align: center;">
                            <strong>{!! $params['titleRekap'] !!}</strong><br><br>
                        </div>
                        @if (strtoupper($params['idKodam']) == 'ALL')
                        <table class="table table-striped table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th rowspan="2">No</th>
                                    <th rowspan="2" style="border-right: 2px solid #000000;">Wilayah</th>
                                    <th rowspan="2">PKRI</th>
                                    <th colspan="3">Pembela</th>
                                    <th rowspan="2">Perdamaian</th>
                                    <th rowspan="2">ANM</th>
                                    <!-- <th rowspan="2">Vet<br>Anumerta</th> -->
                                    <th colspan="2">Jenis Kelamin</th>
                                    <th colspan="3">Usia</th>
                                    <th rowspan="2">Jumlah</th>
                                </tr>
                                <tr>
                                    <th>TKR</th>
                                    <th>DKR</th>
                                    <th>SRJ</th>
                                    <th>L</th>
                                    <th>P</th>
                                    <th>< 60</th>
                                    <th>60 - 70</th>
                                    <th>> 70</th>
                                </tr>
                                <tr style="border-bottom: 2px solid #000000;">
                                    <th>1</th>
                                    <th style="border-right: 2px solid #000000;">2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach($list as $key => $data)
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="border-right: 2px solid #000000;"></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                @if ($data['WILAYAH'] == 'JUMLAH')
                                <tr>
                                    <td colspan="2" style="text-align: left;border-right: 2px solid #000000;">{{ $data['WILAYAH'] }}</td>
                                    <td>{{ $data['PKRI'] }}</td>
                                    <td>{{ $data['TKR'] }}</td>
                                    <td>{{ $data['DKR'] }}</td>
                                    <td>{{ $data['SRJ'] }}</td>
                                    <td>{{ $data['PERDAMAIAN'] }}</td>
                                    <td>{{ $data['ANM'] }}</td>
                                    <td>{{ $data['L'] }}</td>
                                    <td>{{ $data['P'] }}</td>
                                    <td>{{ $data['< 60'] }}</td>
                                    <td>{{ $data['60 - 70'] }}</td>
                                    <td>{{ $data['> 70'] }}</td>
                                    <td>{{ $data['JUMLAH'] }}</td>
                                </tr>
                                @else
                                <tr>
                                    <td style="text-align: center;">{{ $key+1 }}</td>
                                    <td style="text-align: left;border-right: 2px solid #000000;">{{ $data['WILAYAH'] }}</td>
                                    <td>{{ $data['PKRI'] }}</td>
                                    <td>{{ $data['TKR'] }}</td>
                                    <td>{{ $data['DKR'] }}</td>
                                    <td>{{ $data['SRJ'] }}</td>
                                    <td>{{ $data['PERDAMAIAN'] }}</td>
                                    <td>{{ $data['ANM'] }}</td>
                                    <td>{{ $data['L'] }}</td>
                                    <td>{{ $data['P'] }}</td>
                                    <td>{{ $data['< 60'] }}</td>
                                    <td>{{ $data['60 - 70'] }}</td>
                                    <td>{{ $data['> 70'] }}</td>
                                    <td>{{ $data['JUMLAH'] }}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        @else
                        <table class="table table-striped table-hover" id="datatables">
                            <thead>
                                <tr>
                                    <th rowspan="4">No</th>
                                    <th rowspan="4" style="border-right: 2px solid #000000;">KESATUAN</th>
                                    <th rowspan="1" colspan="8">VETERAN YANG SUDAH MENERIMA</th>
                                    <th rowspan="1" colspan="8">VETERAN YANG BELUM MENERIMA</th>
                                    <!-- <th rowspan="4">KET</th> -->
                                </tr>
                                <tr>
                                    <th rowspan="1" colspan="4">DAHOR</th>
                                    <th rowspan="1" colspan="4">TUVET</th>
                                    <th rowspan="1" colspan="4">DAHOR</th>
                                    <th rowspan="1" colspan="4">TUVET</th>
                                </tr>
                                <tr>
                                    <th rowspan="2">PKRI</th>
                                    <th colspan="3">PEMBELA</th>
                                    <th rowspan="2">PKRI</th>
                                    <th colspan="3">PEMBELA</th>
                                    <th rowspan="2">PKRI</th>
                                    <th colspan="3">PEMBELA</th>
                                    <th rowspan="2">PKRI</th>
                                    <th colspan="3">PEMBELA</th>
                                </tr>
                                <tr>
                                    <th>TKR</th>
                                    <th>DKR</th>
                                    <th>SRJ</th>
                                    <th>TKR</th>
                                    <th>DKR</th>
                                    <th>SRJ</th>
                                    <th>TKR</th>
                                    <th>DKR</th>
                                    <th>SRJ</th>
                                    <th>TKR</th>
                                    <th>DKR</th>
                                    <th>SRJ</th>
                                </tr>
                                <tr style="border-bottom: 2px solid #000000;">
                                    <th>1</th>
                                    <th style="border-right: 2px solid #000000;">2</th>
                                    <th>3</th>
                                    <th>4</th>
                                    <th>5</th>
                                    <th>6</th>
                                    <th>7</th>
                                    <th>8</th>
                                    <th>9</th>
                                    <th>10</th>
                                    <th>11</th>
                                    <th>12</th>
                                    <th>13</th>
                                    <th>14</th>
                                    <th>15</th>
                                    <th>16</th>
                                    <th>17</th>
                                    <th>18</th>
                                    <!-- <th>19</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($list as $key => $data)
                                @if ($data['WILAYAH'] == 'JUMLAH')
                                <tr>
                                    <td colspan="2" style="text-align: left;border-right: 2px solid #000000;">{{ $data['WILAYAH'] }}</td>
                                    <td>{{ $data['PKRI_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['TKR_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['DKR_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['SRJ_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['PKRI_TUVET_SUDAH'] }}</td>
                                    <td>{{ $data['TKR_TUVET_SUDAH'] }}</td>
                                    <td>{{ $data['DKR_TUVET_SUDAH'] }}</td>
                                    <td>{{ $data['SRJ_TUVET_SUDAH'] }}</td>

                                    <td>{{ $data['PKRI_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['TKR_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['DKR_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['SRJ_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['PKRI_TUVET_BELUM'] }}</td>
                                    <td>{{ $data['TKR_TUVET_BELUM'] }}</td>
                                    <td>{{ $data['DKR_TUVET_BELUM'] }}</td>
                                    <td>{{ $data['SRJ_TUVET_BELUM'] }}</td>
                                </tr>
                                @else
                                <tr>
                                    <td style="text-align: center;">{{ $key+1 }}</td>
                                    <td style="text-align: left;border-right: 2px solid #000000;">{{ $data['WILAYAH'] }}</td>
                                    <td>{{ $data['PKRI_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['TKR_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['DKR_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['SRJ_DAHOR_SUDAH'] }}</td>
                                    <td>{{ $data['PKRI_TUVET_SUDAH'] }}</td>
                                    <td>{{ $data['TKR_TUVET_SUDAH'] }}</td>
                                    <td>{{ $data['DKR_TUVET_SUDAH'] }}</td>
                                    <td>{{ $data['SRJ_TUVET_SUDAH'] }}</td>

                                    <td>{{ $data['PKRI_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['TKR_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['DKR_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['SRJ_DAHOR_BELUM'] }}</td>
                                    <td>{{ $data['PKRI_TUVET_BELUM'] }}</td>
                                    <td>{{ $data['TKR_TUVET_BELUM'] }}</td>
                                    <td>{{ $data['DKR_TUVET_BELUM'] }}</td>
                                    <td>{{ $data['SRJ_TUVET_BELUM'] }}</td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                    </div>
                </div>
                @endif

                @if ($params['typeData'] == 'grafik')
                <div class="divGrafik" style="display: none;">
                    <!-- <div style="padding: 20px; text-align: center;">
                        <form method="POST" action="{{ route('rekapPerkodamPDF') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <input type="hidden" name="html" id="html" value="">
                            <button type="submit" class="btn btn-primary" id="savePDF">SAVE PDF</button>
                        </form>
                    </div><br> -->
                    <div id="target" class="row">
                        <div  class="col-sm-12">
                            <!-- <h1 class="title-section">GRAFIK</h1> -->
                            <br>
                            <div style="text-align: center;">
                                <strong>{!! $params['titleRekap'] !!}</strong><br><br>
                            </div>
                            @if (strtoupper($params['idKodam']) == 'ALL')
                                {-- @foreach($list as $key => $data)
                                <!-- <div style="padding-bottom: 50px;">
                                    <h3 class="title-section">{{ $data['WILAYAH'] }}</h3>
                                    <div class="col-md-6">
                                        <canvas id="grafikJenisKelamin{{ $key+1 }}" width="100%" height="40px"></canvas>
                                    </div>
                                    <div class="col-md-6">
                                        <canvas id="grafikUsia{{ $key+1 }}" width="100%" height="30px"></canvas>
                                    </div>
                                    <br><br>
                                    <div class="col-md-12">
                                        <canvas id="grafikPredikat{{ $key+1 }}" width="100%" height="40px"></canvas>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="background-color: #cccccc; height: 5px; width: 100%;"></div>
                                    <div class="clearfix"></div>
                                </div> -->
                                @endforeach --}
                                @if ($total)
                                <div style="padding-bottom: 50px;">
                                    <h3 class="title-section">Grafik</h3>
                                    <div class="col-md-6">
                                        <canvas id="grafikJenisKelaminIndonesia" width="100%" height="40px"></canvas>
                                    </div>
                                    <div class="col-md-6">
                                        <canvas id="grafikUsiaIndonesia" width="100%" height="30px"></canvas>
                                    </div>
                                    <br><br>
                                    <div class="col-md-12">
                                        <canvas id="grafikPredikatIndonesia" width="100%" height="40px"></canvas>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="background-color: #cccccc; height: 5px; width: 100%;"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <div style="padding-bottom: 50px;">
                                    <h3 class="title-section">{{ isset($total['NAME']) ? $total['NAME'] : '' }}</h3>
                                    <div class="col-md-12">
                                        <canvas id="grafikTotal" width="100%"></canvas>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div style="background-color: #cccccc; height: 5px; width: 100%;"></div>
                                    <div class="clearfix"></div>
                                </div>
                                @endif
                            @else
                                @foreach($list as $key => $data)
                                <div style="padding-bottom: 50px;">
                                    <h3 class="title-section">{{ $data['WILAYAH'] }}</h3>
                                    <div class="row">
                                        <div class="col-md-12"><strong>VETERAN YANG SUDAH MENERIMA</strong></div>
                                        <div class="col-md-6">
                                            <canvas id="grafikDahorSudah{{ $key+1 }}" width="100%" height="40px"></canvas>
                                        </div>
                                        <div class="col-md-6">
                                            <canvas id="grafikTuvetSudah{{ $key+1 }}" width="100%" height="40px"></canvas>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12"><br><br><strong>VETERAN YANG BELUM MENERIMA</strong></div>
                                        <div class="col-md-6">
                                            <canvas id="grafikDahorBelum{{ $key+1 }}" width="100%" height="40px"></canvas>
                                        </div>
                                        <div class="col-md-6">
                                            <canvas id="grafikTuvetBelum{{ $key+1 }}" width="100%" height="40px"></canvas>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <!-- <hr> -->
                                    <div style="background-color: #cccccc; height: 5px; width: 100%;"></div>
                                    <div class="clearfix"></div>
                                </div>
                                @endforeach
                            @endif
                            <!-- JUMLAH -->
                            <!-- <h3 class="title-section">JUMLAH</h3>
                            <div class="col-md-6">
                                <canvas id="grafikJenisKelamin" width="100%" height="40px"></canvas>
                            </div>
                            <div class="col-md-6">
                                <canvas id="grafikUsia" width="100%" height="30px"></canvas>
                            </div>
                            <br><br>
                            <div class="col-md-12">
                                <canvas id="grafikPredikat" width="100%" height="40px"></canvas>
                            </div> -->
                        </div>
                    </div>
                </div>
                @endif
            </div>            
        </div>

    </div>
</div>

@stop

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('assets/components/Chart.js/dist/chart.js') }}"></script>
<script src="{{ url('assets/components/Chart.js/dist/Chart.bundle.min.js') }}"></script>
<script src="{{ url('assets/components/ImageMapster/dist/jquery.imagemapster.js') }}"></script>
<script type="Text/javascript">
    $(document).ready(function()
    {
        @if ($params['typeData'] == 'grafik')
            /* GRAPHICS */
            $('.divGrafik').show();

            @if (strtoupper($params['idKodam']) == 'ALL')
                {-- @foreach($list as $key => $data)
                    /*var ctx3 = document.getElementById("grafikJenisKelamin{{ $key+1 }}");
                    var grafikJenisKelamin = new Chart(ctx3, {
                        type: 'pie',
                        data: {
                            labels: ["Perempuan", "Laki - Laki"],
                            datasets: [{
                                label: 'JENIS KELAMIN',
                                data: [{{ $data['P'] }}, {{ $data['L'] }}],
                                backgroundColor: [
                                    'rgba(128, 0, 0, 1)',
                                    'rgba(5, 57, 102, 0.8)',
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)',
                                    'rgba(5, 57, 102, 0)',
                                ],
                                borderWidth: 1
                            }, {

                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'JENIS KELAMIN'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            }
                        }
                    });

                    var ctx4 = document.getElementById("grafikPredikat{{ $key+1 }}");
                    ctx4.height = 20;
                    var grafikPredikat = new Chart(ctx4, {
                        type: 'horizontalBar',
                        data: {
                            //labels: ["PKRI", "TKR", "DKR", "SRJ", "Perdamaian", "Vet Anumerta"],
                            datasets: [{
                                label: 'PKRI',
                                data: [{{ $data['PKRI'] }}],
                                backgroundColor: 'rgba(128, 0, 0, 1)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'TKR',
                                data: [{{ $data['TKR'] }}],
                                backgroundColor: 'rgba(5, 57, 102, 0.8)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'DKR',
                                data: [{{ $data['DKR'] }}],
                                backgroundColor: 'rgba(244, 23, 0, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'SRJ',
                                data: [{{ $data['SRJ'] }}],
                                backgroundColor: 'rgba(0, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'PERDAMAIAN',
                                data: [{{ $data['PERDAMAIAN'] }}],
                                backgroundColor: 'rgba(127, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'ANM',
                                data: [{{ $data['ANM'] }}],
                                backgroundColor: 'rgba(173, 32, 102, 1)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'PREDIKAT'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });

                    var ctx5 = document.getElementById("grafikUsia{{ $key+1 }}");
                    // ctx5.height = 20;
                    var grafikUsia = new Chart(ctx5, {
                        type: 'bar',
                        data: {
                            //labels: ["< 60", "60 - 70", "> 70"],
                            datasets: [{
                                label: '< 60',
                                data: [{{ $data['< 60'] }}],
                                backgroundColor: [
                                    'rgba(128, 0, 0, 1)'
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)'
                                ],
                                borderWidth: 1
                            }, {
                                label: '60 - 70',
                                data: [{{ $data['60 - 70'] }}],
                                backgroundColor: [
                                    'rgba(5, 57, 102, 0.8)'
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)'
                                ],
                                borderWidth: 1
                            }, {
                                label: '> 70',
                                data: [{{ $data['> 70'] }}],
                                backgroundColor: [
                                    'rgba(244, 23, 0, 0.6)'
                                ],
                                borderColor: [
                                    'rgba(5, 57, 102, 0)'
                                ],
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'USIA'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            pan: {
                                enabled: true,
                                mode: 'y'
                            },
                            zoom: {
                                enabled: true,
                                mode: 'y',
                                limits: {
                                    max: 10,
                                    min: 1
                                }
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });*/
                @endforeach --}

                // GRAFIK TOTAL
                @if ($total)
                var ctx3 = document.getElementById("grafikJenisKelaminIndonesia");
                var grafikJenisKelamin = new Chart(ctx3, {
                    type: 'pie',
                    data: {
                        labels: ["Perempuan", "Laki - Laki"],
                        datasets: [{
                            label: 'JENIS KELAMIN',
                            data: [{{ $totalIndonesia['P'] }}, {{ $totalIndonesia['L'] }}],
                            backgroundColor: [
                                'rgba(128, 0, 0, 1)',
                                'rgba(5, 57, 102, 0.8)',
                            ],
                            borderColor: [
                                'rgba(5, 57, 102, 0)',
                                'rgba(5, 57, 102, 0)',
                            ],
                            borderWidth: 1
                        }, {

                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'JENIS KELAMIN'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        }
                    }
                });

                var ctx4 = document.getElementById("grafikPredikatIndonesia");
                ctx4.height = 20;
                var grafikPredikat = new Chart(ctx4, {
                    type: 'horizontalBar',
                    data: {
                        //labels: ["PKRI", "TKR", "DKR", "SRJ", "Perdamaian", "Vet Anumerta"],
                        datasets: [{
                            label: 'PKRI',
                            data: [{{ $totalIndonesia['PKRI'] }}],
                            backgroundColor: 'rgba(128, 0, 0, 1)',
                            borderColor: 'rgba(5, 57, 102, 0)',
                            borderWidth: 1
                        }, {
                            label: 'TKR',
                            data: [{{ $totalIndonesia['TKR'] }}],
                            backgroundColor: 'rgba(5, 57, 102, 0.8)',
                            borderColor: 'rgba(5, 57, 102, 0)',
                            borderWidth: 1
                        }, {
                            label: 'DKR',
                            data: [{{ $totalIndonesia['DKR'] }}],
                            backgroundColor: 'rgba(244, 23, 0, 0.6)',
                            borderColor: 'rgba(5, 57, 102, 0)',
                            borderWidth: 1
                        }, {
                            label: 'SRJ',
                            data: [{{ $totalIndonesia['SRJ'] }}],
                            backgroundColor: 'rgba(0, 191, 2, 0.6)',
                            borderColor: 'rgba(5, 57, 102, 0)',
                            borderWidth: 1
                        }, {
                            label: 'PERDAMAIAN',
                            data: [{{ $totalIndonesia['PERDAMAIAN'] }}],
                            backgroundColor: 'rgba(127, 191, 2, 0.6)',
                            borderColor: 'rgba(5, 57, 102, 0)',
                            borderWidth: 1
                        }, {
                            label: 'ANM',
                            data: [{{ $totalIndonesia['ANM'] }}],
                            backgroundColor: 'rgba(173, 32, 102, 1)',
                            borderColor: 'rgba(5, 57, 102, 0)',
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'PREDIKAT'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });

                var ctx5 = document.getElementById("grafikUsiaIndonesia");
                // ctx5.height = 20;
                var grafikUsia = new Chart(ctx5, {
                    type: 'bar',
                    data: {
                        //labels: ["< 60", "60 - 70", "> 70"],
                        datasets: [{
                            label: '< 60',
                            data: [{{ $totalIndonesia['< 60'] }}],
                            backgroundColor: [
                                'rgba(128, 0, 0, 1)'
                            ],
                            borderColor: [
                                'rgba(5, 57, 102, 0)'
                            ],
                            borderWidth: 1
                        }, {
                            label: '60 - 70',
                            data: [{{ $totalIndonesia['60 - 70'] }}],
                            backgroundColor: [
                                'rgba(5, 57, 102, 0.8)'
                            ],
                            borderColor: [
                                'rgba(5, 57, 102, 0)'
                            ],
                            borderWidth: 1
                        }, {
                            label: '> 70',
                            data: [{{ $totalIndonesia['> 70'] }}],
                            backgroundColor: [
                                'rgba(244, 23, 0, 0.6)'
                            ],
                            borderColor: [
                                'rgba(5, 57, 102, 0)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            position: 'top',
                        },
                        title: {
                            display: true,
                            text: 'USIA'
                        },
                        animation: {
                            animateScale: true,
                            animateRotate: true
                        },
                        pan: {
                            enabled: true,
                            mode: 'y'
                        },
                        zoom: {
                            enabled: true,
                            mode: 'y',
                            limits: {
                                max: 10,
                                min: 1
                            }
                        },
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });

                var chartData = {
                    labels: [
                        @foreach ($total['data'] as $year => $data)
                        '{{ $year }}',
                        @endforeach
                    ],
                    datasets: [
                    /*{
                        type: 'line',
                        label: 'Line',
                        borderColor: 'rgba(5, 57, 102, 0.8)',
                        borderWidth: 2,
                        fill: false,
                        data: [
                            2983,
                            877,
                            114219,
                            11421,
                            114242
                        ]
                    },*/
                    {
                        type: 'bar',
                        label: 'Pertambahan',
                        backgroundColor: 'rgba(0, 127, 255, 0.8)',
                        data: [
                        @foreach ($total['data'] as $year => $data)
                            {{ $data['Pertambahan'] }},
                        @endforeach
                        ],
                        borderColor: 'white',
                        borderWidth: 2
                    },
                    {
                        type: 'bar',
                        label: 'Kematian',
                        backgroundColor: 'rgba(255, 127, 0, 0.8)',
                        data: [
                        @foreach ($total['data'] as $year => $data)
                            {{ $data['Kematian'] }},
                        @endforeach
                        ]
                    },
                    {
                        type: 'bar',
                        label: 'Total',
                        backgroundColor: 'rgba(255, 242, 0, 0.8)',
                        data: [
                        @foreach ($total['data'] as $year => $data)
                            {{ $data['Total'] }},
                        @endforeach
                        ]
                    }
                    ]
                };

                var ctxTotal = document.getElementById("grafikTotal");
                // ctxTotal.height = 20;
                var grafikTotal = new Chart(ctxTotal, {
                    type: 'bar',
                    data: chartData,
                    options: {
                        responsive: true,
                        title: {
                            display: false,
                            text: '{{ isset($total['NAME']) ? $total['NAME'] : '' }}'
                        },
                        tooltips: {
                            mode: 'index',
                            intersect: true
                        }
                    }
                });
                @endif
            @else
                @foreach($list as $key => $data)
                    var ctx4 = document.getElementById("grafikDahorSudah{{ $key+1 }}");
                    var grafikDahorSudah{{ $key+1 }} = new Chart(ctx4, {
                        type: 'horizontalBar',
                        data: {
                            //labels: ["PKRI", "TKR", "DKR", "SRJ"],
                            datasets: [{
                                label: 'PKRI',
                                data: [{{ $data['PKRI_DAHOR_SUDAH'] }}],
                                backgroundColor: 'rgba(128, 0, 0, 0.7)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'TKR',
                                data: [{{ $data['TKR_DAHOR_SUDAH'] }}],
                                backgroundColor: 'rgba(5, 57, 102, 0.8)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'DKR',
                                data: [{{ $data['DKR_DAHOR_SUDAH'] }}],
                                backgroundColor: 'rgba(244, 23, 0, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'SRJ',
                                data: [{{ $data['SRJ_DAHOR_SUDAH'] }}],
                                backgroundColor: 'rgba(0, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'DAHOR'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });

                    var ctx4 = document.getElementById("grafikTuvetSudah{{ $key+1 }}");
                    var grafikTuvetSudah{{ $key+1 }} = new Chart(ctx4, {
                        type: 'horizontalBar',
                        data: {
                            //labels: ["PKRI", "TKR", "DKR", "SRJ"],
                            datasets: [{
                                label: 'PKRI',
                                data: [{{ $data['PKRI_TUVET_SUDAH'] }}],
                                backgroundColor: 'rgba(128, 0, 0, 0.7)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'TKR',
                                data: [{{ $data['TKR_TUVET_SUDAH'] }}],
                                backgroundColor: 'rgba(5, 57, 102, 0.8)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'DKR',
                                data: [{{ $data['DKR_TUVET_SUDAH'] }}],
                                backgroundColor: 'rgba(244, 23, 0, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'SRJ',
                                data: [{{ $data['SRJ_TUVET_SUDAH'] }}],
                                backgroundColor: 'rgba(0, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'TUVET'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });

                    var ctx4 = document.getElementById("grafikDahorBelum{{ $key+1 }}");
                    var grafikDahorBelum{{ $key+1 }} = new Chart(ctx4, {
                        type: 'horizontalBar',
                        data: {
                            //labels: ["PKRI", "TKR", "DKR", "SRJ"],
                            datasets: [{
                                label: 'PKRI',
                                data: [{{ $data['PKRI_DAHOR_BELUM'] }}],
                                backgroundColor: 'rgba(128, 0, 0, 0.7)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'TKR',
                                data: [{{ $data['TKR_DAHOR_BELUM'] }}],
                                backgroundColor: 'rgba(5, 57, 102, 0.8)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'DKR',
                                data: [{{ $data['DKR_DAHOR_BELUM'] }}],
                                backgroundColor: 'rgba(244, 23, 0, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'SRJ',
                                data: [{{ $data['SRJ_DAHOR_BELUM'] }}],
                                backgroundColor: 'rgba(0, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'DAHOR'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });

                    var ctx4 = document.getElementById("grafikTuvetBelum{{ $key+1 }}");
                    var grafikTuvetBelum{{ $key+1 }} = new Chart(ctx4, {
                        type: 'horizontalBar',
                        data: {
                            //labels: ["PKRI", "TKR", "DKR", "SRJ"],
                            datasets: [{
                                label: 'PKRI',
                                data: [{{ $data['PKRI_TUVET_BELUM'] }}],
                                backgroundColor: 'rgba(128, 0, 0, 0.7)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'TKR',
                                data: [{{ $data['TKR_TUVET_BELUM'] }}],
                                backgroundColor: 'rgba(5, 57, 102, 0.8)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'DKR',
                                data: [{{ $data['DKR_TUVET_BELUM'] }}],
                                backgroundColor: 'rgba(244, 23, 0, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }, {
                                label: 'SRJ',
                                data: [{{ $data['SRJ_TUVET_BELUM'] }}],
                                backgroundColor: 'rgba(0, 191, 2, 0.6)',
                                borderColor: 'rgba(5, 57, 102, 0)',
                                borderWidth: 1
                            }]
                        },
                        options: {
                            responsive: true,
                            legend: {
                                position: 'top',
                            },
                            title: {
                                display: true,
                                text: 'TUVET'
                            },
                            animation: {
                                animateScale: true,
                                animateRotate: true
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero:true
                                    }
                                }]
                            }
                        }
                    });
                @endforeach
            @endif
        @endif

        @if ($params['typeData'] == 'tabel')
            $('.divTabel').show();
        @endif
        $('#html').val($('#target').html());
        /*$.ajax({
            url: '{{ route('rekapPerkodamPDF') }}',
            type: 'POST',
            dataType: 'json',
            data: {
                _token: '{{ csrf_token() }}',
                html : $('#target').html()
            },
         })
         .done(function() {
            console.log("success");
         })
         .fail(function() {
            console.log("error");
         })
         .always(function() {
            console.log("complete");
         });*/
          
    });
    jQuery(window).load(function ()
    {
        $('#html').val($('#target').html());
    });
</script>
@endsection