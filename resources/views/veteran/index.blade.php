@extends('layouts.admin')

@section('title', 'Data Veteran')
@section('veteran', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
<style>
    form .btn { width: 100%; }
</style>
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
                <li>
                    <i class="{{ config('icon.sidebarAdmin.veteran') }}"></i>Data Veteran
                </li>
            </ol>
        </div>

        <div class="content">
            {{ csrf_field() }}
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            @if (in_array('ALL', Config::get('menu.role.veteran.export')))
            <div class="row">
                <form action="{{ route('veteran.downloadsample') }}" method="POST" enctype="multipart/form-data">
                    <div class="btn-container col-sm-2">
                        <!-- <a href="{{ url('assets/excel/sample/veteran.xlsx') }}" download class="btn btn-primary">Download Sample Excel</a> -->
                       {{ csrf_field() }}
                       <button type="submit" class="btn btn-primary">Download Sample</button>
                    </div>
                </form>
                <form action="{{ route('veteran.export') }}" method="POST" enctype="multipart/form-data">
                    <div class="btn-container col-sm-2">
                       {{ csrf_field() }}
                       <button type="submit" class="btn btn-primary">Export</button>
                       <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Export</button> -->
                    </div>
                </form>
            </div>
            @else
                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.import')))
                <div class="row">
                    <form action="{{ route('veteran.downloadsample') }}" method="POST" enctype="multipart/form-data">
                        <div class="btn-container col-sm-2">
                            <!-- <a href="{{ url('assets/excel/sample/veteran.xlsx') }}" download class="btn btn-primary">Download Sample Excel</a> -->
                           {{ csrf_field() }}
                           <button type="submit" class="btn btn-primary">Download Sample</button>
                        </div>
                    </form>
                    <form action="{{ route('veteran.export') }}" method="POST" enctype="multipart/form-data">
                        <div class="btn-container col-sm-2">
                           {{ csrf_field() }}
                           <button type="submit" class="btn btn-primary">Export</button>
                           <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Export</button> -->
                        </div>
                    </form>
                </div>
                @endif
            @endif

            <div class="clearfix"></div>
            @if (in_array('ALL', Config::get('menu.role.veteran.import')))
            <div class="row">
                <div class="btn-container col-sm-6">
                    <form action="{{ route('veteran.import') }}" method="POST" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="btn-container pull-left">
                        <label for="uploadFile">Upload File Excel</label><br>
                           <input type="file" name="fileExcel" id="uploadFile" style="display: inline-block;">
                        </div>
                        <div class="btn-container pull-left">
                            <button type="submit" class="btn btn-primary">Import</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr>
            @else
                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.import')))
                <div class="row">
                    <div class="btn-container col-sm-6">
                        <form action="{{ route('veteran.import') }}" method="POST" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="btn-container pull-left">
                            <label for="uploadFile">Upload File Excel</label><br>
                               <input type="file" name="fileExcel" id="uploadFile" style="display: inline-block;">
                            </div>
                            <div class="btn-container pull-left">
                                <button type="submit" class="btn btn-primary">Import</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
                <div class="clearfix"></div>
                @endif
            @endif

            @if (in_array('ALL', Config::get('menu.role.veteran.create')))
            <div class="row">
                <div class="btn-container col-sm-2">
                    <a href="{{ route('veteran.create') }}" class="btn btn-add">Add New</a>
                </div>
            </div>
            @else
                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.create')))
                <div class="row">
                    <div class="btn-container col-sm-2">
                        <a href="{{ route('veteran.create') }}" class="btn btn-add">Add New</a>
                    </div>
                </div>
                @endif
            @endif
            <div class="clearfix"></div>
            <div>
                <form action="" method="get" id="search-form">
                    <div class="row">
                        <div class="col-md-3">
                            <select name="kodam" class="form-control select2" id="kodam"
                            onchange="loadSelect(this, 'kodim', '{{ url('kanminvetcad/get') }}', '{{ isset($item->idKodim) ? $item->idKodim : '' }}');">
                            <!-- <select name="kodam" id="kodam" class="form-control"> -->
                                <option value="">Pilih Babinminvetcaddam</option>
                                @foreach ($list['kodam'] as $key => $data)
                                <option value="{{ $data['idKodam'] }}">{{ $data['namaKodam'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="kodim" id="kodim" class="form-control">
                                <option value="">Pilih Kanminvetcad</option>
                                @foreach ($list['kodim'] as $key => $data)
                                <option value="{{ $data['idKodim'] }}">{{ $data['namaKodim'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-2">
                            <button type="button" onclick="searchForm()" class="btn btn-primary">SEARCH</button>
                        </div>
                        <div class="col-md-1">
                            <button type="reset" class="btn btn-default" onclick="resetData()">RESET</button>
                        </div>
                    </div>
                </form>
            </div><br>
            <div class="container-fluid table-responsive" align="center-block">
                <table class="table table-striped table-hover" id="dataTables">
                    <thead>
                        <tr>

                            @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.destroy')))
                            <th>
                                <div class="btn-group">
                                    <button type="button" class="dropdown-toggle" data-toggle="dropdown">
                                        <input type="checkbox" name="checkBoxAll" id="checkBoxAll">
                                        <i class="fa fa-caret-down" aria-hidden="true"></i>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#" onclick="checkAll('{{ $list['veteran'] }}')">All</a></li>
                                        <li><a href="#" onclick="removeCheckAll()">None</a></li>
                                        <li class="divider"><a href="#">&nbsp;</a></li>
                                        <li><a href="#" onclick="deleteSelected()">Delete</a></li>
                                    </ul>
                                </div>
                            </th>
                            @endif
                            <th>Nama</th>
                            <th>NRP/NIP</th>
                            <th>NPV</th>
                            <th>No Pendaftaran</th>
                            <th>Predikat</th>
                            <th>Provinsi</th>
                            <th>No Karip</th>
                            <!-- <th>Jenis Kelamin</th> -->
                            <th>RH</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
var selectedId = [];
var jsonString = "";
$(document).ready(function() {
    listManager.listUrl = "{{ route('veteran.index') }}/list";
    listManager.deleteUrl = "{{ route('veteran.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.destroy')))
                                {
                                    "data": 'check',
                                    name: 'check',
                                    orderable: false,
                                    searchable: false,
                                    "render": function (data, type, row)
                                    {
                                        if(jQuery.inArray(row.veteranId, selectedId) !== -1)
                                        {
                                            return '<input type="checkbox" name="check" class="checkRow" onChange="selectRow(this, \''+row.idVeteran+'\')" checked>';
                                        }
                                        else
                                        {
                                            return '<input type="checkbox" name="check" class="checkRow" onChange="selectRow(this, \''+row.idVeteran+'\')">';
                                        }
                                    }
                                },
                                @endif
                                { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                { "data": "NIP", "name": "veteran.NIP" },
                                { "data": "npvBaru", "name": "veteran.npvBaru" },
                                { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                { "data": "predikat", "name": "veteran.predikat" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": "noKarip", "name": "veteran.noKarip" },
                                // { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"}
    ];
    listManager.ready();

    /*$('#search-form').on('submit', function(e)
    {
        var kodam = $('#kodam').val();
        var kodim = $('#kodim').val();
        var customSearch = "kodam="+kodam+"&kodim="+kodim;
        listManager.destroy();
        listManager.customSearch = customSearch;
        listManager.listUrl = "{{ route('veteran.index') }}/list";
        listManager.deleteUrl = "{{ route('veteran.index') }}";
        listManager.token = "{{ csrf_token() }}";
        listManager.dataTable = [
                                    { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                    { "data": "NIP", "name": "veteran.NIP" },
                                    { "data": "npvBaru", "name": "veteran.npvBaru" },
                                    { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                    { "data": "predikat", "name": "veteran.predikat" },
                                    { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                    { "data": "noKarip", "name": "veteran.noKarip" },
                                    // { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                    { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                    { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"}
        ];
        listManager.ready();
        e.preventDefault();
    });*/


    /* CHECK DELETE */
    $('#dataTables').on( 'page.dt', function ()
    {
        setTimeout(function()
        {
            $('#checkBoxAll').prop("checked",true);
            $(".checkRow").each( function()
            {
                if(!$(this).is(':checked'))
                {
                    $('#checkBoxAll').prop("checked",false);
                }
            });
        }, 500);
    });

    // CHECK ALL OLD --
    $("#checkAll").on('click',function()
    {
        var status = this.checked;

        if (status)
        {
            id = $("#checkAll").val();
            var obj = $.parseJSON(id);
            $.each(obj, function(index, id)
            {
                selectedId.push(id.toString() );
            });
        }
        else {
            selectedId = [];
        }

        $(".checkRow").each( function()
        {
            $(this).prop("checked",status);
        });
    });
    // CHECK ALL OLD --
});

function searchForm()
{
    var kodam = $('#kodam').val();
    var kodim = $('#kodim').val();
    var customSearch = "kodam="+kodam+"&kodim="+kodim;
    listManager.destroy();
    listManager.customSearch = customSearch;
    listManager.listUrl = "{{ route('veteran.index') }}/list";
    listManager.deleteUrl = "{{ route('veteran.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.destroy')))
                                {
                                    "data": 'check',
                                    name: 'check',
                                    orderable: false,
                                    searchable: false,
                                    "render": function (data, type, row)
                                    {
                                        if(jQuery.inArray(row.veteranId, selectedId) !== -1)
                                        {
                                            return '<input type="checkbox" name="check" class="checkRow" onChange="selectRow(this, \''+row.idVeteran+'\')" checked>';
                                        }
                                        else
                                        {
                                            return '<input type="checkbox" name="check" class="checkRow" onChange="selectRow(this, \''+row.idVeteran+'\')">';
                                        }
                                    }
                                },
                                @endif
                                { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                { "data": "NIP", "name": "veteran.NIP" },
                                { "data": "npvBaru", "name": "veteran.npvBaru" },
                                { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                { "data": "predikat", "name": "veteran.predikat" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": "noKarip", "name": "veteran.noKarip" },
                                // { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"}
    ];
    listManager.ready();
}

function resetData()
{
    listManager.destroy();
    listManager.customSearch = "";
    listManager.listUrl = "{{ route('veteran.index') }}/list";
    listManager.deleteUrl = "{{ route('veteran.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.destroy')))
                                {
                                    "data": 'check',
                                    name: 'check',
                                    orderable: false,
                                    searchable: false,
                                    "render": function (data, type, row)
                                    {
                                        if(jQuery.inArray(row.veteranId, selectedId) !== -1)
                                        {
                                            return '<input type="checkbox" name="check" class="checkRow" onChange="selectRow(this, \''+row.idVeteran+'\')" checked>';
                                        }
                                        else
                                        {
                                            return '<input type="checkbox" name="check" class="checkRow" onChange="selectRow(this, \''+row.idVeteran+'\')">';
                                        }
                                    }
                                },
                                @endif
                                { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                { "data": "NIP", "name": "veteran.NIP" },
                                { "data": "npvBaru", "name": "veteran.npvBaru" },
                                { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                { "data": "predikat", "name": "veteran.predikat" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": "noKarip", "name": "veteran.noKarip" },
                                // { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"}
    ];
    listManager.ready();
}

function checkAll(id)
{
   /*var obj = $.parseJSON(id);
   $.each(obj, function(index, id)
   {
      selectedId.push(id.toString() );
   });*/
   $('#checkBoxAll').prop("checked",true);
   $(".checkRow").each( function()
   {
      // $(this).prop("checked",true);
      $(this).prop("checked",true).trigger("change");
   });
}

function removeCheckAll()
{
   // selectedId = [];
   $('#checkBoxAll').prop("checked",false);
   $(".checkRow").each( function()
   {
      $(this).prop("checked",false).trigger("change");
      // $(this).prop("checked",false);
   });
}

function selectRow(self, id)
{
   if ($(self).is(':checked'))
   {
      // SET CHECK
      selectedId.push(id);
      $('#checkBoxAll').prop("checked",true);
      $(".checkRow").each( function()
      {
         if(!$(this).is(':checked'))
         {
            $('#checkBoxAll').prop("checked",false);
         }
      });
   }
   else {
      // REMOVE ID
      $('#checkBoxAll').prop("checked",false);
      selectedId = jQuery.grep(selectedId, function(value) {
         return value != id;
      });
   }
}

// ACTION DELETE
function deleteSelected()
{
   var yes = confirm('Are you sure want to delete selected data ?');
   if (yes)
   {
      $.ajax({
         type: "POST",
         url: "{{ url('veteran/delete/selected') }}",
         dataType: "JSON",
         data: {
            selectedId: selectedId,
            _token: '{{ csrf_token() }}'
         },
         success:function(resp)
         {
            try {
               if(resp.statusCode == "200")
               {
                  $('#checkBoxAll').prop("checked",false);
                  listManager.destroy();
                  listManager.ready();
                  // location.reload();
               }
               else {
                  // location.reload();
               }
            } catch(e) {
               // location.reload();
            }
         },
         error:function()
         {
            // location.reload();
         }
      });
   }
}

</script>
@endsection