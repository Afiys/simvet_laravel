@extends('layouts.admin')

@section('title', 'Veteran')
@section('veteran', 'active')

@section('css')
<link href="{{ url('assets/components/select2/dist/css/select2.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
   	<div class="row row-offcanvas row-offcanvas-left">
		
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
			<ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
				<li>
					<a href="{{ route('veteran.index') }}"><i class="{{ config('icon.sidebarAdmin.veteran') }}"></i>Veteran</a>&nbsp;&nbsp;&nbsp;
				</li>
				<li>
					<i class="{{ config('icon.'.($id == 0 ? 'add' : 'update')) }}" aria-hidden="true"></i>{{ ($id == 0 ? 'Tambah' : 'Edit') }}
				</li>
			</ol>
        </div>

		<div class="content">
			<div class="panel panel-primary">
				<div class="panel-heading">RH {{ old('namaVeteran', isset($item->namaVeteran) ? $item->namaVeteran : '') }}</div>
				<form method="POST" class="form-horizontal" action="{{ url(route('veteran.index') . ($id == 0 ? '' : '/' . $id)) }}" enctype="multipart/form-data">
					<div class="panel-body">
						@if ($errors->any())
						<div class="alert alert-danger">
						    <ul>
						        @foreach ($errors->all() as $error)
						            <li>{{ $error }}</li>
						        @endforeach
						    </ul>
						</div>
						@endif
					    {{ csrf_field() }}
					    @if($id != 0)
					    <input name="_method" type="hidden" value="PUT">
						@endif
						<div class="row">
							<div class="col-lg-12">
								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#halaman1">Halaman 1</a></li>
									<li><a data-toggle="tab" href="#halaman2">Halaman 2</a></li>
									<li><a data-toggle="tab" href="#halaman3">Halaman 3</a></li>
								</ul>

								<div class="tab-content"><br>
									<div id="halaman1" class="tab-pane fade in active">
										<div class="row">
											<div class="col-md-4">
				                                <div class="form-group">
				                                    <label for="image" class="col-sm-5 control-label">&nbsp;</label>
				                                    <!-- <label for="image" class="col-sm-5 control-label">Foto</label> -->
				                                    <div class="col-sm-4" style="padding-left: 35px;">
				                                        <div class="box" style="width: 200px; height: 220px;">
				                                            <div class="js--image-preview" style="background-image: url('{{ (isset($item->imageUrl) ? image($item->imageUrl, Config::get('app.directory.veteran')) : '') }}'); width: 200px; height: 220px;"></div>
				                                            <!-- <div class="upload-options">
				                                                <label>
				                                                    <input type="file" class="image-upload" accept="image/*" name="image" id="image"  disabled=""/>
				                                                </label>
				                                            </div> -->
				                                        </div>
				                                        <!-- <small>Recommended dimension: 640 x 480 px</small> -->
				                                    </div>
												</div>
			                                </div>

			                                <div class="col-md-8">
												<div class="form-group">
													<label for="namaVeteran" class="col-sm-3 control-label">Nama Veteran</label>
													<div class="col-sm-6">
														<input type="text" name="namaVeteran" class="form-control" id="namaVeteran" placeholder="Nama Veteran" value="{{ old('namaVeteran', isset($item->namaVeteran) ? $item->namaVeteran : '') }}" disabled=""/>
													</div>
												</div>
												<div class="form-group">
													<label for="tempatLahir" class="col-sm-3 control-label">Tempat Lahir</label>
													<div class="col-sm-6">
														<input type="text" name="tempatLahir" class="form-control" id="tempatLahir" placeholder="Tempat Lahir" value="{{ old('tempatLahir', isset($item->tempatLahir) ? $item->tempatLahir : '') }}" disabled=""/>
													</div>
												</div>
												<div class="form-group">
													<label for="tanggalLahir" class="col-sm-3 control-label">Tanggal Lahir</label>
													<div class="col-sm-6">
														<input type="text" name="tanggalLahir" class="form-control" id="tanggalLahir" placeholder="Tanggal Lahir" value="{{ old('tanggalLahir', isset($item->tanggalLahir) ? formatDate($item->tanggalLahir, 'd-m-Y') : '') }}" disabled=""/>
													</div>
												</div>
												<div class="form-group">
													<label for="jenisKelamin" class="col-sm-3 control-label">Jenis Kelamin</label>
													<div class="col-sm-6">
														<input type="text" name="jenisKelamin" class="form-control" id="jenisKelamin" placeholder="Jenis Kelamin" value="{{ old('jenisKelamin', (isset($item->jenisKelamin) && $item->jenisKelamin != '') ? (($item->jenisKelamin == 'L') ? 'Laki - Laki' :'Perempuan' ) : '') }}" disabled=""/>
														<!-- 
														<select name="jenisKelamin" class="form-control select2" id="jenisKelamin">
															<option value="">Pilih Jenis Kelamin</option>}
															<option value="L" {{ old('jenisKelamin', (isset($item->jenisKelamin) && $item->jenisKelamin == 'L') ? 'selected' : '') }}>Laki-laki</option>}
															<option value="P" {{ old('jenisKelamin', (isset($item->jenisKelamin) && $item->jenisKelamin == 'P') ? 'selected' : '') }}>Perempuan</option>}
														</select> -->
													</div>
												</div>
												<div class="form-group">
													<label for="NIK" class="col-sm-3 control-label">NIK</label>
													<div class="col-sm-6">
														<input type="text" name="NIK" class="form-control" id="NIK" placeholder="NIK" value="{{ old('NIK', isset($item->NIK) ? $item->NIK : '') }}" disabled=""/>
													</div>
												</div>
			                                </div>
			                            </div>
										<div class="form-group">
											<label for="NIP" class="col-sm-2 control-label">NRP/NIP</label>
											<div class="col-sm-8">
												<input type="text" name="NIP" class="form-control" id="NIP" placeholder="NRP/NIP" value="{{ old('NIP', isset($item->NIP) ? $item->NIP : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="noPendaftaran" class="col-sm-2 control-label">No Pendaftaran</label>
											<div class="col-sm-3">
												<input type="text" name="noPendaftaran" class="form-control" id="noPendaftaran" placeholder="No Pendaftaran" value="{{ old('noPendaftaran', isset($item->noPendaftaran) ? $item->noPendaftaran : '') }}" disabled=""/>
											</div>
											<label for="tanggalPendaftaran" class="col-sm-2 control-label">Tanggal Pendaftaran</label>
											<div class="col-sm-3">
												<input type="text" name="tanggalPendaftaran" class="form-control" id="tanggalPendaftaran" placeholder="Tanggal Pendaftaran" value="{{ old('tanggalPendaftaran', isset($item->tanggalPendaftaran) ? formatDate($item->tanggalPendaftaran, 'd-m-Y') : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="npvLama" class="col-sm-2 control-label">NPV Lama</label>
											<div class="col-sm-3">
												<input type="text" name="npvLama" class="form-control" id="npvLama" placeholder="NPV Lama" value="{{ old('npvLama', isset($item->npvLama) ? $item->npvLama : '') }}" disabled=""/>
											</div>
											<label for="npvBaru" class="col-sm-2 control-label">NPV Baru</label>
											<div class="col-sm-3">
												<input type="text" name="npvBaru" class="form-control" id="npvBaru" placeholder="NPV Baru" value="{{ old('npvBaru', isset($item->npvBaru) ? $item->npvBaru : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="noKeputusan" class="col-sm-2 control-label">No Keputusan</label>
											<div class="col-sm-3">
												<input type="text" name="noKeputusan" class="form-control" id="noKeputusan" placeholder="No Keputusan" value="{{ old('noKeputusan', isset($item->noKeputusan) ? $item->noKeputusan : '') }}" disabled=""/>
											</div>
											<label for="tanggalKeputusan" class="col-sm-2 control-label">Tanggal Keputusan</label>
											<div class="col-sm-3">
												<input type="text" name="tanggalKeputusan" class="form-control" id="tanggalKeputusan" placeholder="Tanggal Keputusan" value="{{ old('tanggalKeputusan', isset($item->tanggalKeputusan) ? formatDate($item->tanggalKeputusan, 'd-m-Y') : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="predikat" class="col-sm-2 control-label">Predikat</label>
											<div class="col-sm-8">
												<input type="text" name="predikat" class="form-control" id="predikat" placeholder="Predikat" value="{{ old('predikat', isset($item->predikat) ? $item->predikat : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="golongan" class="col-sm-2 control-label">Golongan</label>
											<div class="col-sm-1">
												<input type="text" name="golongan" class="form-control" id="golongan" placeholder="Golongan" value="{{ old('golongan', isset($item->golongan) ? $item->golongan : '') }}" disabled=""/>
											</div>
											<label for="tahunMasaBakti" class="col-sm-2 control-label" style="width: 10%;">Masa Bakti</label>
											<div class="col-sm-2" style="padding-right: 0;">
												<input type="text" name="tahunMasaBakti" class="form-control" id="tahunMasaBakti" placeholder="Tahun Masa Bakti" value="{{ old('tahunMasaBakti', isset($item->tahunMasaBakti) ? $item->tahunMasaBakti : '') }}" disabled=""/>
											</div>
											<label for="tahunMasaBakti" class="col-sm-1 control-label" style="width: 6%;">Tahun</label>
											<div class="col-sm-2" style="margin-left: 30px; padding-right: 0;">
												<input type="text" name="bulanMasaBakti" class="form-control" id="bulanMasaBakti" placeholder="Bulan Masa Bakti" value="{{ old('bulanMasaBakti', isset($item->bulanMasaBakti) ? $item->bulanMasaBakti : '') }}" disabled=""/>
											</div>
											<label for="bulanMasaBakti" class="col-sm-1 control-label" style="width: 6%;">Bulan</label>
										</div>
										<div class="form-group">
											<label for="tanggalMeninggal" class="col-sm-2 control-label">Tanggal Meninggal</label>
											<div class="col-sm-3">
												<input type="text" name="tanggalMeninggal" class="form-control" id="tanggalMeninggal" placeholder="Tanggal Meninggal" value="{{ old('tanggalMeninggal', isset($item->tanggalMeninggal) ? formatDate($item->tanggalMeninggal, 'd-m-Y') : '') }}" disabled=""/>
											</div>
											<label for="statusMeninggal" class="col-sm-2 control-label">Hidup/Mati</label>
											<div class="col-sm-3">
												<select name="statusMeninggal" id="statusMeninggal" class="form-control select2" disabled="">
													<option value="tidak" {{ old('statusMeninggal', isset($item->statusMeninggal) && $item->statusMeninggal == 'tidak' ? 'selected' : '') }}>Hidup</option>
													<option value="ya" {{ old('statusMeninggal',isset($item->statusMeninggal) && $item->statusMeninggal == 'ya' ? 'selected' : '') }}>Mati</option>
												</select>
											</div>
										</div>
									</div>

									<div id="halaman2" class="tab-pane fade">
										<div class="form-group">
											<label for="namaAhliWaris" class="col-sm-2 control-label">Nama Ahli Waris</label>
											<div class="col-sm-8">
												<input type="text" name="namaAhliWaris" class="form-control" id="namaAhliWaris" placeholder="Nama Ahli Waris" value="{{ old('namaAhliWaris', isset($item->namaAhliWaris) ? $item->namaAhliWaris : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="tanggalLahirAhliWaris" class="col-sm-2 control-label">Tanggal Lahir Ahli Waris</label>
											<div class="col-sm-8">
												<input type="text" name="tanggalLahirAhliWaris" class="form-control" id="tanggalLahirAhliWaris" placeholder="Tanggal Lahir Ahli Waris" value="{{ old('tanggalLahirAhliWaris', isset($item->tanggalLahirAhliWaris) ? formatDate($item->tanggalLahirAhliWaris, 'd-m-Y') : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="NIKAhliWaris" class="col-sm-2 control-label">NIK Ahli Waris</label>
											<div class="col-sm-8">
												<input type="text" name="NIKAhliWaris" class="form-control" id="NIKAhliWaris" placeholder="NIK Ahli Waris" value="{{ old('NIKAhliWaris', isset($item->NIKAhliWaris) ? $item->NIKAhliWaris : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="tanggalMeninggalAhliWaris" class="col-sm-2 control-label">Tanggal Meninggal Ahli Waris</label>
											<div class="col-sm-8">
												<input type="text" name="tanggalMeninggalAhliWaris" class="form-control" id="tanggalMeninggalAhliWaris" placeholder="Tanggal Meninggal Ahli Waris" value="{{ old('tanggalMeninggalAhliWaris', isset($item->tanggalMeninggalAhliWaris) ? formatDate($item->tanggalMeninggalAhliWaris, 'd-m-Y') : '') }}" disabled=""/>
											</div>
										</div>

							            <div class="form-group">
							               <label for="Babinminvetcaddam" class="col-sm-2 control-label">Babinminvetcaddam</label>
							               <div class="col-sm-8">
												<select name="idKodam" class="form-control select2" id="Babinminvetcaddam"
												onchange="loadSelect(this, 'Kanminvetcad', '{{ url('kanminvetcad/get') }}', '{{ isset($item->idKodim) ? $item->idKodim : '' }}');loadSelect(this, 'Provinsi', '{{ url('provinsi/get') }}', '{{ isset($item->idProvinsi) ? $item->idProvinsi : '' }}');" disabled="">
													<option value="">Pilih Babinminvetcaddam</option>
													@foreach ($lookupTable['kodam'] as $kodam)
													<option value="{{ $kodam->idKodam }}" {{ (isset($item) && $item->idKodam == $kodam->idKodam) ? 'selected=selected' : '' }}>{{ $kodam->namaKodam }}</option>
													@endforeach
												</select>
							               </div>
							            </div>

							            <div class="form-group">
							               <label for="Kanminvetcad" class="col-sm-2 control-label">Kanminvetcad</label>
							               <div class="col-sm-8">
												<select name="idKodim" class="form-control select2" id="Kanminvetcad" disabled="">
													<option value="">Pilih Kanminvetcad</option>
												</select>
							               </div>
							            </div>
							            
							            <div class="form-group">
							               <label for="Provinsi" class="col-sm-2 control-label">Provinsi</label>
							               <div class="col-sm-8">
												<select name="idProvinsi" class="form-control select2" id="Provinsi" onchange="loadSelect(this, 'Kabupaten', '{{ url('kabupaten/get') }}', '{{ isset($item) ? $item->idKabupaten : '' }}');" disabled="">
													<option value="">Pilih Provinsi</option>
												</select>
							               </div>
							            </div>
							            
							            <div class="form-group">
							               <label for="Kabupaten" class="col-sm-2 control-label">Kabupaten</label>
							               <div class="col-sm-8">
												<select name="idKabupaten" class="form-control select2" id="Kabupaten" disabled="">
													<option value="">Pilih Kabupaten</option>
												</select>
							               </div>
							            </div>
										<div class="form-group">
											<label for="kecamatan" class="col-sm-2 control-label">Kecamatan</label>
											<div class="col-sm-8">
												<input type="text" name="kecamatan" class="form-control" id="kecamatan" placeholder="Kecamatan" value="{{ old('kecamatan', isset($item->kecamatan) ? $item->kecamatan : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="kelurahan" class="col-sm-2 control-label">Kelurahan</label>
											<div class="col-sm-8">
												<input type="text" name="kelurahan" class="form-control" id="kelurahan" placeholder="Kelurahan" value="{{ old('kelurahan', isset($item->kelurahan) ? $item->kelurahan : '') }}" disabled=""/>
											</div>
										</div>

										<div class="form-group">
											<label for="alamat" class="col-sm-2 control-label">Alamat</label>
											<div class="col-sm-8">
												<textarea name="alamat" class="form-control" id="alamat" placeholder="Alamat" disabled="">{{ old('alamat', isset($item->alamat) ? $item->alamat : '') }}</textarea>
											</div>
										</div>
										<div class="form-group">
											<label for="rt" class="col-sm-2 control-label">RT</label>
											<div class="col-sm-2">
												<input type="text" name="rt" class="form-control" id="rt" placeholder="RT" value="{{ old('rt', isset($item->rt) ? $item->rt : '') }}" disabled=""/>
											</div>
											<label for="rw" class="col-sm-1 control-label">RW</label>
											<div class="col-sm-2">
												<input type="text" name="rw" class="form-control" id="rw" placeholder="RW" value="{{ old('rw', isset($item->rw) ? $item->rw : '') }}" disabled=""/>
											</div>
										</div>
									</div>
									<div id="halaman3" class="tab-pane fade">
										<div class="form-group">
											<label for="noKepDahor" class="col-sm-2 control-label">No Kep Dahor</label>
											<div class="col-sm-4">
												<input type="text" name="noKepDahor" class="form-control" id="noKepDahor" placeholder="No Kep Dahor" value="{{ old('noKepDahor', isset($item->noKepDahor) ? $item->noKepDahor : '') }}" disabled=""/>
											</div>
											<label for="tglKepDahor" class="col-sm-2 control-label">Tanggal Kep Dahor</label>
											<div class="col-sm-2">
												<input type="text" name="tglKepDahor" class="form-control" id="tglKepDahor" placeholder="Tanggal Kep Dahor" value="{{ old('tglKepDahor', isset($item->tglKepDahor) ? formatDate($item->tglKepDahor, 'd-m-Y') : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="noKepTuvet" class="col-sm-2 control-label">No Kep Tuvet</label>
											<div class="col-sm-4">
												<input type="text" name="noKepTuvet" class="form-control" id="noKepTuvet" placeholder="No Kep Tuvet" value="{{ old('noKepTuvet', isset($item->noKepTuvet) ? $item->noKepTuvet : '') }}" disabled=""/>
											</div>
											<label for="tglKepTuvet" class="col-sm-2 control-label">Tanggal Kep Tuvet</label>
											<div class="col-sm-2">
												<input type="text" name="tglKepTuvet" class="form-control" id="tglKepTuvet" placeholder="Tanggal Kep Tuvet" value="{{ old('tglKepTuvet', isset($item->tglKepTuvet) ? formatDate($item->tglKepTuvet, 'd-m-Y') : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="noKepTundayatu" class="col-sm-2 control-label">No Kep Tundayatu</label>
											<div class="col-sm-4">
												<input type="text" name="noKepTundayatu" class="form-control" id="noKepTundayatu" placeholder="No Kep Tundayatu" value="{{ old('noKepTundayatu', isset($item->noKepTundayatu) ? $item->noKepTundayatu : '') }}" disabled=""/>
											</div>
											<label for="tglKepTundayatu" class="col-sm-2 control-label">Tanggal Kep Tundayatu</label>
											<div class="col-sm-2">
												<input type="text" name="tglKepTundayatu" class="form-control" id="tglKepTundayatu" placeholder="Tanggal Kep Tundayatu" value="{{ old('tglKepTundayatu', isset($item->tglKepTundayatu) ? formatDate($item->tglKepTundayatu, 'd-m-Y') : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="nomorRekening" class="col-sm-2 control-label">Nomor Rekening</label>
											<div class="col-sm-4">
												<input type="text" name="nomorRekening" class="form-control" id="nomorRekening" placeholder="Nomor Rekening" value="{{ old('nomorRekening', isset($item->nomorRekening) ? $item->nomorRekening : '') }}" disabled=""/>
											</div>
											<label for="tempatPembayaran" class="col-sm-2 control-label">Tempat Pembayaran</label>
											<div class="col-sm-2">
												<input type="text" name="tempatPembayaran" class="form-control" id="tempatPembayaran" placeholder="Tempat Pembayaran" value="{{ old('tempatPembayaran', isset($item->tempatPembayaran) ? $item->tempatPembayaran : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="noKarip" class="col-sm-2 control-label">No Karip</label>
											<div class="col-sm-8">
												<input type="text" name="noKarip" class="form-control" id="noKarip" placeholder="No Karip" value="{{ old('noKarip', isset($item->noKarip) ? $item->noKarip : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="PTTaspen" class="col-sm-2 control-label">PT Taspen</label>
											<div class="col-sm-8">
												<input type="text" name="PTTaspen" class="form-control" id="PTTaspen" placeholder="PT Taspen" value="{{ old('PTTaspen', isset($item->PTTaspen) ? $item->PTTaspen : '') }}" disabled=""/>
											</div>
										</div>
										<div class="form-group">
											<label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
											<div class="col-sm-8">
												<textarea name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan" disabled="">{{ old('keterangan', isset($item->keterangan) ? $item->keterangan : '') }}</textarea>
											</div>
										</div>
									</div>

								</div>

					      	</div>
					    </div>
					</div>
					<div class="panel-footer">

            			<!-- @if (in_array('ALL', Config::get('menu.role.veteran.update')))
						<div class="col-lg-2">
							<a href="{{ route('veteran.index') }}" class="btn btn-md btn-block btn-primary">Edit</a>
						</div>
						@else
							@if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.veteran.update')))
							<div class="col-lg-2">
								<a href="{{ route('veteran.index') }}" class="btn btn-md btn-block btn-primary">Edit</a>
							</div>
							@endif
						@endif -->
						<div class="col-lg-2">
							<a href="{{ route('veteran.index') }}" class="btn btn-md btn-block btn-default">Back</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
      	</div>
  	</div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/js/upload.single.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('assets/components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
	$(document).ready(function()
	{
		$(".select2").select2();
		{!! (isset($item->idKodam) && $item->idKodam) ? '$("#Babinminvetcaddam").val("'.$item->idKodam.'").change();' : '' !!}
	});
</script>
@endsection