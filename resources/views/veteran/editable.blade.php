@extends('layouts.admin')

@section('title', 'Veteran')
@section('veteran', 'active')

@section('css')
<link href="{{ url('assets/components/select2/dist/css/select2.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
   	<div class="row row-offcanvas row-offcanvas-left">
		
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
			<ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
				<li>
					<a href="{{ route('veteran.index') }}"><i class="{{ config('icon.sidebarAdmin.veteran') }}"></i>Veteran</a>&nbsp;&nbsp;&nbsp;
				</li>
				<li>
					<i class="{{ config('icon.'.($id == 0 ? 'add' : 'update')) }}" aria-hidden="true"></i>{{ ($id == 0 ? 'Tambah' : 'Edit') }}
				</li>
			</ol>
        </div>
        <div id="outer-cont">
			<div class="content" id="preview">
				<div class="panel panel-primary">
					<div class="panel-heading">{{ ($id == 0 ? 'Tambah' : 'Edit') }} Veteran</div>
					<form method="POST" class="form-horizontal" id="form" action="{{ url(route('veteran.index') . ($id == 0 ? '' : '/' . $id)) }}" enctype="multipart/form-data">
						<div class="panel-body">
							@if ($errors->any())
							<div class="alert alert-danger">
							    <ul>
							        @foreach ($errors->all() as $error)
							            <li>{{ $error }}</li>
							        @endforeach
							    </ul>
							</div>
							@endif
						    {{ csrf_field() }}
						    @if($id != 0)
						    <input name="_method" type="hidden" value="PUT">
							@endif
							<div class="row">
								<div class="col-lg-12">
									<ul class="nav nav-tabs">
										<li class="active"><a data-toggle="tab" href="#halaman1">Halaman 1</a></li>
										<li><a data-toggle="tab" href="#halaman2">Halaman 2</a></li>
										<li><a data-toggle="tab" href="#halaman3">Halaman 3</a></li>
									</ul>

									<div class="tab-content"><br>
										<div id="halaman1" class="tab-pane fade in active">
											<div class="row">
												<div class="col-md-4">
					                                <div class="form-group">
					                                    <label for="image" class="col-sm-5 control-label">&nbsp;</label>
					                                    <!-- <label for="image" class="col-sm-5 control-label">Foto</label> -->
					                                    <div class="col-sm-4" style="padding-left: 35px;">
					                                        <div class="box" style="width: 200px; height: 220px;">
					                                            <div class="js--image-preview" style="background-image: url('{{ (isset($item->imageUrl) ? image($item->imageUrl, Config::get('app.directory.veteran')) : '') }}'); width: 200px; height: 200px;"></div>
					                                            <div class="upload-options">
					                                                <label>
					                                                    <input type="file" class="image-upload" accept="image/*" name="image" id="image" />
					                                                </label>
					                                            </div>
					                                        </div>
					                                        <!-- <div style="margin-left: 10px; width: 100%;" id="browse-media">
					                                        	<a href="javascript:;" class="btn btn-primary" style="width: 200px; font-size: 14px; padding: 0; color: #e6e6e6;" onclick="modalMedia(this)">Browse Media</a>
					                                        	<input type="hidden" name="fileBrowseMedia" id="fileBrowseMedia" value="">
					                                        </div> -->
					                                        <!-- <small>Recommended dimension: 640 x 480 px</small> -->
					                                    </div>
													</div>
				                                </div>

				                                <div class="col-md-8">
													<div class="form-group">
														<label for="namaVeteran" class="col-sm-3 control-label">Nama Veteran</label>
														<div class="col-sm-6">
															<input type="text" name="namaVeteran" class="form-control" id="namaVeteran" placeholder="Nama Veteran" value="{{ old('namaVeteran', isset($item->namaVeteran) ? $item->namaVeteran : '') }}"/>
														</div>
													</div>
													<div class="form-group">
														<label for="tempatLahir" class="col-sm-3 control-label">Tempat Lahir</label>
														<div class="col-sm-6">
															<input type="text" name="tempatLahir" class="form-control" id="tempatLahir" placeholder="Tempat Lahir" value="{{ old('tempatLahir', isset($item->tempatLahir) ? $item->tempatLahir : '') }}"/>
														</div>
													</div>
													<div class="form-group">
														<label for="tanggalLahir" class="col-sm-3 control-label">Tanggal Lahir</label>
														<div class="col-sm-6">
															<input type="date" name="tanggalLahir" class="form-control" id="tanggalLahir" placeholder="Tanggal Lahir" value="{{ old('tanggalLahir', isset($item->tanggalLahir) ? $item->tanggalLahir : '') }}"/>
														</div>
													</div>
													<div class="form-group select">
														<label for="jenisKelamin" class="col-sm-3 control-label">Jenis Kelamin</label>
														<div class="col-sm-6">
															<div class="selectDiv">
																<select name="jenisKelamin" class="form-control select2" id="jenisKelamin">
																	<option value="">Pilih Jenis Kelamin</option>}
																	<option value="L" {{ old('jenisKelamin', (isset($item->jenisKelamin) && $item->jenisKelamin == 'L') ? 'selected' : '') }}>Laki-laki</option>}
																	<option value="P" {{ old('jenisKelamin', (isset($item->jenisKelamin) && $item->jenisKelamin == 'P') ? 'selected' : '') }}>Perempuan</option>}
																</select>
															</div>
															<input type="text" name="" value="" id="jenisKelaminInput" class="form-control" style="display: none;">
														</div>
													</div>
													<div class="form-group">
														<label for="NIK" class="col-sm-3 control-label">NIK</label>
														<div class="col-sm-6">
															<input type="text" name="NIK" class="form-control" id="NIK" placeholder="NIK" value="{{ old('NIK', isset($item->NIK) ? $item->NIK : '') }}"/>
														</div>
													</div>
				                                </div>
				                            </div>

											<div class="form-group">
												<label for="NIP" class="col-sm-2 control-label">NRP/NIP</label>
												<div class="col-sm-8">
													<input type="text" name="NIP" class="form-control" id="NIP" placeholder="NRP/NIP" value="{{ old('NIP', isset($item->NIP) ? $item->NIP : '') }}"/>
												</div>
											</div>
											<div class="form-group show-selectnav show-week hide-btnrow">
												<label for="noPendaftaran" class="col-sm-2 control-label">No Pendaftaran</label>
												<div class="col-sm-3">
													<input type="text" name="noPendaftaran" class="form-control" id="noPendaftaran" placeholder="No Pendaftaran" value="{{ old('noPendaftaran', isset($item->noPendaftaran) ? $item->noPendaftaran : '') }}"/>
												</div>
												<label for="tanggalPendaftaran" class="col-sm-2 control-label">Tanggal Pendaftaran</label>
												<div class="col-sm-3">
													<input type="date" name="tanggalPendaftaran" class="form-control" id="tanggalPendaftaran" placeholder="Tanggal Pendaftaran" value="{{ old('tanggalPendaftaran', isset($item->tanggalPendaftaran) ? $item->tanggalPendaftaran : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="npvLama" class="col-sm-2 control-label">NPV Lama</label>
												<div class="col-sm-3">
													<input type="text" name="npvLama" class="form-control" id="npvLama" placeholder="NPV Lama" value="{{ old('npvLama', isset($item->npvLama) ? $item->npvLama : '') }}"/>
												</div>
												<label for="npvBaru" class="col-sm-2 control-label">NPV Baru</label>
												<div class="col-sm-3">
													<input type="text" name="npvBaru" class="form-control" id="npvBaru" placeholder="NPV Baru" value="{{ old('npvBaru', isset($item->npvBaru) ? $item->npvBaru : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="noKeputusan" class="col-sm-2 control-label">No Keputusan</label>
												<div class="col-sm-3">
													<input type="text" name="noKeputusan" class="form-control" id="noKeputusan" placeholder="No Keputusan" value="{{ old('noKeputusan', isset($item->noKeputusan) ? $item->noKeputusan : '') }}"/>
												</div>
												<label for="tanggalKeputusan" class="col-sm-2 control-label">Tanggal Keputusan</label>
												<div class="col-sm-3">
													<input type="date" name="tanggalKeputusan" class="form-control" id="tanggalKeputusan" placeholder="Tanggal Keputusan" value="{{ old('tanggalKeputusan', isset($item->tanggalKeputusan) ? $item->tanggalKeputusan : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="predikat" class="col-sm-2 control-label">Predikat</label>
												<div class="col-sm-8">
													<input type="text" name="predikat" class="form-control" id="predikat" placeholder="Predikat" value="{{ old('predikat', isset($item->predikat) ? $item->predikat : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="golongan" class="col-sm-2 control-label">Golongan</label>
												<div class="col-sm-1">
													<input type="text" name="golongan" class="form-control" id="golongan" placeholder="Golongan" value="{{ old('golongan', isset($item->golongan) ? $item->golongan : '') }}"/>
												</div>
												<label for="tahunMasaBakti" class="col-sm-2 control-label" style="width: 10%;">Masa Bakti</label>
												<div class="col-sm-2" style="padding-right: 0;">
													<input type="text" name="tahunMasaBakti" class="form-control" id="tahunMasaBakti" placeholder="Tahun Masa Bakti" value="{{ old('tahunMasaBakti', isset($item->tahunMasaBakti) ? $item->tahunMasaBakti : '') }}"/>
												</div>
												<label for="tahunMasaBakti" class="col-sm-1 control-label" style="width: 6%;">Tahun</label>
												<div class="col-sm-2" style="margin-left: 30px; padding-right: 0;">
													<input type="text" name="bulanMasaBakti" class="form-control" id="bulanMasaBakti" placeholder="Bulan Masa Bakti" value="{{ old('bulanMasaBakti', isset($item->bulanMasaBakti) ? $item->bulanMasaBakti : '') }}"/>
												</div>
											<label for="bulanMasaBakti" class="col-sm-1 control-label" style="width: 6%;">Bulan</label>
											</div>
											<div class="form-group">
												<label for="tanggalMeninggal" class="col-sm-2 control-label">Tanggal Meninggal</label>
												<div class="col-sm-3">
													<input type="date" id="tanggalMeninggal" name="tanggalMeninggal" class="form-control" id="tanggalMeninggal" placeholder="Tanggal Meninggal" value="{{ old('tanggalMeninggal', isset($item->tanggalMeninggal) ? $item->tanggalMeninggal : '') }}" onchange="changeStatusMeninggal(this)" />
												</div>
												<label for="statusMeninggal" class="col-sm-2 control-label">Hidup/Mati</label>
												<div class="col-sm-3">
													<select name="statusMeninggal" id="statusMeninggal" class="form-control select2" onchange="changeTanggalMeninggal(this)">
														<option value="tidak" {{ old('statusMeninggal', isset($item->statusMeninggal) && $item->statusMeninggal == 'tidak' ? 'selected' : '') }}>Hidup</option>
														<option value="ya" {{ old('statusMeninggal', isset($item->statusMeninggal) && $item->statusMeninggal == 'ya' ? 'selected' : '') }}>Mati</option>
													</select>
												</div>
											</div>
										</div>

										<div id="halaman2" class="tab-pane fade">
											<div class="form-group">
												<label for="namaAhliWaris" class="col-sm-2 control-label">Nama Ahli Waris</label>
												<div class="col-sm-8">
													<input type="text" name="namaAhliWaris" class="form-control" id="namaAhliWaris" placeholder="Nama Ahli Waris" value="{{ old('namaAhliWaris', isset($item->namaAhliWaris) ? $item->namaAhliWaris : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="tanggalLahirAhliWaris" class="col-sm-2 control-label">Tanggal Lahir Ahli Waris</label>
												<div class="col-sm-8">
													<input type="date" name="tanggalLahirAhliWaris" class="form-control" id="tanggalLahirAhliWaris" placeholder="Tanggal Lahir Ahli Waris" value="{{ old('tanggalLahirAhliWaris', isset($item->tanggalLahirAhliWaris) ? $item->tanggalLahirAhliWaris : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="NIKAhliWaris" class="col-sm-2 control-label">NIK Ahli Waris</label>
												<div class="col-sm-8">
													<input type="text" name="NIKAhliWaris" class="form-control" id="NIKAhliWaris" placeholder="NIK Ahli Waris" value="{{ old('NIKAhliWaris', isset($item->NIKAhliWaris) ? $item->NIKAhliWaris : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="tanggalMeninggalAhliWaris" class="col-sm-2 control-label">Tanggal Meninggal Ahli Waris</label>
												<div class="col-sm-8">
													<input type="date" name="tanggalMeninggalAhliWaris" class="form-control" id="tanggalMeninggalAhliWaris" placeholder="Tanggal Meninggal Ahli Waris" value="{{ old('tanggalMeninggalAhliWaris', isset($item->tanggalMeninggalAhliWaris) ? $item->tanggalMeninggalAhliWaris : '') }}"/>
												</div>
											</div>

								            <div class="form-group select">
								               <label for="Babinminvetcaddam" class="col-sm-2 control-label">Babinminvetcaddam</label>
								               <div class="col-sm-8">
													<div class="selectDiv">
														<select name="idKodam" class="form-control select2" id="Babinminvetcaddam"
														onchange="loadSelect(this, 'Kanminvetcad', '{{ url('kanminvetcad/get') }}', '{{ isset($item->idKodim) ? $item->idKodim : '' }}');loadSelect(this, 'Provinsi', '{{ url('provinsi/get') }}', '{{ isset($item->idProvinsi) ? $item->idProvinsi : '' }}');">
															<option value="">Pilih Babinminvetcaddam</option>
															@foreach ($lookupTable['kodam'] as $kodam)
															<option value="{{ $kodam->idKodam }}" {{ (isset($item) && $item->idKodam == $kodam->idKodam) ? 'selected=selected' : '' }}>{{ $kodam->namaKodam }}</option>
															@endforeach
														</select>
													</div>
													<input type="text" name="" value="" id="BabinminvetcaddamInput" class="form-control" style="display: none;">
								               </div>
								            </div>

								            <div class="form-group select">
								               <label for="Kanminvetcad" class="col-sm-2 control-label">Kanminvetcad</label>
								               <div class="col-sm-8">
													<div class="selectDiv">
														<select name="idKodim" class="form-control select2" id="Kanminvetcad">
															<option value="">Pilih Kanminvetcad</option>
														</select>
													</div>
													<input type="text" name="" value="" id="KanminvetcadInput" class="form-control" style="display: none;">
								               </div>
								            </div>
								            
								            <div class="form-group select">
								               <label for="Provinsi" class="col-sm-2 control-label">Provinsi</label>
								               <div class="col-sm-8">
													<div class="selectDiv">
														<select name="idProvinsi" class="form-control select2" id="Provinsi" onchange="loadSelect(this, 'Kabupaten', '{{ url('kabupaten/get') }}', '{{ isset($item) ? $item->idKabupaten : '' }}');">
															<option value="">Pilih Provinsi</option>
														</select>
													</div>
													<input type="text" name="" value="" id="ProvinsiInput" class="form-control" style="display: none;">
								               </div>
								            </div>
								            
								            <div class="form-group select">
								               <label for="Kabupaten" class="col-sm-2 control-label">Kabupaten</label>
								               <div class="col-sm-8">
													<div class="selectDiv">
														<select name="idKabupaten" class="form-control select2" id="Kabupaten">
															<option value="">Pilih Kabupaten</option>
														</select>
													</div>
													<input type="text" name="" value="" id="KabupatenInput" class="form-control" style="display: none;">
								               </div>
								            </div>
											<div class="form-group">
												<label for="kecamatan" class="col-sm-2 control-label">Kecamatan</label>
												<div class="col-sm-8">
													<input type="text" name="kecamatan" class="form-control" id="kecamatan" placeholder="Kecamatan" value="{{ old('kecamatan', isset($item->kecamatan) ? $item->kecamatan : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="kelurahan" class="col-sm-2 control-label">Kelurahan</label>
												<div class="col-sm-8">
													<input type="text" name="kelurahan" class="form-control" id="kelurahan" placeholder="Kelurahan" value="{{ old('kelurahan', isset($item->kelurahan) ? $item->kelurahan : '') }}"/>
												</div>
											</div>

											<div class="form-group">
												<label for="alamat" class="col-sm-2 control-label">Alamat</label>
												<div class="col-sm-8">
													<textarea name="alamat" class="form-control" id="alamat" placeholder="Alamat">{{ old('alamat', isset($item->alamat) ? $item->alamat : '') }}</textarea>
												</div>
											</div>
											<div class="form-group">
												<label for="rt" class="col-sm-2 control-label">RT</label>
												<div class="col-sm-2">
													<input type="text" name="rt" class="form-control" id="rt" placeholder="RT" value="{{ old('rt', isset($item->rt) ? $item->rt : '') }}"/>
												</div>
												<label for="rw" class="col-sm-1 control-label">RW</label>
												<div class="col-sm-2">
													<input type="text" name="rw" class="form-control" id="rw" placeholder="RW" value="{{ old('rw', isset($item->rw) ? $item->rw : '') }}"/>
												</div>
											</div>
										</div>
										<div id="halaman3" class="tab-pane fade">
											<div class="form-group">
												<label for="noKepTuvet" class="col-sm-2 control-label">No Kep Tuvet</label>
												<div class="col-sm-4">
													<input type="text" name="noKepTuvet" class="form-control" id="noKepTuvet" placeholder="No Kep Tuvet" value="{{ old('noKepTuvet', isset($item->noKepTuvet) ? $item->noKepTuvet : '') }}"/>
												</div>
												<label for="tglKepTuvet" class="col-sm-2 control-label">Tanggal Kep Tuvet</label>
												<div class="col-sm-2">
													<input type="date" name="tglKepTuvet" class="form-control" id="tglKepTuvet" placeholder="Tanggal Kep Tuvet" value="{{ old('tglKepTuvet', isset($item->tglKepTuvet) ? $item->tglKepTuvet : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="noKepDahor" class="col-sm-2 control-label">No Kep Dahor</label>
												<div class="col-sm-4">
													<input type="text" name="noKepDahor" class="form-control" id="noKepDahor" placeholder="No Kep Dahor" value="{{ old('noKepDahor', isset($item->noKepDahor) ? $item->noKepDahor : '') }}"/>
												</div>
												<label for="tglKepDahor" class="col-sm-2 control-label">Tanggal Kep Dahor</label>
												<div class="col-sm-2">
													<input type="date" name="tglKepDahor" class="form-control" id="tglKepDahor" placeholder="Tanggal Kep Dahor" value="{{ old('tglKepDahor', isset($item->tglKepDahor) ? $item->tglKepDahor : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="noKepTundayatu" class="col-sm-2 control-label">No Kep Tundayatu</label>
												<div class="col-sm-4">
													<input type="text" name="noKepTundayatu" class="form-control" id="noKepTundayatu" placeholder="No Kep Tundayatu" value="{{ old('noKepTundayatu', isset($item->noKepTundayatu) ? $item->noKepTundayatu : '') }}"/>
												</div>
												<label for="tglKepTundayatu" class="col-sm-2 control-label">Tanggal Kep Tundayatu</label>
												<div class="col-sm-2">
													<input type="date" name="tglKepTundayatu" class="form-control" id="tglKepTundayatu" placeholder="Tanggal Kep Tundayatu" value="{{ old('tglKepTundayatu', isset($item->tglKepTundayatu) ? $item->tglKepTundayatu : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="nomorRekening" class="col-sm-2 control-label">Nomor Rekening</label>
												<div class="col-sm-4">
													<input type="text" name="nomorRekening" class="form-control" id="nomorRekening" placeholder="Nomor Rekening" value="{{ old('nomorRekening', isset($item->nomorRekening) ? $item->nomorRekening : '') }}"/>
												</div>
												<label for="tempatPembayaran" class="col-sm-2 control-label">Tempat Pembayaran</label>
												<div class="col-sm-2">
													<input type="text" name="tempatPembayaran" class="form-control" id="tempatPembayaran" placeholder="Tempat Pembayaran" value="{{ old('tempatPembayaran', isset($item->tempatPembayaran) ? $item->tempatPembayaran : '') }}"/>
												</div>
											</div>

											<div class="form-group">
												<label for="noKarip" class="col-sm-2 control-label">No Karip</label>
												<div class="col-sm-8">
													<input type="text" name="noKarip" class="form-control" id="noKarip" placeholder="No Karip" value="{{ old('noKarip', isset($item->noKarip) ? $item->noKarip : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="PTTaspen" class="col-sm-2 control-label">PT Taspen</label>
												<div class="col-sm-8">
													<input type="text" name="PTTaspen" class="form-control" id="PTTaspen" placeholder="PT Taspen" value="{{ old('PTTaspen', isset($item->PTTaspen) ? $item->PTTaspen : '') }}"/>
												</div>
											</div>
											<div class="form-group">
												<label for="keterangan" class="col-sm-2 control-label">Keterangan</label>
												<div class="col-sm-8">
													<textarea name="keterangan" class="form-control" id="keterangan" placeholder="Keterangan">{{ old('keterangan', isset($item->keterangan) ? $item->keterangan : '') }}</textarea>
												</div>
											</div>
										</div>

									</div>

						      	</div>
						    </div>
						</div>
						<div class="panel-footer">
							<div class="col-lg-2">
								<button class="btn btn-md btn-block btn-primary" type="submit">Save</button>
							</div>
							<!-- <div class="col-lg-2">
								<button class="btn btn-md btn-block btn-info" type="button" id="btn-preview" onclick="showModalPreview(this)">Preview</button>
							</div> -->
							<div class="col-lg-2">
								<a href="{{ route('veteran.index') }}" class="btn btn-md btn-block btn-default" id="btn-back">Back to File</a>
							</div>
							<div class="clearfix"></div>
						</div>
					</form>
				</div>
	      	</div>
        </div>
  	</div>
</div>

<div id="modalMedia" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Media</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div id="modalPreview" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Preview</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
            	<div class="col-md-6">
                	<button type="button" class="btn btn-primary">Save</button>
            	</div>
            	<div class="col-md-6">
                	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            	</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/js/upload.single.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('assets/components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
	$(document).ready(function()
	{
		$(".select2").select2();
		{!! (isset($item->idKodam) && $item->idKodam) ? '$("#Babinminvetcaddam").val("'.$item->idKodam.'").change();' : '' !!}
		// {!! (isset($item->idKodam) && $item->idKodim) ? '$("#Kanminvetcad").val("'.$item->idKodim.'").change();' : '' !!}
		// {!! (isset($item->idKodam) && $item->idProvinsi) ? '$("#Provinsi").val("'.$item->idProvinsi.'").change();' : '' !!}
		// {!! (isset($item->idKodam) && $item->idKabupaten) ? '$("#Kabupaten").val("'.$item->idKabupaten.'").change();' : '' !!}
		$(document).on('keyup','input[type="text"]',function(){
	        $(this).attr('value',$(this).val());
	    });
		$(document).on('mouseout','input.ws-date',function(){
			$(this).attr('value',$(this).val());
	    });
	    
		$( "select" ).change(function () {
			var idSelect = $(this).attr('id');
			var valueSelected = $(this).find("option:selected").text();
			$('#'+idSelect+'Input').attr('value',valueSelected);
		}).change();
	});


	$('#form').submit(function()
	{
		return true;
	});

	function changeStatusMeninggal(self)
	{
		var value = $(self).val();
		if (value != "") {
			$('#statusMeninggal').val('ya').change();
		}
		else {
			$('#statusMeninggal').val('tidak').change();
		}
	}

	function changeTanggalMeninggal(self)
	{
		var value = $(self).val();
		if (value == "tidak")
			$('#tanggalMeninggal').val('');
	}

	function showModalPreview(self)
	{
		var e = $(self);
		var body = '';
		body = $('#preview').clone().html().replace(/\halaman/g,'modalHalaman').replace(/\select2/g,'');

		$('#modalMedia .modal-body').html(body);
		$('#modalMedia .select input').show();
		$('#modalMedia input').css({'width': "100%"});
		$('#modalMedia .input-buttons').css({'margin-right': "0"});
		$('#modalMedia input').attr({'disabled': true});
		$('#modalMedia textarea').css({'width': "100%"});
		$('#modalMedia textarea').attr({'disabled': true});

		$('#modalMedia select').removeClass('select2');
		$('#modalMedia select').css({'width': "100%"});
		$('#modalMedia .select .selectDiv').css({'display': "none"});
		$('#modalMedia .select2-container').hide();
		$('#modalMedia select').attr({'disabled': true});
		$('#modalMedia select').hide();

        $('#modalMedia .upload-options').hide();
        $('#modalMedia #browse-media').hide();
        $('#modalMedia .panel-footer').hide();

		/*$('#modalMedia .modal-body .nav-tabs a').each(function() {
			$(this).attr("href", function(index, old) {
				return old.replace("#outer-cont #halaman", "modalHalaman");
			});
		});
        $('#halaman1').attr( 'id' , 'modalHalaman1' );
        $('#halaman2').attr( 'id' , 'modalHalaman2' );
        $('#halaman3').attr( 'id' , 'modalHalaman3' );*/

		$("#modalMedia").modal("show");
		$("#modalMedia btn-preview").hide();
		$(".select2").select2();
	}

	function showPreview(self)
	{
	}

	function modalMedia(self)
	{
		var e = $(self);
        $.ajax({
            url: '{{ route('media.modal') }}',
            type: "get",
            data: {
                _token: '{{ csrf_token() }}',
            },
            success: function (data)
            {
				$("#modalMedia").modal("show");
				$('#modalMedia .modal-body').html(data);
            }
        });
	}
</script>
@endsection