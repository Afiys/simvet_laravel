@extends('layouts.admin')

@section('title', 'Setting')
@section('setting', 'active')

@section('css')
<link href="{{ url('assets/components/select2/dist/css/select2.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
   	<div class="row row-offcanvas row-offcanvas-left">
		
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
			<ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
				<li>
					<a href="{{ route('setting.index') }}"><i class="{{ config('icon.sidebarAdmin.setting') }}"></i>Setting</a>&nbsp;&nbsp;&nbsp;
				</li>
				<li>
					<i class="{{ config('icon.update') }}" aria-hidden="true"></i>Edit
				</li>
			</ol>
        </div>

		<div class="content">
			<div class="panel panel-primary">
				<div class="panel-heading">Setting</div>
				<form method="POST" class="form-horizontal" action="{{ url(route('setting.index')) }}" enctype="multipart/form-data">
					<div class="panel-body">
						@if ($errors->any())
						<div class="alert alert-danger">
						    <ul>
						        @foreach ($errors->all() as $error)
						            <li>{{ $error }}</li>
						        @endforeach
						    </ul>
						</div>
						@endif
					    {{ csrf_field() }}
						<div class="row">
							<div class="col-lg-12">

								@if (isset($item) && count($item) > 0)
								@foreach ($item as $key => $value)
								@php ($name = $value['key'])

								@if ($value['type'] == 'text')
								<div class="form-group">
									<label for="{{ isset($name) ? $name : ''}}" class="col-sm-2 control-label">{{ isset($value['title']) ? $value['title'] : ''}}</label>
									<div class="col-sm-8">
										<input type="text" name="{{ isset($name) ? $name : ''}}[data]" class="form-control" id="{{ isset($name) ? $name : ''}}" placeholder="{{ isset($value['title']) ? $value['title'] : ''}}" value="{{ old($name, isset($value['value']) ? $value['value'] : '') }}" />
										<input type="hidden" name="{{ isset($name) ? $name : ''}}[old]" value="{{ old($name, isset($value['value']) ? $value['value'] : '') }}">
										<input type="hidden" name="{{ isset($name) ? $name : ''}}[type]" value="text">
									</div>
								</div>
                                <div class="clearfix"></div>
								@endif

								@if ($value['type'] == 'image')
								<div class="col-md-4">
	                                <div class="form-group">
	                                    <label for="image" class="col-sm-5 control-label">{{ isset($value['title']) ? $value['title'] : ''}}</label>
	                                    <div class="col-sm-4" style="padding-left: 35px;">
	                                        <div class="box" style="width: 150px; height: 150px;">
	                                            <div class="js--image-preview" style="background-image: url('{{ (isset($value['value']) ? url(''.$value['value']) : '') }}'); width: 150px; height: 130px;"></div>
	                                            <div class="upload-options">
	                                                <label>
	                                                    <input type="file" class="image-upload" accept="image/*" name="{{ isset($name) ? $name : ''}}[data]" id="image" />
														<input type="hidden" name="{{ isset($name) ? $name : ''}}[type]" value="image">
	                                                </label>
	                                            </div>
	                                        </div>
	                                        <!-- <small>Recommended dimension: 640 x 480 px</small> -->
	                                    </div>
									</div>
                                </div>
                                <div class="clearfix"></div>
								@endif

								@if ($value['type'] == 'textarea')
								<div class="form-group">
									<label for="{{ isset($name) ? $name : ''}}" class="col-sm-2 control-label">{{ isset($value['title']) ? $value['title'] : ''}}</label>
									<div class="col-sm-8">
										<textarea name="{{ isset($name) ? $name : ''}}[data]" class="form-control" id="{{ isset($name) ? $name : ''}}" placeholder="{{ isset($value['title']) ? $value['title'] : ''}}">{{ old($name, isset($value['longText']) ? removebr($value['longText']) : '') }}</textarea>

										<!-- <input type="text" name="{{ isset($name) ? $name : ''}}[data]" class="form-control" id="{{ isset($name) ? $name : ''}}" placeholder="{{ isset($value['title']) ? $value['title'] : ''}}" value="{{ old($name, isset($value['value']) ? $value['value'] : '') }}" /> -->
										<input type="hidden" name="{{ isset($name) ? $name : ''}}[old]" value="{{ old($name, isset($value['value']) ? $value['value'] : '') }}">
										<input type="hidden" name="{{ isset($name) ? $name : ''}}[type]" value="textarea">
									</div>
								</div>
                                <div class="clearfix"></div>
								@endif

								@endforeach
								@endif

					      	</div>
					    </div>
					</div>
					<div class="panel-footer">
						<div class="col-lg-2">
							<button class="btn btn-md btn-block btn-primary" type="submit">Save</button>
						</div>
						<div class="col-lg-2">
							<a href="{{ route('setting.index') }}" class="btn btn-md btn-block btn-default">Cancel</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
      	</div>
  	</div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/js/upload.single.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ url('assets/components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.select2').select2();
	});
</script>
@endsection