@extends('layouts.admin')

@section('title', 'Maps')
@section('map', 'active')

@section('css')
@endsection

@section('content')
<div class="container-fluid" id="container-content">
   	<div class="row row-offcanvas row-offcanvas-left">
		
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
			<ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
				<li>
					<a href="{{ route('provinsi.index') }}"><i class="{{ config('icon.sidebarAdmin.map') }}"></i>Map</a>&nbsp;&nbsp;&nbsp;
				</li>
				<li>
					<i class="{{ config('icon.update') }}" aria-hidden="true"></i>Edit
				</li>
			</ol>
        </div>

		<div class="content">
			<textarea rows=3 name="coords1" class="canvas-area"
	        placeholder="Shape Coordinates"
	        data-image-url="{{ url('assets/img/peta/Indonesia_kodam.jpg') }}"></textarea>
      	</div>
  	</div>
</div>
@endsection

@section('js')
<!-- <script language="javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<script language="javascript" src="{{ url('assets/components/CanvasImageMapAreaEditor/jquery.canvasAreaDraw.js') }}"></script>
@endsection