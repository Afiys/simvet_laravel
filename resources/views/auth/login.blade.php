<!DOCTYPE html>
<html lang="en">
<head>
   <title>Login</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" type="image/png" sizes="16x16" href="{{ url('assets/img/icon-large.png') }}">
   <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ url('assets/components/font-awesome/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ url('assets/css/login.css') }}">
</head>
<body>
   <div id="container">
      <div id="overlay">
         <div class="logo"><a href="#"><img src="{{ url(''.GetData::setting()->imgLogo['value']) }}" width="100px"></a></div><br>
         <div class="content">
            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
               {{ csrf_field() }}
               <!-- <div class="logo"><a href="{{ url('') }}"><img src="{{ url('assets/img/logo-invers.png') }}" width="70%"></a></div> -->
               <h3>Log in</h3>

               @if ($errors->has('username'))
               <div class="alert alert-danger">{{ $errors->first('username') }}</div>
               @endif
               @if ($errors->has('password'))
               <div class="alert alert-danger">{{ $errors->first('password') }}</div>
               @endif

               <div class="form-group">
                  <div class="input-icon">
                     <input type="username" class="input" placeholder="Username" name="username" value="{{ old('username') }}" required />
                  </div>
               </div>
               <div class="form-group">
                  <div class="input-icon">
                     <input type="password" class="input" placeholder="Password" name="password" id="password" required/>
                  </div>
               </div>

               <div class="form-group">
                  <div class="input-icon">
                     <div class="checkbox">
                         <label for="remember"><input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} style="width:auto;"> Remember Me</label>
                     </div>
                  </div>
               </div>

               <div class="form-actions">
                  <button type="submit" class="btn btn-red">Log In</button>
               </div>
            </form>
         </div>
         <div class="footer">{!! Config::get('app.copyright') !!}</div>
      </div>
   </div>
   <script src="{{ url('assets/js/jquery.min.js') }}"></script>
   <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
</body>
</html>