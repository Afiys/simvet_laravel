@extends('layouts.app')

@section('content')
<div id="container">
    <div class="content">
        <div class="title">
            <h3>Sign Up / Register</h3>
        </div>
        <form class="form-horizontal" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="container-form">
                <div class="form-group">
                    <label class="control-label col-sm-3" for="name">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Your Name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                        <div class="has-error">
                           <span class="help-block">
                              <strong>{{ $errors->first('name') }}</strong>
                           </span>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="company">Company</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="company" id="company" placeholder="Your Company" value="{{ old('company') }}">
                        @if ($errors->has('company'))
                        <div class="has-error">
                           <span class="help-block">
                              <strong>{{ $errors->first('company') }}</strong>
                           </span>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <div class="col-sm-4 nopadding">
                                <label><input type="checkbox" name="isShipper" value="1" {{ old('isShipper') ? 'checked' : '' }}>Shipper</label>
                            </div>
                            <div class="col-sm-8 nopadding">
                                <label><input type="checkbox" name="isCarrier" value="1" {{ old('isCarrier') ? 'checked' : '' }}>Carrier</label>
                            </div>
                            @if ($errors->has('customerType'))
                            <div class="has-error">
                               <span class="help-block">
                                  <strong>{{ $errors->first('customerType') }}</strong>
                               </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3">Type of Company</label>
                    <div class="col-sm-9">
                        <div class="radio">
                            <div class="col-sm-4 nopadding">
                                <label><input type="radio" name="typeOfCompany" value="PERORANGAN" onclick="otherChecked()" {{ old('typeOfCompany') == 'PERORANGAN' ? 'checked' : '' }}>Perorangan</label>
                                <br>
                                <label style="padding-top: 5px;"><input type="radio" name="typeOfCompany" value="CV" onclick="otherChecked()" {{ old('typeOfCompany') == 'CV' ? 'checked' : '' }}>CV</label>
                            </div>
                            <div class="col-sm-8 nopadding">
                                <label><input type="radio" name="typeOfCompany" value="PT" onclick="otherChecked()" {{ old('typeOfCompany') == 'PT' ? 'checked' : '' }}>PT</label>
                                <br>
                                <label style="padding-top: 5px;"><input type="radio" name="typeOfCompany" value="OTHERS" id="others" onclick="otherChecked()" {{ old('typeOfCompany') == 'OTHERS' ? 'checked' : '' }}>Others</label>
                                <input type="text" name="other" id="inputOther" class="form-control rightinput" disabled="">
                            </div>

                            @if ($errors->has('typeOfCompany'))
                            <div class="has-error">
                               <span class="help-block">
                                  <strong>{{ $errors->first('typeOfCompany') }}</strong>
                               </span>
                            </div>
                            @endif
                            @if ($errors->has('other'))
                            <div class="has-error">
                               <span class="help-block">
                                  <strong>{{ $errors->first('other') }}</strong>
                               </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">Email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                        <div class="has-error">
                           <span class="help-block">
                              <strong>{{ $errors->first('email') }}</strong>
                           </span>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email_confirmation">Confirm Email</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="email_confirmation" id="email_confirmation" placeholder="Repeat Your Email" value="{{ old('email_confirmation') }}">
                    </div>
                </div>
                <br><br>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="username">Username</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="username" id="username" placeholder="Your Username" value="{{ old('username') }}">
                        @if ($errors->has('username'))
                        <div class="has-error">
                           <span class="help-block">
                              <strong>{{ $errors->first('username') }}</strong>
                           </span>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="password">Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Your Password">
                        @if ($errors->has('password'))
                        <div class="has-error">
                           <span class="help-block">
                              <strong>{{ $errors->first('password') }}</strong>
                           </span>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="password_confirmation">Confirm Password</label>
                    <div class="col-sm-9">
                        <input type="password" class="form-control" name="password_confirmation" id="password_confirmation" placeholder="Repeat Your Password">
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label><input type="checkbox" name="accept_terms" valud="1">I accept the terms</label>
                            @if ($errors->has('accept_terms'))
                            <div class="has-error">
                               <span class="help-block">
                                  <strong>{{ $errors->first('accept_terms') }}</strong>
                               </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <div class="g-recaptcha" data-sitekey="{{ env('RE_CAP_SITE') }}" style="transform:scale(0.77);-webkit-transform:scale(0.77);transform-origin:0 0;-webkit-transform-origin:0 0;"></div>
                            @if ($errors->has('g-recaptcha-response'))
                            <div class="has-error">
                               <span class="help-block">
                                  <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                               </span>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="form-group" style="margin-bottom:0px; padding-bottom:20px;">
                    <label class="control-label col-sm-3"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <button type="submit" class="btn btn-blue">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop

@section('js')
<script src='https://www.google.com/recaptcha/api.js'></script>
<script>
    otherChecked();
    function otherChecked()
    {
        if ($('input#others').is(':checked'))
        {
            $('#inputOther').prop('disabled', false);
        }
        else {
            $('#inputOther').prop('disabled', true);
        }
    }
</script>
@endsection
