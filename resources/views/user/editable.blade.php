@extends('layouts.admin')

@section('title', 'User')
@section('user', 'active')

@section('css')
<link href="{{ url('assets/components/select2/dist/css/select2.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
   	<div class="row row-offcanvas row-offcanvas-left">
		
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
			<ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Home</a>&nbsp;&nbsp;&nbsp;
                </li>
				<li>
					<a href="{{ route('user.index') }}"><i class="{{ config('icon.sidebarAdmin.user') }}"></i>User</a>&nbsp;&nbsp;&nbsp;
				</li>
				<li>
					<i class="{{ config('icon.'.($id == 0 ? 'add' : 'update')) }}" aria-hidden="true"></i>{{ ($id == 0 ? 'Tambah' : 'Edit') }}
				</li>
			</ol>
        </div>

		<div class="content">
			<div class="panel panel-primary">
				<div class="panel-heading">{{ ($id == 0 ? 'Tambah' : 'Edit') }} User</div>
				<form method="POST" class="form-horizontal" action="{{ url(route('user.index') . ($id == 0 ? '' : '/' . $id)) }}" enctype="multipart/form-data">
					<div class="panel-body">
						@if ($errors->any())
						<div class="alert alert-danger">
						    <ul>
						        @foreach ($errors->all() as $error)
						            <li>{{ $error }}</li>
						        @endforeach
						    </ul>
						</div>
						@endif
					    {{ csrf_field() }}
					    @if($id != 0)
					    <input name="_method" type="hidden" value="PUT">
						@endif
						<div class="row">
							<div class="col-lg-12">

					            <div class="form-group">
					               <label for="name" class="col-sm-2 control-label">Nama</label>
					               <div class="col-sm-8">
					                  <input type="text" name="name" class="form-control" id="name" placeholder="Nama" required="" value="{{ old('name', isset($item) ? $item->name : '') }}" />
					               </div>
					            </div>

					            <div class="form-group">
					               <label for="username" class="col-sm-2 control-label">Username</label>
					               <div class="col-sm-8">
					                  <input type="text" name="username" class="form-control" id="username" placeholder="Username" required="" value="{{ old('username', isset($item) ? $item->username : '') }}" />
					               </div>
					            </div>

					            <div class="form-group">
					               <label for="email" class="col-sm-2 control-label">Email</label>
					               <div class="col-sm-8">
					                  <input type="email" name="email" class="form-control" id="email" placeholder="Email (Opsional)" value="{{ old('email', isset($item) ? $item->email : '') }}" />
					               </div>
					            </div>

					            <div class="form-group">
					            	<label for="password" class="col-sm-2 control-label">Password</label>
					            	<div class="col-sm-8">
						            	<div class="col-sm-11 nopadding">
						            		<input type="password" name="password" id="password" class="form-control pwd" placeholder="Password" value="">
						            	</div>
						            	<span class="input-group-btn">
					            			<button class="btn btn-default reveal" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
					            		</span>
					            	</div>
					            </div>

					            <div class="form-group">
					               <label for="role" class="col-sm-2 control-label">Role</label>
					               <div class="col-sm-8">
										<select name="roleCode" class="form-control select2" id="role" required="">
											<option value="">Pilih Role</option>
											@foreach (Config::get('role') as $role)
											<option value="{{ $role['code'] }}" {{ (isset($item) && $item->roleCode == $role['code']) ? 'selected=selected' : '' }}>{{ $role['name'] }}</option>
											@endforeach
										</select>
					               </div>
					            </div>

					      	</div>
					    </div>
					</div>
					<div class="panel-footer">
						<div class="col-lg-2">
							<button class="btn btn-md btn-block btn-primary" type="submit">Save</button>
						</div>
						<div class="col-lg-2">
							<a href="{{ route('user.index') }}" class="btn btn-md btn-block btn-default">Cancel</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
      	</div>
  	</div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/select2/dist/js/select2.full.min.js') }}"></script>
<script>
	$(document).ready(function()
	{
		$('.select2').select2();
		$(".reveal").mousedown(function() {
			$(".pwd").replaceWith($('.pwd').clone().attr('type', 'text'));
		})
		.mouseup(function() {
			$(".pwd").replaceWith($('.pwd').clone().attr('type', 'password'));
		})
		.mouseout(function() {
			$(".pwd").replaceWith($('.pwd').clone().attr('type', 'password'));
		});
	});
</script>
@endsection