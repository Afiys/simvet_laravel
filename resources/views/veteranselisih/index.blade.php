@extends('layouts.admin')

@section('title', 'Data Veteran Selisih')
@section('veteranselisih', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
<style>
    form .btn { width: 100%; }
</style>
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
                <li>
                    <i class="{{ config('icon.sidebarAdmin.veteran') }}"></i>Data Veteran Selisih
                </li>
            </ol>
        </div>

        <div class="content">
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <div class="container-fluid table-responsive" align="center-block">
                <table class="table table-striped table-hover" id="dataTables">
                    <thead>
                        <!-- <tr>
                            <th>Nama</th>
                            <th>NRP/NIP</th>
                            <th>NPV</th>
                            <th>No Pendaftaran</th>
                            <th>Predikat</th>
                            <th>Provinsi</th>
                            <th>No Karip</th>
                            <th>RH</th>
                            <th>Action</th>
                        </tr> -->
                        <tr>
                            <th>Nama</th>
                            <th>NRP/NIP</th>
                            <th>NPV</th>

                            <th>Tanggal Lahir</th>
                            <th>Jenis Kelamin</th>

                            <th>Predikat</th>
                            <th>Provinsi</th>
                            <th>Kabupaten</th>
                            <th>Babinminvetcaddam</th>
                            <th>Kanminvetcad</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>

        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
var selectedId = [];
var jsonString = "";
$(document).ready(function() {
    listManager.listUrl = "{{ route('veteranselisih.index') }}/list";
    // listManager.deleteUrl = "{{ route('veteran.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                { "data": "NIP", "name": "veteran.NIP" },
                                { "data": "npvBaru", "name": "veteran.npvBaru" },
                                { "data": "tanggalLahir", "name": "veteran.tanggalLahir" },
                                { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                { "data": "predikat", "name": "veteran.predikat" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": "namaKabupaten", "name": "kabupaten.namaKabupaten" },
                                { "data": "namaKodam", "name": "kodam.namaKodam" },
                                { "data": "namaKodim", "name": "kodim.namaKodim" },
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"}
                                /*
                                { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                { "data": "NIP", "name": "veteran.NIP" },
                                { "data": "npvBaru", "name": "veteran.npvBaru" },
                                { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                { "data": "predikat", "name": "veteran.predikat" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": "noKarip", "name": "veteran.noKarip" },
                                { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"}
                                */
    ];
    listManager.ready();

    $('#search-form').on('submit', function(e)
    {
        var kodam = $('#kodam').val();
        var kodim = $('#kodim').val();
        var customSearch = "kodam="+kodam+"&kodim="+kodim;
        listManager.destroy();
        listManager.customSearch = customSearch;
        listManager.listUrl = "{{ route('veteranselisih.index') }}/list";
        listManager.deleteUrl = "{{ route('veteranselisih.index') }}";
        listManager.token = "{{ csrf_token() }}";
        listManager.dataTable = [
                                    { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                    { "data": "NIP", "name": "veteran.NIP" },
                                    { "data": "npvBaru", "name": "veteran.npvBaru" },
                                    { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                    { "data": "predikat", "name": "veteran.predikat" },
                                    { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                    { "data": "noKarip", "name": "veteran.noKarip" },
                                    // { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                    { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                    { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"}
        ];
        listManager.ready();
        e.preventDefault();
    });


    /* CHECK DELETE */

    $('#dataTables').on( 'page.dt', function ()
    {
        setTimeout(function()
        {
            $('#checkBoxAll').prop("checked",true);
            $(".checkRow").each( function()
            {
                if(!$(this).is(':checked'))
                {
                    $('#checkBoxAll').prop("checked",false);
                }
            });
        }, 500);
    });

    // CHECK ALL OLD --
    $("#checkAll").on('click',function()
    {
        var status = this.checked;

        if (status)
        {
            id = $("#checkAll").val();
            var obj = $.parseJSON(id);
            $.each(obj, function(index, id)
            {
                selectedId.push(id.toString() );
            });
        }
        else {
            selectedId = [];
        }

        $(".checkRow").each( function()
        {
            $(this).prop("checked",status);
        });
    });
    // CHECK ALL OLD --
});

function resetData()
{
    listManager.destroy();
    listManager.customSearch = "";
    listManager.listUrl = "{{ route('veteranselisih.index') }}/list";
    listManager.deleteUrl = "{{ route('veteran.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                { "data": "namaVeteran", "name": "veteran.namaVeteran" },
                                { "data": "npvBaru", "name": "veteran.npvBaru" },
                                // { "data": "noPendaftaran", "name": "veteran.noPendaftaran" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": "noKarip", "name": "veteran.noKarip" },
                                { "data": "jenisKelamin", "name": "veteran.jenisKelamin" },
                                { "data": "predikat", "name": "veteran.predikat" },
                                { "data": 'rh', orderable: false, searchable: false, "sClass": "actionList"},
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"},
    ];
    listManager.ready();
}

function checkAll(id)
{
   /*var obj = $.parseJSON(id);
   $.each(obj, function(index, id)
   {
      selectedId.push(id.toString() );
   });*/
   $('#checkBoxAll').prop("checked",true);
   $(".checkRow").each( function()
   {
      // $(this).prop("checked",true);
      $(this).prop("checked",true).trigger("change");
   });
}

function removeCheckAll()
{
   // selectedId = [];
   $('#checkBoxAll').prop("checked",false);
   $(".checkRow").each( function()
   {
      $(this).prop("checked",false).trigger("change");
      // $(this).prop("checked",false);
   });
}

function selectRow(self, id)
{
   if ($(self).is(':checked'))
   {
      // SET CHECK
      selectedId.push(id);
      $('#checkBoxAll').prop("checked",true);
      $(".checkRow").each( function()
      {
         if(!$(this).is(':checked'))
         {
            $('#checkBoxAll').prop("checked",false);
         }
      });
   }
   else {
      // REMOVE ID
      $('#checkBoxAll').prop("checked",false);
      selectedId = jQuery.grep(selectedId, function(value) {
         return value != id;
      });
   }
}

// ACTION DELETE
function deleteSelected()
{
   var yes = confirm('Are you sure want to delete selected data ?');
   if (yes)
   {
      $.ajax({
         type: "POST",
         url: "{{ url('veteran/delete/selected') }}",
         dataType: "JSON",
         data: {
            selectedId: selectedId,
            _token: '{{ csrf_token() }}'
         },
         success:function(resp)
         {
            try {
               if(resp.statusCode == "200")
               {
                  $('#checkBoxAll').prop("checked",false);
                  listManager.destroy();
                  listManager.ready();
                  // location.reload();
               }
               else {
                  // location.reload();
               }
            } catch(e) {
               // location.reload();
            }
         },
         error:function()
         {
            // location.reload();
         }
      });
   }
}

</script>
@endsection