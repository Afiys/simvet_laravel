@extends('layouts.admin')

@section('title', 'Provinsi')
@section('provinsi', 'active')

@section('css')
<link href="{{ url('assets/components/select2/dist/css/select2.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
   	<div class="row row-offcanvas row-offcanvas-left">
		
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
			<ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
				<li>
					<a href="{{ route('provinsi.index') }}"><i class="{{ config('icon.sidebarAdmin.provinsi') }}"></i>Provinsi</a>&nbsp;&nbsp;&nbsp;
				</li>
				<li>
					<i class="{{ config('icon.'.($id == 0 ? 'add' : 'update')) }}" aria-hidden="true"></i>{{ ($id == 0 ? 'Tambah' : 'Edit') }}
				</li>
			</ol>
        </div>

		<div class="content">
			<div class="panel panel-primary">
				<div class="panel-heading">{{ ($id == 0 ? 'Tambah' : 'Edit') }} Provinsi</div>
				<form method="POST" class="form-horizontal" action="{{ url(route('provinsi.index') . ($id == 0 ? '' : '/' . $id)) }}" enctype="multipart/form-data">
					<div class="panel-body">
						@if ($errors->any())
						<div class="alert alert-danger">
						    <ul>
						        @foreach ($errors->all() as $error)
						            <li>{{ $error }}</li>
						        @endforeach
						    </ul>
						</div>
						@endif
					    {{ csrf_field() }}
					    @if($id != 0)
					    <input name="_method" type="hidden" value="PUT">
						@endif
						<div class="row">
							<div class="col-lg-12">

					            <div class="form-group">
					               <label for="namaProvinsi" class="col-sm-2 control-label">Nama Provinsi</label>
					               <div class="col-sm-8">
					                  <input type="text" name="namaProvinsi" class="form-control" id="namaProvinsi" placeholder="Nama Provinsi" required="" value="{{ old('namaProvinsi', isset($item) ? $item->namaProvinsi : '') }}" />
					               </div>
					            </div>

					            <div class="form-group">
					               <label for="Babinminvetcaddam" class="col-sm-2 control-label">Babinminvetcaddam</label>
					               <div class="col-sm-8">
										<select name="idKodam" class="form-control select2" id="idKodam" required="">
											<option value="">Pilih Babinminvetcaddam</option>
											@foreach ($lookupTable['kodam'] as $kodam)
											<option value="{{ $kodam->idKodam }}" {{ (isset($item) && $item->idKodam == $kodam->idKodam) ? 'selected=selected' : '' }}>{{ $kodam->namaKodam }}</option>
											@endforeach
										</select>
					               </div>
					            </div>

					      	</div>
					    </div>
					</div>
					<div class="panel-footer">
						<div class="col-lg-2">
							<button class="btn btn-md btn-block btn-primary" type="submit">Save</button>
						</div>
						<div class="col-lg-2">
							<a href="{{ route('provinsi.index') }}" class="btn btn-md btn-block btn-default">Cancel</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</form>
			</div>
      	</div>
  	</div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/select2/dist/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function()
	{
		$('.select2').select2();
	});
</script>
@endsection