@extends('layouts.admin')

@section('title', 'Media')
@section('media', 'active')

@section('css')
<link href="{{ url('assets/css/media.css') }}" rel="stylesheet">
<style>
div#upload-container {
    position: relative;
    overflow: hidden;
    cursor: pointer;
}
input#btn-upload {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    right: 0;
    top: 0;
    cursor: pointer;
}
</style>
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
                <li>
                    <i class="{{ config('icon.sidebarAdmin.media') }}"></i>Media
                </li>
            </ol>
        </div>

        <div class="content">
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif

            <div class="clearfix"></div>
            @if (in_array('ALL', Config::get('menu.role.media.create')))
            <div style="float: right;">
                <form action="{{ route('media.uploadFileMedia') }}" method="post" enctype="multipart/form-data" id="formUpload">
                    <a href="#" class="btn btn-add" data-target="#modalCreateFolder" data-toggle="modal">+ Buat Folder</a>
                    <!-- <a href="{{ route('media.create') }}" class="btn btn-add"><i class="fa fa-upload"></i> Upload Image</a> -->
                    <input type="hidden" name="pathFolder" id="pathFolder" value="" placeholder="Nama Folder" class="form-control pathFolder">
                    {{ csrf_field() }}
                    <div id="upload-container" class="file btn btn-add">
                        Upload
                        <input id="btn-upload" class="upload" type="file" name="file[]" multiple/>
                    </div>
                </form>
            </div>
            @else
                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.media.create')))
                <div style="float: right;">
                    <form action="{{ route('media.uploadFileMedia') }}" method="post" enctype="multipart/form-data" id="formUpload">
                        <a href="#" class="btn btn-add" data-target="#modalCreateFolder" data-toggle="modal">+ Buat Folder</a>
                        <!-- <a href="{{ route('media.create') }}" class="btn btn-add"><i class="fa fa-upload"></i> Upload Image</a> -->
                        <input type="hidden" name="pathFolder" id="pathFolder" value="" placeholder="Nama Folder" class="form-control pathFolder">
                        {{ csrf_field() }}
                        <div id="upload-container" class="file btn btn-add">
                            Upload
                            <input id="btn-upload" class="upload" type="file" name="file[]" multiple/>
                        </div>
                    </form>
                </div>
                @endif
            @endif
            <div class="clearfix"></div>

            <div class="filemanager">

                <div class="search">
                    <input type="search" placeholder="Find a file.." />
                </div>
                <div class="breadcrumbs"></div>
                <ul class="data"></ul>
                <div class="nothingfound">
                    <div class="nofiles"></div>
                    <span>No files here.</span>
                </div>
            </div>

        </div>
    </div>
</div>
<div id="modalCreateFolder" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Buat Folder</h4>
            </div>
            <form method="POST" action="{{ route('media.createFolder') }}">
                <div class="modal-body">
                    <input type="text" name="folderName" value="" placeholder="Nama Folder" class="form-control">
                    <input type="hidden" name="pathFolder" id="pathFolder" value="" placeholder="Nama Folder" class="form-control pathFolder">
                </div>
                <div class="modal-footer">
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="modalShowMedia" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">File</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">
    var _token = '{{ csrf_token() }}';
    var pathFolder = '';
    $('.file').change(function() {
        $('#formUpload').submit();
    });
</script>
<script src="{{ url('assets/js/media.js') }}"></script>
@endsection