<link href="{{ url('assets/css/media.css') }}" rel="stylesheet">
<style>
div#upload-container {
    position: relative;
    overflow: hidden;
    cursor: pointer;
}
input#btn-upload {
    position: absolute;
    font-size: 50px;
    opacity: 0;
    right: 0;
    top: 0;
    cursor: pointer;
}
</style>
        <div class="content">
            <div class="clearfix"></div>

            <div class="filemanager">

                <div class="search">
                    <input type="search" placeholder="Find a file.." />
                </div>
                <div class="breadcrumbs"></div>
                <ul class="data"></ul>
                <div class="nothingfound">
                    <div class="nofiles"></div>
                    <span>No files here.</span>
                </div>
            </div>

        </div>

<script type="text/javascript">
    var _token = '{{ csrf_token() }}';
    mediaSelect = true;
    var pathFolder = '';
    $('.file').change(function() {
        $('#formUpload').submit();
    });
</script>
<script src="{{ url('assets/js/media.js') }}"></script>