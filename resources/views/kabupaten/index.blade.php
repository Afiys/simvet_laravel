@extends('layouts.admin')

@section('title', 'Kabupaten')
@section('kabupaten', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
                <li>
                    <i class="{{ config('icon.sidebarAdmin.kabupaten') }}"></i>Kabupaten
                </li>
            </ol>
        </div>

        <div class="content">
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            @if (in_array('ALL', Config::get('menu.role.kabupaten.create')))
            <div class="btn-container">
                <a href="{{ route('kabupaten.create') }}" class="btn btn-add">Add New</a>
            </div>
            @else
                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.kabupaten.create')))
                <div class="btn-container">
                    <a href="{{ route('kabupaten.create') }}" class="btn btn-add">Add New</a>
                </div>
                @endif
            @endif
            <div class="container-fluid table-responsive" align="center-block">

                <table class="table table-striped table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Kabupaten</th>
                            <th>Provinsi</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    listManager.listUrl = "{{ route('kabupaten.index') }}/list";
    listManager.deleteUrl = "{{ route('kabupaten.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                { "data": "idKabupaten", "name": "kabupaten.idKabupaten" },
                                { "data": "namaKabupaten", "name": "kabupaten.namaKabupaten" },
                                { "data": "namaProvinsi", "name": "provinsi.namaProvinsi" },
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"},
    ];
    listManager.ready();
});
</script>
@endsection