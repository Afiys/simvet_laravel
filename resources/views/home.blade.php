@extends('layouts.admin')

@section('title', 'Dashboard')
@section('dashboard', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <!-- <img src="{{ url('assets/img/header.jpg') }}" width="100%"> -->
        <div class="content" style="padding-left: 0px; padding-right: 0px;">
            <br>
            <div class="row">

                <form class="form-horizontal" enctype="multipart/form-data">
                    <div class="col-lg-4">
                        <div class="form-group">
                           <label class="col-sm-6 control-label" for="wilayah">Pilih Tipe Wilayah</label>
                           <div class="col-sm-6">
                                <select name="wilayah" class="form-control select2" id="wilayah" onchange="changeMaps(this);" autocomplete="off">
                                    <option value="kodam">Wilayah Militer</option>
                                    <option value="provinsi">Wilayah Administratif</option>
                                </select>
                           </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                           <label class="col-sm-6 control-label" for="tipeData">Pilih Tipe Data</label>
                           <div class="col-sm-6">
                                <select name="tipeData" class="form-control select2" id="tipeData" autocomplete="off">
                                    <option value="tabel">Tabel</option>
                                    <option value="grafik">Grafik</option>
                                </select>
                           </div>
                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="form-group">
                            <div class="col-sm-4">
                                <button type="button" class="btn btn-info" onclick="openRekap('all')">Rekap Data Veteran RI (Semua Wilayah)</button>
                                <!-- <button type="button" class="btn btn-info showModal" data-urlkodam="{{ route('rekapPerkodam') }}" data-urlprovinsi="{{ route('rekapPerProvinsi') }}" data-title="Rekap Data Veteran RI (Semua Wilayah)" data-target="#myModal" data-toggle="modal" data-parameter="idKodam=all&idProvinsi=all">Rekap Data Veteran RI (Semua Wilayah)</button> -->
                            </div>
                        </div>
                    </div>
                </form>

                <div class="col-lg-2">
                    <div class="form-group">
                        <div class="col-sm-11">
                            <form action="{{ route('search') }}" method="POST">
                                {{ csrf_field() }}
                                <input type="search" class="search form-control" placeholder="search" name="search" required="" onkeydown="if (event.keyCode == 13) { this.form.submit(); return false; }">
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div id="wilayahMiliter">
                <img src="{{ url('assets/img/peta/kodam_baru.jpg') }}" id="imgMapMiliter" usemap="#MapMiliter" width="100%">
                <map name="MapMiliter" style="width: 100%">
                    @foreach ($list['kodam'] as $key => $data)
                    <area shape="poly" coords="{!! $data['coords'] !!}" href="javascript:;" onclick="openRekap('{!! $data['idKodam'] !!}')">
                    <!-- <area shape="poly" coords="{!! $data['coords'] !!}" href="#" title="{!! $data['namaKodam'] !!}" class="showModal" data-urlkodam="{{ route('rekapPerkodam') }}" data-urlprovinsi="" data-title="Rekap Data Veteran RI ({!! $data['namaKodam'] !!})" data-target="#myModal" data-toggle="modal" data-parameter="idKodam={!! $data['idKodam'] !!}"> -->
                    @endforeach
                </map>
            </div>

            <div id="wilayahAdministratif">
                <img src="{{ url('assets/img/peta/provinsi_baru.jpg') }}" id="imgMapAdministratif" usemap="#MapAdministratif" width="100%">
                <map name="MapAdministratif" style="width: 100%">
                    @foreach ($list['provinsi'] as $key => $data)
                    <area shape="poly" coords="{!! $data['coords'] !!}" href="javascript:;" onclick="openRekap('{!! $data['idProvinsi'] !!}')">
                    <!-- <area shape="poly" coords="{!! $data['coords'] !!}" href="#" title="{!! $data['namaProvinsi'] !!}" class="showModal" data-urlprovinsi="{{ route('rekapPerProvinsi') }}" data-urlkodam="" data-title="{!! $data['namaProvinsi'] !!}" data-target="#myModal" data-toggle="modal" data-parameter="idProvinsi={!! $data['idProvinsi'] !!}"> -->
                    @endforeach
                </map>
            </div>
        </div>

        <!-- <div class="content">
            <div class="welcome">
                <br><br>
                <h1><center>Selamat Datang di Sistem Informasi Veteran</center></h1>
                <br><br>
            </div>
            <div class="datas">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="title-section">TOTAL VETERAN</div>
                        <div class="chart blue"><i class="fa fa-bar-chart"></i></div>
                        <div class="total-summary blue">59</div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-5">
                        <br><br><br>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-3">
                        <div class="title-section">TOTAL USER</div>
                        <div class="chart red"><i class="fa fa-bar-chart"></i></div>
                        <div class="total-summary red">11</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="row">
                    <div  class="col-sm-12">
                        <div class="title-section">DATA</div>
                        <div class="col-md-6">
                            <canvas id="chart3" width="100%" height="40px"></canvas>
                        </div>
                        <div class="col-md-6">
                            <canvas id="chart4" width="100%" height="40px"></canvas>
                        </div>
                        <br>
                        <div class="col-md-12">
                            <canvas id="chart5" width="100%" height="30px"></canvas>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-11">
                        <div class="title-section">LAST 10 DATA</div>
                        <div class="container-fluid table-responsive" align="center-block">
                            <table class="table table-striped table-hover" id="dataTables" width="100%">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Transaction Code</th>
                                        <th>Transaction Date</th>
                                        <th>User</th>
                                        <th>Vendor</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div> -->

    </div>
</div>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@stop

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ url('assets/components/Chart.js/dist/chart.js') }}"></script>
<script src="{{ url('assets/components/Chart.js/dist/Chart.bundle.min.js') }}"></script>
<script src="{{ url('assets/components/ImageMapster/dist/jquery.imagemapster.js') }}"></script>
<script type="Text/javascript">
$(document).ready(function()
{
    var imgMapAdministratif = true;
    $('#imgMapMiliter').mapster({
        opacity:1,
        stroke: false,
        clickNavigate: true,
        render_highlight: {
            fillColor: '2aff00',
            stroke: false,
            altImage: '{{ url('assets/img/peta/kodam_baru-hover.jpg') }}'
        },
        render_select: {
            fillColor: 'ff000c',
            stroke: false,
            altImage: '{{ url('assets/img/peta/kodam_baru-hover.jpg') }}'
        },
        fadeInterval: 50
    });
    $('#wilayahAdministratif').hide();

    $('.showModal').click(function()
    {
        var loader = '<div id="modal-loader" style="text-align: center;"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><span class="sr-only">Loading...</span></div>';
        // var _token = $('input[name=_token]').val();
        var _token = "{{ csrf_token() }}";
        var urlkodam = $(this).data('urlkodam');
        var urlprovinsi = $(this).data('urlprovinsi');
        var url = "";
        var wilayah = $('#wilayah').val();
        if (wilayah == 'provinsi')
            url = urlprovinsi;
        else
            url = urlkodam;

        var title = $(this).data('title');
        var parameter = $(this).data('parameter');
        // console.log(parameter)
        var typeData = $('#tipeData').val();
        $('.modal-body').html(loader);
        $.ajax({
            url: url,
            type: 'post',
            data: {
                '_token': _token,
                'typeData': typeData,
                parameter
            },
        })
        .done(function(resp)
        {
            $('.modal-body').html(resp);
            $('.modal-title').html(title);
        })
        .fail(function(){
            $('.modal-body').html('');
            $('.modal-title').html(title);
            // alert('Request Failed...');
        });
    });

});

function openRekap(id)
{
    var url = '';
    var wilayah = $('#wilayah').val();
    var tipeData = $('#tipeData').val();
    if (wilayah == 'provinsi')
    {
        url = '/rekap-provinsi/';
    }
    else
    {
        url = '/rekap-babinminvetcaddam/';
    }
    url = url + tipeData + "/" + id;
    url = '{{ url('') }}'+url;
    window.open(url);
}

function changeMaps(self)
{
    var val = $(self).val();
    if (val == 'provinsi')
    {
        $('#wilayahMiliter').hide();
        $('#wilayahAdministratif').show();
        if (imgMapAdministratif)
        {
            $('#imgMapAdministratif').mapster({
                opacity:1,
                stroke: false,
                clickNavigate: true,
                render_highlight: {
                    fillColor: '2aff00',
                    stroke: false,
                    altImage: '{{ url('assets/img/peta/provinsi_baru-hover.jpg') }}'
                },
                render_select: {
                    fillColor: 'ff000c',
                    stroke: false,
                    altImage: '{{ url('assets/img/peta/provinsi_baru-hover.jpg') }}'
                },
                fadeInterval: 50
            });
            imgMapAdministratif = false;
        }
    }
    else {
        $('#wilayahMiliter').show();
        $('#wilayahAdministratif').hide();
    }
}
</script>
@endsection