<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" type="image/png" sizes="16x16" href="{{ url('assets/img/icon.png?v=1.1') }}">
	<!-- <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}"> -->
	<!-- <link rel="stylesheet" href="{{ url('assets/css/bootstrap.css') }}"> -->
	<!-- <link rel="stylesheet" href="{{ url('assets/components/font-awesome/css/font-awesome.min.css') }}"> -->
	<link rel="stylesheet" href="{{ url('assets/css/main.css') }}">
	<!-- <link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet"> -->
	<!-- <link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet"> -->
	<!-- <link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet"> -->
	<!-- <link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet"> -->
	<style>
		.html * {
			text-transform: uppercase;
		}
		* {
			font-size: 28px !important;
			color: #000 !important;
			text-shadow: none !important;
			background: transparent !important;
			box-shadow: none !important;
			font-family: 'sans-serif';
		}
		table {
			border-collapse: collapse;
			border-spacing: 0;
		}
		@media print {
			a,
			a:visited {
				text-decoration: underline;
			}
			a[href]:after {
				content: " (" attr(href) ")";
			}
			abbr[title]:after {
				content: " (" attr(title) ")";
			}
			a[href^="javascript:"]:after,
			a[href^="#"]:after {
				content: "";
			}
			pre,
			blockquote {
				border: 1px solid #999;
				page-break-inside: avoid;
			}
			thead {
				display: table-header-group;
			}
			tr,
			img {
				page-break-inside: avoid;
			}
			.table td,
			.table th {
				background-color: #fff !important;
			}
			.table {
				border-collapse: collapse !important;
			}
			.table-bordered th,
			.table-bordered td {
				border: 1px solid #ddd !important;
			}
		}

		table {
			max-width: 100%;
			background-color: transparent;
		}

		th {
			text-align: left;
		}

		.table {
			width: 100%;
			margin-bottom: 20px;
		}

		.table > thead > tr > th,
		.table > tbody > tr > th,
		.table > tfoot > tr > th,
		.table > thead > tr > td,
		.table > tbody > tr > td,
		.table > tfoot > tr > td {
			padding: 8px;
			line-height: 1.428571429;
			vertical-align: top;
			border-top: 1px solid #dddddd;
		}

		.table > thead > tr > th {
			vertical-align: bottom;
			border-bottom: 2px solid #dddddd;
		}

		.table > caption + thead > tr:first-child > th,
		.table > colgroup + thead > tr:first-child > th,
		.table > thead:first-child > tr:first-child > th,
		.table > caption + thead > tr:first-child > td,
		.table > colgroup + thead > tr:first-child > td,
		.table > thead:first-child > tr:first-child > td {
			border-top: 0;
		}

		.table > tbody + tbody {
			border-top: 2px solid #dddddd;
		}

		.table .table {
			background-color: #ffffff;
		}

		.table-condensed > thead > tr > th,
		.table-condensed > tbody > tr > th,
		.table-condensed > tfoot > tr > th,
		.table-condensed > thead > tr > td,
		.table-condensed > tbody > tr > td,
		.table-condensed > tfoot > tr > td {
			padding: 5px;
		}

		.table-bordered {
			border: 1px solid #dddddd;
		}

		.table-bordered > thead > tr > th,
		.table-bordered > tbody > tr > th,
		.table-bordered > tfoot > tr > th,
		.table-bordered > thead > tr > td,
		.table-bordered > tbody > tr > td,
		.table-bordered > tfoot > tr > td {
			border: 1px solid #dddddd;
		}

		.table-bordered > thead > tr > th,
		.table-bordered > thead > tr > td {
			border-bottom-width: 2px;
		}

		.table-striped > tbody > tr:nth-child(odd) > td,
		.table-striped > tbody > tr:nth-child(odd) > th {
			background-color: #f9f9f9;
		}

		.table-hover > tbody > tr:hover > td,
		.table-hover > tbody > tr:hover > th {
			background-color: #f5f5f5;
		}

		table col[class*="col-"] {
			position: static;
			display: table-column;
			float: none;
		}

		table td[class*="col-"],
		table th[class*="col-"] {
			display: table-cell;
			float: none;
		}

		.table > thead > tr > .active,
		.table > tbody > tr > .active,
		.table > tfoot > tr > .active,
		.table > thead > .active > td,
		.table > tbody > .active > td,
		.table > tfoot > .active > td,
		.table > thead > .active > th,
		.table > tbody > .active > th,
		.table > tfoot > .active > th {
			background-color: #f5f5f5;
		}

		.table-hover > tbody > tr > .active:hover,
		.table-hover > tbody > .active:hover > td,
		.table-hover > tbody > .active:hover > th {
			background-color: #e8e8e8;
		}

		.table > thead > tr > .success,
		.table > tbody > tr > .success,
		.table > tfoot > tr > .success,
		.table > thead > .success > td,
		.table > tbody > .success > td,
		.table > tfoot > .success > td,
		.table > thead > .success > th,
		.table > tbody > .success > th,
		.table > tfoot > .success > th {
			background-color: #dff0d8;
		}

		.table-hover > tbody > tr > .success:hover,
		.table-hover > tbody > .success:hover > td,
		.table-hover > tbody > .success:hover > th {
			background-color: #d0e9c6;
		}

		.table > thead > tr > .danger,
		.table > tbody > tr > .danger,
		.table > tfoot > tr > .danger,
		.table > thead > .danger > td,
		.table > tbody > .danger > td,
		.table > tfoot > .danger > td,
		.table > thead > .danger > th,
		.table > tbody > .danger > th,
		.table > tfoot > .danger > th {
			background-color: #f2dede;
		}

		.table-hover > tbody > tr > .danger:hover,
		.table-hover > tbody > .danger:hover > td,
		.table-hover > tbody > .danger:hover > th {
			background-color: #ebcccc;
		}

		.table > thead > tr > .warning,
		.table > tbody > tr > .warning,
		.table > tfoot > tr > .warning,
		.table > thead > .warning > td,
		.table > tbody > .warning > td,
		.table > tfoot > .warning > td,
		.table > thead > .warning > th,
		.table > tbody > .warning > th,
		.table > tfoot > .warning > th {
			background-color: #fcf8e3;
		}

		.table-hover > tbody > tr > .warning:hover,
		.table-hover > tbody > .warning:hover > td,
		.table-hover > tbody > .warning:hover > th {
			background-color: #faf2cc;
		}

		@media (max-width: 767px) {
			.table-responsive {
				width: 100%;
				margin-bottom: 15px;
				overflow-x: scroll;
				overflow-y: hidden;
				border: 1px solid #dddddd;
				-ms-overflow-style: -ms-autohiding-scrollbar;
				-webkit-overflow-scrolling: touch;
			}
			.table-responsive > .table {
				margin-bottom: 0;
			}
			.table-responsive > .table > thead > tr > th,
			.table-responsive > .table > tbody > tr > th,
			.table-responsive > .table > tfoot > tr > th,
			.table-responsive > .table > thead > tr > td,
			.table-responsive > .table > tbody > tr > td,
			.table-responsive > .table > tfoot > tr > td {
				white-space: nowrap;
			}
			.table-responsive > .table-bordered {
				border: 0;
			}
			.table-responsive > .table-bordered > thead > tr > th:first-child,
			.table-responsive > .table-bordered > tbody > tr > th:first-child,
			.table-responsive > .table-bordered > tfoot > tr > th:first-child,
			.table-responsive > .table-bordered > thead > tr > td:first-child,
			.table-responsive > .table-bordered > tbody > tr > td:first-child,
			.table-responsive > .table-bordered > tfoot > tr > td:first-child {
				border-left: 0;
			}
			.table-responsive > .table-bordered > thead > tr > th:last-child,
			.table-responsive > .table-bordered > tbody > tr > th:last-child,
			.table-responsive > .table-bordered > tfoot > tr > th:last-child,
			.table-responsive > .table-bordered > thead > tr > td:last-child,
			.table-responsive > .table-bordered > tbody > tr > td:last-child,
			.table-responsive > .table-bordered > tfoot > tr > td:last-child {
				border-right: 0;
			}
			.table-responsive > .table-bordered > tbody > tr:last-child > th,
			.table-responsive > .table-bordered > tfoot > tr:last-child > th,
			.table-responsive > .table-bordered > tbody > tr:last-child > td,
			.table-responsive > .table-bordered > tfoot > tr:last-child > td {
				border-bottom: 0;
			}
		}

		.table { border: 1px solid #000000; }
		.table > thead > tr > th { text-align: center; vertical-align: middle; }
		.table > tbody > tr > td { text-align: right; vertical-align: middle; }
		.table > thead > tr > th, .table > tbody > tr > td { border: 1px solid #000000; padding-top: 0px !important; padding-bottom: 0px !important; }
	</style>
</head>
<body>
	<br><br><br>
	<div class="html">
		{!! $html !!}
	</div>
	<br><br>
	<div style="float: right; width: 30%;">
		<div style="text-align: center;">
			Jakarta, {{ $tanggalTtd }}<br>
			Direktur Veteran, <br>
			<br>
			<br>
			<br>
			<br>
			{{ GetData::setting()->direkturVeteran['value'] }}<br>
			{{ GetData::setting()->jabatanDirekturVeteran['value'] }}
		</div>
	</div>
</body>
</html>