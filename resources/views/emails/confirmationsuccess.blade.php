@extends('layouts.email')

@section('title', 'Rumah Otomotif')

@section('css')
@endsection

@section('content')
<div>
	<p>Hi {{ $name }}</p>
	<p>Konfirmasi pembayaran Anda telah berhasil di verifikasi, Terima Kasih telah melakukan booking service di Rumah Otomotif</p>

	<p><b>Regards<br>
	Rumah Otomotif</b></p>
</div>
@stop

@section('js')
@endsection