<!DOCTYPE html>
<html lang="en">
<head>
   <title>{{ GetData::setting()->applicationName['value'] }}, {{ GetData::setting()->applicationShortDescription['value'] }} :: @yield('title')</title>
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <link rel="icon" type="image/png" sizes="16x16" href="{{ url('assets/img/icon.png?v=1.1') }}">
   <link rel="stylesheet" href="{{ url('assets/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ url('assets/components/font-awesome/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ url('assets/css/main.css') }}">
   @yield('css')
</head>
<body>
   <nav id="nav-mobile">
      <div class="header-main-menu"><h3>SIM VETERAN</h3></div>
      <div class="header-sidebar">
         <div class="back-button" onclick="closeSideBarMenu()">
            <i class="fa fa-caret-left" aria-hidden="true"></i>
         </div>
         <h3>Direktorat Veteran</h3>
      </div>
      <div id="dismiss"><i class="fa fa-close"></i></div>
      <ul class="list-unstyled components side-bar-menu" id="administration">
         @include('includes.sidebar-admin')
      </ul>
   </nav>
   <div class="overlay"></div>

   <div id="main">
      <nav class="bars-menu navbar navbar-default" role="navigation">

         <div id="mobile">
            <div class="navbar-header">
               <button type="button" id="nav-mobileCollapse" class="navbar-toggle collapsed navbar-left" data-toggle="collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar top-bar"></span>
                  <span class="icon-bar mid-bar"></span>
                  <span class="icon-bar bot-bar"></span>
               </button>
               <div class="col-lg">
                  
               </div>
               <div class="logo" align="center">         
                  <a href="{{ route('home') }}">
                     <img id="logo-large" src="{{ url(''.GetData::setting()->imgLogo['value']) }}">
                     <!-- <img id="logo-init" src="{{ url('assets/img/logo-init.png') }}"> -->
                  </a>
               </div>

               <ul class="nav navbar-nav navbar-right">
                  <li class="home"><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                  @if (isset(Auth::user()->name))
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <div class="user">
                           @php
                              if (isset(Auth::user()->name))
                              {
                                 $initialName = explode(" ", Auth::user()->name);
                                 $initial = '';
                                 foreach ($initialName as $value)
                                 {
                                    $initial .= substr($value, 0, 1);
                                 }
                                 echo $initial;
                              }
                           @endphp
                        </div>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-right fa-ul" role="menu">
                        <li><a href="{{ route('myProfile') }}"><i class="{{ config('icon.top.myProfile') }}"></i>My Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="{{ config('icon.top.logout') }}"></i>Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </li>
                     </ul>
                  </li>
                  @else
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <div class="user">
                           <i class="fa fa-user"></i>
                        </div>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-right fa-ul" role="menu">
                        <li><a href="{{ url('login') }}"><i class="{{ config('icon.top.myProfile') }}"></i>Login</a></li>
                     </ul>
                  </li>
                  @endif
               </ul>
            </div>
         </div>

         <div class="container-fluid main-menu" id="navfluid">
            <div class="navbar-header">
               <div class="logo">
                  <a class="navbar-brand" href="{{ route('home') }}">
                     <img id="logo-large" src="{{ url(''.GetData::setting()->imgLogo['value']) }}" align="left">
                     &nbsp;
                     <span style="font-size: 12px; font-weight: bold;">{{ GetData::setting()->applicationName['value'] }}</span><br>
                     &nbsp;
                     <span style="font-size: 14px; font-weight: bold; vertical-align: middle; color: #333333; line-height: 18px;"><!-- Direktorat Veteran -->{{ GetData::setting()->applicationShortDescription['value'] }}</span>
                  </a>
                  <div class="clearfix"></div>
               </div>
            </div>
            <div style="float: left; height: 69px;"><img src="{{ url(''.GetData::setting()->mainImage['value']) }}" width="100%"></div>
            <div class="collapse navbar-collapse" id="navigationbar">
               <ul class="nav navbar-nav navbar-right">
                  <li><a href="{{ route('home') }}"><i class="fa fa-home"></i></a></li>
                  @if (isset(Auth::user()->name))
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <div class="user">
                           @php
                              if (isset(Auth::user()->name))
                              {
                                 $initialName = explode(" ", Auth::user()->name);
                                 $initial = '';
                                 foreach ($initialName as $value)
                                 {
                                    $initial .= substr($value, 0, 1);
                                 }
                                 echo $initial;
                              }
                           @endphp
                        </div>
                     </a>
                     <ul class="dropdown-menu fa-ul" role="menu">
                        <li><a href="{{ route('myProfile') }}"><i class="{{ config('icon.top.myProfile') }}"></i>My Profile</a></li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="{{ config('icon.top.logout') }}"></i>Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
                        </li>
                     </ul>
                  </li>
                  @else
                  <li class="dropdown">
                     <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        <div class="user">
                           <i class="fa fa-user"></i>
                        </div>
                     </a>
                     <ul class="dropdown-menu dropdown-menu-right fa-ul" role="menu">
                        <li><a href="{{ url('login') }}"><i class="{{ config('icon.top.myProfile') }}"></i>Login</a></li>
                     </ul>
                  </li>
                  @endif
               </ul>
            </div>
         </div>
      </nav>

      <div class="container-fluid text-center">    
         <div class="row container-content">
            <div class="col-sm-12" style="min-height: 100%;">
               @yield('content')
            </div>
         </div>
      </div>
      <footer class="container-fluid text-center">
         <p>{{ GetData::setting()->copyright['value'] }}</p>
      </footer>

   </div>

   <script src="{{ url('assets/js/jquery-3.2.1.min.js') }}"></script>
   <script src="{{ url('assets/js/jquery-migrate-3.0.1.js') }}"></script>
   <!-- <script src="{{ url('assets/js/jquery-1.11.0.min.js') }}"></script> -->
   <script src="{{ url('assets/js/bootstrap.min.js') }}"></script>
   <script src="{{ url('assets/js/moment.min.js') }}"></script>
   <script src="{{ url('assets/components/js-webshim/minified/polyfiller.js') }}"></script>
   <script src="{{ url('assets/js/main.js') }}" charset="utf-8" async defer></script>
   <script src="{{ url('assets/js/global.js') }}" charset="utf-8" async defer></script>
   <script>
      var publicUrl = '{{ url('') }}/';
      var mediaSelect = false;
      $('.dropdown').click(function(e)
      {
         $(this).children('ul').slideToggle('fast');
         $(this).children('a').children('span').children('i').toggleClass('fa-angle-left fa-angle-down')
      });
   </script>
   @yield('js')
</body>
</html>
