@extends('layouts.admin')

@section('title', 'Kanminvetcad')
@section('kanminvetcad', 'active')

@section('css')
<link href="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">
<link href="{{ url('assets/components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" id="container-content">
    <div class="row row-offcanvas row-offcanvas-left">
        
        <!-- SIDEBAR -->
        <div class="col-xs-4 col-sm-2 sidebar-offcanvas" id="sidebar" role="navigation">
            <div class="sidebar-nav">
                @include('includes.sidebar-admin')
            </div>
        </div>
        <!-- SIDEBAR -->

        <div class="container-trailing">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"><i class="{{ config('icon.sidebarAdmin.dashboard') }}" aria-hidden="true"></i>Dashboard</a>&nbsp;&nbsp;&nbsp;
                </li>
                <li>
                    <i class="{{ config('icon.sidebarAdmin.kodim') }}"></i>Kanminvetcad
                </li>
            </ol>
        </div>

        <div class="content">
            @if (Session::has('message'))
            <div class="alert alert-info">{{ Session::get('message') }}</div>
            @endif
            @if (in_array('ALL', Config::get('menu.role.kanminvetcad.create')))
            <div class="btn-container">
                <a href="{{ route('kanminvetcad.create') }}" class="btn btn-add">Add New</a>
            </div>
            @else
                @if (isset(Auth::user()->roleCode) && in_array(Auth::user()->roleCode, Config::get('menu.role.kanminvetcad.create')))
                <div class="btn-container">
                    <a href="{{ route('kanminvetcad.create') }}" class="btn btn-add">Add New</a>
                </div>
                @endif
            @endif
            <div>
                <form action="" method="get" id="search-form">
                    <div class="row">
                        <div class="col-md-3">
                            <select name="kodam" class="form-control select2" id="kodam">
                                <option value="">Pilih Babinminvetcaddam</option>
                                @foreach ($list['kodam'] as $key => $data)
                                <option value="{{ $data['idKodam'] }}">{{ $data['namaKodam'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-1">
                            <button type="submit" class="btn btn-primary">SEARCH</button>
                        </div>
                        <div class="col-md-1">
                            <button type="reset" class="btn btn-default" onclick="resetData()">RESET</button>
                        </div>
                    </div>
                </form>
            </div><br>
            <div class="container-fluid table-responsive" align="center-block">

                <table class="table table-striped table-hover" id="dataTables">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Kanminvetcad</th>
                            <th>Babinminvetcaddam</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="{{ url('assets/components/datatables/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('assets/components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
    listManager.listUrl = "{{ route('kanminvetcad.index') }}/list";
    listManager.deleteUrl = "{{ route('kanminvetcad.index') }}";
    listManager.token = "{{ csrf_token() }}";
    listManager.dataTable = [
                                { "data": "idKodim", "name": "kodim.idKodim" },
                                { "data": "namaKodim", "name": "kodim.namaKodim" },
                                { "data": "namaKodam", "name": "kodam.namaKodam" },
                                { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"},
    ];
    listManager.ready();

    $('#search-form').on('submit', function(e)
    {
        var kodam = $('#kodam').val();
        var kodim = $('#kodim').val();
        var customSearch = "kodam="+kodam+"&kodim="+kodim;
        listManager.destroy();
        listManager.customSearch = customSearch;
        listManager.listUrl = "{{ route('kanminvetcad.index') }}/list";
        listManager.deleteUrl = "{{ route('kanminvetcad.index') }}";
        listManager.token = "{{ csrf_token() }}";
        listManager.dataTable = [
                                    { "data": "idKodim", "name": "kodim.idKodim" },
                                    { "data": "namaKodim", "name": "kodim.namaKodim" },
                                    { "data": "namaKodam", "name": "kodam.namaKodam" },
                                    { "data": 'action', name: 'action', orderable: false, searchable: false, "sClass": "actionList"},
        ];
        listManager.ready();
        e.preventDefault();
    });
});
</script>
@endsection