<?php
$data[] = array(
			'NO' => '1',
			'NAMA' => 'Ahmad',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1914',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '114/E/1/VII/1960',
			'TANGGAL PENDAFTARAN' => '30-07-1960',
			'NPV LAMA' => '5913/E',
			'NPV BARU' => '1000001',
			'NOMOR SURAT KEPUTUSAN' => ' SKEP/956/VIII/1981',
			'TANGGAL SURAT KEPUTUSAN' =>' ',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' =>'ya',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>'3',
			'KANMINVETCAD' =>'4',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => 'Ds. Gunungkleng Meureubo',
			'KECAMATAN' => 'Kec. Mereubo',
			'KABUPATEN' => '6',
			'PROPINSI' => '2',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' => 'L V R I'
		);
$data[] = array(
			'NO' => '2',
			'NAMA' => 'Abdullah Agam',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '28-05-1918',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '138/E/1/VII/1960',
			'TANGGAL PENDAFTARAN' => '30-07-1960',
			'NPV LAMA' => '5927/E',
			'NPV BARU' => '1000002',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' =>'tidak',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>'10',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

$data[] = array(
			'NO' => '3',
			'NAMA' => 'Aziz (abd)',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1910',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '71/E/1/VI/60',
			'TANGGAL PENDAFTARAN' => '06-07-1960',
			'NPV LAMA' => '5945/E',
			'NPV BARU' => '1000003',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '2',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' =>'tidak',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

$data[] = array(
			'NO' => '4',
			'NAMA' => 'Abdurrani',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1913',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '79/E/1/VI/1960',
			'TANGGAL PENDAFTARAN' => '21-06-1960',
			'NPV LAMA' => '5948/E',
			'NPV BARU' => '1000004',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'ya',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

$data[] = array(
			'NO' => '5',
			'NAMA' => 'Aliusman',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1906',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '346/E/1/VI/1960',
			'TANGGAL PENDAFTARAN' => '21-06-1960',
			'NPV LAMA' => '7440/E',
			'NPV BARU' => '1000005',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '2',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'ya',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

$data[] = array(
			'NO' => '6',
			'NAMA' => 'Ali Akbar Bay',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1914',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '534/E/I/X/1960',
			'TANGGAL PENDAFTARAN' => '22-10-1960',
			'NPV LAMA' => '14178/E',
			'NPV BARU' => '1000006',
			'NOMOR SURAT KEPUTUSAN' => ' SKEP/956/VIII/1981',
			'TANGGAL SURAT KEPUTUSAN' =>' ',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'ya',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => 'Ds. Blang Luah',
			'KECAMATAN' => 'Kec. Woyla Barat',
			'KABUPATEN' => '6',
			'PROPINSI' => '2',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' => 'L V R I'
		);

$data[] = array(
			'NO' => '7',
			'NAMA' => 'Amin (m)',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1920',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '275/E/I/IX/1960',
			'TANGGAL PENDAFTARAN' => '15-09-1960',
			'NPV LAMA' => '15849/E',
			'NPV BARU' => '1000007',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '3',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'tidak',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

$data[] = array(
			'NO' => '8',
			'NAMA' => 'Ahmad K',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1915',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '388/E/I/X/1960',
			'TANGGAL PENDAFTARAN' => '19-10-1960',
			'NPV LAMA' => '18161/E',
			'NPV BARU' => '1000008',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'tidak',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

$data[] = array(
			'NO' => '9',
			'NAMA' => 'Abdullah Satri',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1929',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '597/E/I/X/1960',
			'TANGGAL PENDAFTARAN' => '11-02-1960',
			'NPV LAMA' => '19926/E',
			'NPV BARU' => '1000009',
			'NOMOR SURAT KEPUTUSAN' => ' SKEP/956/VIII/1981',
			'TANGGAL SURAT KEPUTUSAN' =>' ',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'tidak',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => 'Ds. Menuang Kinco',
			'KECAMATAN' => 'Kec. Pante Ceureumen',
			'KABUPATEN' => '6',
			'PROPINSI' => '2',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' => 'L V R I'
		);

$data[] = array(
			'NO' => '10',
			'NAMA' => 'Abdurrahman Af',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1930',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '843/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '25-11-1960',
			'NPV LAMA' => '20132/E',
			'NPV BARU' => '1000010',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'ya',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

$data[] = array(
			'NO' => '11',
			'NAMA' => 'Agam Tjut',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '29-04-1928',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NIP' =>' ',
			'NOMOR PENDAFTRAN' => '903/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '25-11-1960',
			'NPV LAMA' => '20244/E',
			'NPV BARU' => '1000011',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'MENINGGAL' => 'tidak',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'TEMPAT PEMBAYARAN' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' '
		);

return $data;
/*
    [11] => 'Array',
        (
			'NO' => '12',
			'NAMA' => 'Abubakar M',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1927',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '785/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '25-11-1960',
			'NPV LAMA' => '20326/E',
			'NPV BARU' => '1000012',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [12] => 'Array',
        (
			'NO' => '13',
			'NAMA' => 'Abubakar Qidamy (tgk)',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1921',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '716/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '11-04-1960',
			'NPV LAMA' => '20334/E',
			'NPV BARU' => '1000013',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '2',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [13] => 'Array',
        (
			'NO' => '14',
			'NAMA' => 'Ahmad T',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1925',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '733/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '25-11-1960',
			'NPV LAMA' => '20353/E',
			'NPV BARU' => '1000014',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [14] => 'Array',
        (
			'NO' => '15',
			'NAMA' => 'Anzib',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1914',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '806/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '25-11-1960',
			'NPV LAMA' => '20383/E',
			'NPV BARU' => '1000015',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [15] => 'Array',
        (
			'NO' => '16',
			'NAMA' => 'Arsyad (mohd)',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1926',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '1342/E/I/XII/1960',
			'TANGGAL PENDAFTARAN' => '25-12-1960',
			'NPV LAMA' => '20472/E',
			'NPV BARU' => '1000016',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '3',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [16] => 'Array',
        (
			'NO' => '17',
			'NAMA' => 'Abdullah Umar',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1920',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '1582/E/I/XII/1960',
			'TANGGAL PENDAFTARAN' => '23-12-1960',
			'NPV LAMA' => '20529/E',
			'NPV BARU' => '1000017',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [17] => 'Array',
        (
			'NO' => '18',
			'NAMA' => 'Ali Zen',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1924',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '158/E/I/VIII/1960',
			'TANGGAL PENDAFTARAN' => '23-08-1960',
			'NPV LAMA' => '16912/E',
			'NPV BARU' => '1000018',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '2',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [18] => 'Array',
        (
			'NO' => '19',
			'NAMA' => 'Ali Hasan',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1915',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '157/E/I/XIII/1960',
			'TANGGAL PENDAFTARAN' => '23-08-1960',
			'NPV LAMA' => '16943/E',
			'NPV BARU' => '1000019',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [19] => 'Array',
        (
			'NO' => '20',
			'NAMA' => 'Atim',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1901',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '2764/E/V/1961',
			'TANGGAL PENDAFTARAN' => '31-05-1961',
			'NPV LAMA' => '31359/E',
			'NPV BARU' => '1000020',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [20] => 'Array',
        (
			'NO' => '21',
			'NAMA' => 'Abdullah Bugeh',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1926',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '2320/E/I/IV/1961',
			'TANGGAL PENDAFTARAN' => '04-01-1961',
			'NPV LAMA' => '33705/E',
			'NPV BARU' => '1000021',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [21] => 'Array',
        (
			'NO' => '22',
			'NAMA' => 'Abu Kasim',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1926',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '1504/E/I/XII/1961',
			'TANGGAL PENDAFTARAN' => '12-07-1961',
			'NPV LAMA' => '26519/E',
			'NPV BARU' => '1000022',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [22] => 'Array',
        (
			'NO' => '23',
			'NAMA' => 'Abdullah Ali',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1931',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '2747/E/I/V/1961',
			'TANGGAL PENDAFTARAN' => '29-05-1961',
			'NPV LAMA' => '32989/E',
			'NPV BARU' => '1000023',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [23] => 'Array',
        (
			'NO' => '24',
			'NAMA' => 'Aziz Ben Hamid',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1923',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '2847/E/I/VI/1961',
			'TANGGAL PENDAFTARAN' => '06-06-1961',
			'NPV LAMA' => '27477/E',
			'NPV BARU' => '1000024',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [24] => 'Array',
        (
			'NO' => '25',
			'NAMA' => 'Abubakar Nurdin',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1929',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '2862/E/I/VI/1961',
			'TANGGAL PENDAFTARAN' => '06-07-1961',
			'NPV LAMA' => '27492/E',
			'NPV BARU' => '1000025',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '2',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [25] => 'Array',
        (
			'NO' => '26',
			'NAMA' => 'Abdullah',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '-',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '831/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '25-11-1960',
			'NPV LAMA' => '24346/E',
			'NPV BARU' => '1000026',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [26] => 'Array',
        (
			'NO' => '27',
			'NAMA' => 'Aziz Banta (abd)',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1930',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '121/E/I/VI/1960',
			'TANGGAL PENDAFTARAN' => '30-06-1960',
			'NPV LAMA' => '32301/E',
			'NPV BARU' => '1000027',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '2',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [27] => 'Array',
        (
			'NO' => '28',
			'NAMA' => 'Andah',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1924',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '237/E/I/IX/1960',
			'TANGGAL PENDAFTARAN' => '09-10-1960',
			'NPV LAMA' => '32203/E',
			'NPV BARU' => '1000028',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [28] => 'Array',
        (
			'NO' => '29',
			'NAMA' => 'Abah Yatim',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1925',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '735/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '11-04-1960',
			'NPV LAMA' => '25312/E',
			'NPV BARU' => '1000029',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [29] => 'Array',
        (
			'NO' => '30',
			'NAMA' => 'Ali Basyah',
			'TEMPAT LAHIR' => '-',
			'TANGGAL LAHIR' => '1925',
			'JENIS KELAMIN' => 'L',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => '738/E/I/XI/1960',
			'TANGGAL PENDAFTARAN' => '11-04-1960',
			'NPV LAMA' => '25319/E',
			'NPV BARU' => '1000030',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'A',
			'MASA BAKTI TAHUN' => '4',
			'MASA BAKTI BULAN' => '4',
			'TANGGAL MENINGGAL' =>' ',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' => '-',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => '-',
			'KECAMATAN' => '-',
			'KABUPATEN' => '-',
			'PROPINSI' => '-',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )

    [30] => 'Array',
        (
			'NO' => '31',
			'NAMA' => 'Cut Ketek',
			'TEMPAT LAHIR' => 'Silolo',
			'TANGGAL LAHIR' => '13-02-1927',
			'JENIS KELAMIN' => 'P',
			'NIK' =>' ',
			'NOMOR PENDAFTRAN' => 'IM/08/61/I/2016',
			'TANGGAL PENDAFTARAN' => '2016-01-02',
			'NPV LAMA' => '-',
			'NPV BARU' => '1110316',
			'NOMOR SURAT KEPUTUSAN' => '-',
			'TANGGAL SURAT KEPUTUSAN' => '-',
			'PREDIKAT' => 'PKRI',
			'GOLONGAN' => 'C',
			'MASA BAKTI TAHUN' => '2',
			'MASA BAKTI BULAN' => '0',
			'TANGGAL MENINGGAL' => '-',
			'NAMA AHLI WARIS' => '-',
			'TGL LAHIR AHLI WARIS' =>' ',
			'NIK AHLI WARIS' =>' ',
			'TANGGAL MENINGGAL AHLI WARIS' =>' ',
			'BABINMINVETCADDAM' =>' ',
			'KANMINVETCAD' =>' ',
			'JALAN' =>' ',
			'RT' =>' ',
			'RW' =>' ',
			'DESA/KELURAHAN' => 'Desa Silolo',
			'KECAMATAN' => 'Pasi Raja',
			'KABUPATEN' => 'Kab. Aceh Selatan',
			'PROPINSI' => '2',
			'NO KEP TUVET' =>' ',
			'TGL KEP TUVET' =>' ',
			'NO KEP DAHOR' =>' ',
			'TGL KEP DAHOR' =>' ',
			'NO KEP TUNDAYATU' =>' ',
			'TGL KEP TUNDAYATU' =>' ',
			'NO REKENING' =>' ',
			'NO KARIP' => ' ',
			'PT TASPEN' =>' ',
			'KETERANGAN' =>' ',
			'0' =>' ',
        )
)*/