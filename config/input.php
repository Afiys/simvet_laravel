<?php
// JENIS KELAMIN 1:Laki-laki, 2:Perempuan
return [
    'veteran' => [
        'halaman1' => [
            /*[
                'type' => 'select', 'name' => 'idKodam', 'id' => 'Babinminvetcaddam', 'placeholder' => 'Jenis Kelamin', 'required' => false, 'readonly' => false, 'disabled' => false, 'onchange' => 'loadSelect(this, \'Kanminvetcad\', \'{{ url(\'kanminvetcad/get\') }}\', \'{{ isset($item->idKodim) ? $item->idKodim : \'\' }}\');loadSelect(this, \'Provinsi\', \'{{ url(\'provinsi/get\') }}\', \'{{ isset($item->idProvinsi) ? $item->idProvinsi : \'\' }}\');',
                'dbData' => '<option value="">Pilih Babinminvetcaddam</option>
                    @foreach ($lookupTable[\'kodam\'] as $kodam)
                    <option value="{{ $kodam->idKodam }}" {{ (isset($item) && $item->idKodam == $kodam->idKodam) ? \'selected=selected\' : \'\' }}>{{ $kodam->namaKodam }}</option>
                    @endforeach'
            ],*/

            ['type' => 'text', 'name' => 'namaVeteran', 'id' => 'namaVeteran', 'placeholder' => 'Nama Veteran', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'tempatLahir', 'id' => 'tempatLahir', 'placeholder' => 'Tempat Lahir', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tanggalLahir', 'id' => 'tanggalLahir', 'placeholder' => 'Tanggal Lahir', 'required' => false, 'readonly' => false, 'disabled' => false],
            [
                'type' => 'select', 'name' => 'jenisKelamin', 'id' => 'jenisKelamin', 'placeholder' => 'Jenis Kelamin', 'required' => false, 'readonly' => false, 'disabled' => false, 'onchange' => false, 'dbData' => false,
                'option' => [
                    ['value' => '', 'text' => 'Pilih Jenis Kelamin'],
                    ['value' => '1', 'text' => 'Laki-laki'],
                    ['value' => '2', 'text' => 'Perempuan'],
                ]
            ],
            ['type' => 'text', 'name' => 'NIK', 'id' => 'NIK', 'placeholder' => 'NIK', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'noPendaftaran', 'id' => 'noPendaftaran', 'placeholder' => 'No Pendaftaran', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tanggalPendaftaran', 'id' => 'tanggalPendaftaran', 'placeholder' => 'Tanggal Pendaftaran', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'npvLama', 'id' => 'npvLama', 'placeholder' => 'NPV Lama', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'npvBaru', 'id' => 'npvBaru', 'placeholder' => 'NPV Baru', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'noKeputusan', 'id' => 'noKeputusan', 'placeholder' => 'No Keputusan', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tanggalKeputusan', 'id' => 'tanggalKeputusan', 'placeholder' => 'Tanggal Keputusan', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'predikat', 'id' => 'predikat', 'placeholder' => 'Predikat', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'golongan', 'id' => 'golongan', 'placeholder' => 'Golongan', 'required' => false, 'readonly' => false, 'disabled' => false],
        ],
        'halaman2' => [
            ['type' => 'text', 'name' => 'tahunMasaBakti', 'id' => 'tahunMasaBakti', 'placeholder' => 'Tahun Masa Bakti', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'bulanMasaBakti', 'id' => 'bulanMasaBakti', 'placeholder' => 'Bulan Masa Bakti', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tanggalMeninggal', 'id' => 'tanggalMeninggal', 'placeholder' => 'Tanggal Meninggal', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'namaAhliWaris', 'id' => 'namaAhliWaris', 'placeholder' => 'Nama Ahli Waris', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tanggalLahirAhliWaris', 'id' => 'tanggalLahirAhliWaris', 'placeholder' => 'Tanggal Lahir Ahli Waris', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'NIKAhliWaris', 'id' => 'NIKAhliWaris', 'placeholder' => 'NIK Ahli Waris', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tanggalMeninggalAhliWaris', 'id' => 'tanggalMeninggalAhliWaris', 'placeholder' => 'Tanggal Meninggal Ahli Waris', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'textarea', 'name' => 'alamat', 'id' => 'alamat', 'placeholder' => 'Alamat', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'rt', 'id' => 'rt', 'placeholder' => 'RT', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'rw', 'id' => 'rw', 'placeholder' => 'RW', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'kelurahan', 'id' => 'kelurahan', 'placeholder' => 'Kelurahan', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'kecamatan', 'id' => 'kecamatan', 'placeholder' => 'Kecamatan', 'required' => false, 'readonly' => false, 'disabled' => false],
        ],
        'halaman3' => [
            ['type' => 'text', 'name' => 'noKepTuvet', 'id' => 'noKepTuvet', 'placeholder' => 'No Kep Tuvet', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tglKepTuvet', 'id' => 'tglKepTuvet', 'placeholder' => 'Tanggal Kep Tuvet', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'noKepDahor', 'id' => 'noKepDahor', 'placeholder' => 'No Kep Dahor', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tglKepDahor', 'id' => 'tglKepDahor', 'placeholder' => 'Tanggal Kep Dahor', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'noKepTundayatu', 'id' => 'noKepTundayatu', 'placeholder' => 'No Kep Tundayatu', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'date', 'name' => 'tglKepTundayatu', 'id' => 'tglKepTundayatu', 'placeholder' => 'Tanggal Kep Tundayatu', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'nomorRekening', 'id' => 'nomorRekening', 'placeholder' => 'Nomor Rekening', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'text', 'name' => 'karipPTTaspen', 'id' => 'karipPTTaspen', 'placeholder' => 'Karip PT Taspen', 'required' => false, 'readonly' => false, 'disabled' => false],
            ['type' => 'textarea', 'name' => 'keterangan', 'id' => 'keterangan', 'placeholder' => 'Keterangan', 'required' => false, 'readonly' => false, 'disabled' => false],
        ],
    ],
];