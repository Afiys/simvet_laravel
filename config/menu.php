<?php

/**
 * FOR ROLE EVERY PAGE
 * ALL/SUP/ADM
 * ALL Without Login 
 * Then Config to Route
 */
$role = [
	'dashboard' => [
		'index' => ['ALL'],
		'wilayah' => ['ALL'],
		'usia' => ['ALL'],
		'jeniskelamin' => ['ALL'],
		'predikat' => ['ALL'],
		'rekapPerkodam' => ['ALL'],
		'rekapPerProvinsi' => ['ALL'],
	],
	'veteran' => [
		'index' => ['ALL'],
		'create' => ['SUP', 'ADM'],
		'show' => ['ALL'],
		'rh' => ['ALL'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
		'export' => ['SUP', 'ADM'],
		'import' => ['SUP', 'ADM'],
		'downloadsample' => ['SUP', 'ADM'],
	],
	'veteranselisih' => [
		'index' => ['SUP'],
		'create' => ['SUP'],
		'show' => ['SUP'],
		'rh' => ['SUP'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
		'export' => ['SUP'],
		'import' => ['SUP'],
		'downloadsample' => ['SUP'],
	],
	'kritiksaran' => [
		'index' => ['ALL'],
		'create' => ['ALL'],
		'show' => ['ALL'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],
	'laporan' => [
		'index' => ['ALL'],
		'wilayah' => ['ALL'],
		'usia' => ['ALL'],
		'jeniskelamin' => ['ALL'],
		'predikat' => ['ALL'],
	],

	'master' => ['SUP', 'ADM'],
	'babinminvetcaddam' => [
		'index' => ['SUP', 'ADM'],
		'create' => ['SUP'],
		'show' => ['SUP', 'ADM'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],
	'kanminvetcad' => [
		'index' => ['SUP', 'ADM'],
		'create' => ['SUP'],
		'show' => ['SUP', 'ADM'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
		'get' => ['ALL'],
	],
	'provinsi' => [
		'index' => ['SUP', 'ADM'],
		'create' => ['SUP'],
		'show' => ['SUP', 'ADM'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
		'get' => ['ALL'],
	],
	'kabupaten' => [
		'index' => ['SUP', 'ADM'],
		'create' => ['SUP'],
		'show' => ['SUP', 'ADM'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
		'get' => ['ALL'],
	],
	'media' => [
		'index' => ['SUP', 'ADM'],
		'create' => ['SUP', 'ADM'],
		'uploadFileMedia' => ['SUP', 'ADM'],
		'show' => ['SUP', 'ADM'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],
	'manualbook' => [
		'index' => ['SUP', 'ADM'],
		'create' => ['SUP'],
		'uploadFileManualBook' => ['SUP'],
		'show' => ['SUP', 'ADM'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],
	/*'peraturan' => [
		'index' => ['ALL'],
		'create' => ['SUP'],
		'uploadFilePeraturan' => ['SUP'],
		'show' => ['ALL'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],*/
	'peraturan' => [
		'index' => ['ALL'],
		'create' => ['SUP'],
		'uploadFilePeraturan' => ['SUP'],
		'show' => ['ALL'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],
	'setting' => [
		'index' => ['SUP'],
		'create' => ['SUP'],
		'show' => ['SUP'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],
	'user' => [
		'index' => ['SUP'],
		'create' => ['SUP'],
		'show' => ['SUP'],
		'update' => ['SUP'],
		'destroy' => ['SUP'],
	],
];


/**
 * FOR CREATE SIDE BAR MENU 
 */
$menu = [
    [
        'text'  => 'Dashboard',
        'url'   => 'dashboard',
        'icon'  => 'dashboard',
        'role'  => $role['dashboard']['index'],
        'page'	=> 'dashboard',
        /*'submenu' => [
            [
		        'text'  => 'Wilayah',
		        'url'   => 'dashboard',
		        'icon'  => 'dashboard',
		        'role'  => $role['dashboard']['wilayah'],
		        'page'	=> 'dashboard',
            ],
            [
		        'text'  => 'Usia',
		        'url'   => 'dashboard-usia',
		        'icon'  => 'dashboard',
		        'role'  => $role['dashboard']['usia'],
		        'page'	=> 'dashboard-usia',
            ],
            [
		        'text'  => 'Jenis Kelamin',
		        'url'   => 'dashboard-jeniskelamin',
		        'icon'  => 'dashboard',
		        'role'  => $role['dashboard']['jeniskelamin'],
		        'page'	=> 'dashboard-jeniskelamin',
            ],
            [
		        'text'  => 'Predikat',
		        'url'   => 'dashboard-predikat',
		        'icon'  => 'dashboard',
		        'role'  => $role['dashboard']['predikat'],
		        'page'	=> 'dashboard-predikat',
            ],
        ],*/
    ],
    [
        'text'  => 'Data Veteran',
        'url'   => 'veteran',
        'icon'  => 'veteran',
        'role'  => $role['veteran']['index'],
        'page'	=> 'veteran',
    ],
    [
        'text'  => 'Peraturan',
        'url'   => 'peraturan',
        'icon'  => 'peraturan',
        'role'  => $role['peraturan']['index'],
        'page'	=> 'peraturan',
    ],
    [
        'text'  => 'Kritik dan saran',
        'url'   => 'kritiksaran',
        'icon'  => 'kritiksaran',
        'role'  => $role['kritiksaran']['index'],
        'page'	=> 'kritiksaran',
    ],
    /*[
        'text'  => 'Laporan',
        'url'   => 'laporan',
        'icon'  => 'laporan',
        'role'  => $role['laporan']['index'],
        'page'	=> 'laporan',
    ],*/

    'title' => [
    	'text' => 'MASTER',
        'role'  => $role['master'],
    ],
    [
        'text'  => 'Babinminvetcaddam',
        'url'   => 'babinminvetcaddam',
        'icon'  => 'kodam',
        'role'  => $role['babinminvetcaddam']['index'],
        'page'	=> 'babinminvetcaddam',
    ],
    [
        'text'  => 'Kanminvetcad',
        'url'   => 'kanminvetcad',
        'icon'  => 'kodim',
        'role'  => $role['kanminvetcad']['index'],
        'page'	=> 'kanminvetcad',
    ],
    [
        'text'  => 'Provinsi',
        'url'   => 'provinsi',
        'icon'  => 'provinsi',
        'role'  => $role['provinsi']['index'],
        'page'	=> 'provinsi',
    ],
    [
        'text'  => 'Kabupaten/Kota',
        'url'   => 'kabupaten',
        'icon'  => 'kabupaten',
        'role'  => $role['kabupaten']['index'],
        'page'	=> 'kabupaten',
    ],
    [
        'text'  => 'Data Veteran Selisih',
        'url'   => 'veteranselisih',
        'icon'  => 'veteran',
        'role'  => $role['veteranselisih']['index'],
        'page'	=> 'veteranselisih',
    ],
    [
        'text'  => 'Media',
        'url'   => 'media',
        'icon'  => 'media',
        'role'  => $role['media']['index'],
        'page'	=> 'media',
    ],
    [
        'text'  => 'Manual Book',
        'url'   => 'manualbook',
        'icon'  => 'manualbook',
        'role'  => $role['manualbook']['index'],
        'page'	=> 'manualbook',
    ],
    [
        'text'  => 'Setting',
        'url'   => 'setting',
        'icon'  => 'setting',
        'role'  => $role['setting']['index'],
        'page'	=> 'setting',
    ],
    [
        'text'  => 'User',
        'url'   => 'user',
        'icon'  => 'user',
        'role'  => $role['user']['index'],
        'page'	=> 'user',
    ],
];
return ['menu' => $menu, 'role' => $role];