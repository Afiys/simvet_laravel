<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

## AUTH
// Route::post('/login', ['uses' => 'Api\Auth\LoginController@login', 'middleware' => 'APIAuthentication']);
// Route::post('/register', ['uses' => 'Api\Auth\RegisterController@store', 'middleware' => 'APIAuthentication']);