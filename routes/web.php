<?php

Route::get('/', 'Admin\Dashboard@index')->middleware('AuthRole:'.getRoleMenu('dashboard.index'));
Route::get('/dashboard', 'Admin\Dashboard@index')->name('home')->middleware('AuthRole:'.getRoleMenu('dashboard.index'));

Route::get('/rekap-babinminvetcaddam/{tipeData}/{id}', 'Admin\Dashboard@rekapPerkodamNewTab')->name('rekapPerkodamNewTab')->middleware('AuthRole:'.getRoleMenu('dashboard.rekapPerkodam'));
Route::post('/rekapPerbabinminvetcaddam', 'Admin\Dashboard@rekapPerkodam')->name('rekapPerkodam')->middleware('AuthRole:'.getRoleMenu('dashboard.rekapPerkodam'));
Route::post('/rekapPerbabinminvetcaddamPDF', 'Admin\Dashboard@rekapPerkodamPDF')->name('rekapPerkodamPDF')->middleware('AuthRole:'.getRoleMenu('dashboard.rekapPerkodam'));

Route::get('/rekap-provinsi/{tipeData}/{id}', 'Admin\Dashboard@rekapPerProvinsiNewTab')->name('rekapPerProvinsiNewTab')->middleware('AuthRole:'.getRoleMenu('dashboard.rekapPerProvinsi'));
Route::post('/rekapPerProvinsi', 'Admin\Dashboard@rekapPerProvinsi')->name('rekapPerProvinsi')->middleware('AuthRole:'.getRoleMenu('dashboard.rekapPerProvinsi'));
Route::post('/rekapPerProvinsiPDF', 'Admin\Dashboard@rekapPerProvinsiPDF')->name('rekapPerProvinsiPDF')->middleware('AuthRole:'.getRoleMenu('dashboard.rekapPerProvinsi'));

Route::post('/search', 'Admin\Dashboard@search')->name('search')->middleware('AuthRole:'.getRoleMenu('veteran.index'));

Route::group(['prefix' => 'veteran'], function ()
{
	Route::get('/', 'Admin\Veteran@index')->name('veteran.index')->middleware('AuthRole:'.getRoleMenu('veteran.index'));
	Route::post('/', 'Admin\Veteran@store')->name('veteran.store')->middleware('AuthRole:'.getRoleMenu('veteran.create'));
	Route::get('/create', 'Admin\Veteran@create')->name('veteran.create')->middleware('AuthRole:'.getRoleMenu('veteran.create'));
	Route::post('/list', 'Admin\Veteran@listData')->middleware('AuthRole:'.getRoleMenu('veteran.index'));
	Route::post('/downloadsample', 'Admin\Veteran@downloadSample')->name('veteran.downloadsample')->middleware('AuthRole:'.getRoleMenu('veteran.downloadsample'));
	Route::get('/{id}', 'Admin\Veteran@show')->middleware('AuthRole:'.getRoleMenu('veteran.show'));
	Route::get('/{id}/rh', 'Admin\Veteran@rh')->middleware('AuthRole:'.getRoleMenu('veteran.rh'));
	Route::put('/{id}', 'Admin\Veteran@update')->middleware('AuthRole:'.getRoleMenu('veteran.update'));
	Route::delete('/{id}', 'Admin\Veteran@destroy')->middleware('AuthRole:'.getRoleMenu('veteran.destroy'));
	Route::post('delete/selected', 'Admin\Veteran@deleteSelected')->middleware('AuthRole:'.getRoleMenu('veteran.destroy'));
	Route::get('/{id}/edit', 'Admin\Veteran@edit')->middleware('AuthRole:'.getRoleMenu('veteran.update'));
	Route::post('/export', 'Admin\Veteran@export')->name('veteran.export')->middleware('AuthRole:'.getRoleMenu('veteran.export'));
	Route::post('/import', 'Admin\Veteran@import')->name('veteran.import')->middleware('AuthRole:'.getRoleMenu('veteran.import'));
});

Route::group(['prefix' => 'veteranselisih'], function ()
{
	Route::get('/', 'Admin\VeteranSelisih@index')->name('veteranselisih.index')->middleware('AuthRole:'.getRoleMenu('veteranselisih.index'));
	Route::post('/', 'Admin\VeteranSelisih@store')->name('veteranselisih.store')->middleware('AuthRole:'.getRoleMenu('veteranselisih.create'));
	Route::get('/create', 'Admin\VeteranSelisih@create')->name('veteranselisih.create')->middleware('AuthRole:'.getRoleMenu('veteranselisih.create'));
	Route::post('/list', 'Admin\VeteranSelisih@listData')->middleware('AuthRole:'.getRoleMenu('veteranselisih.index'));
	Route::post('/downloadsample', 'Admin\VeteranSelisih@downloadSample')->name('veteranselisih.downloadsample')->middleware('AuthRole:'.getRoleMenu('veteranselisih.downloadsample'));
	Route::get('/{id}', 'Admin\VeteranSelisih@show')->middleware('AuthRole:'.getRoleMenu('veteranselisih.show'));
	Route::get('/{id}/rh', 'Admin\VeteranSelisih@rh')->middleware('AuthRole:'.getRoleMenu('veteranselisih.rh'));
	Route::put('/{id}', 'Admin\VeteranSelisih@update')->middleware('AuthRole:'.getRoleMenu('veteranselisih.update'));
	Route::delete('/{id}', 'Admin\VeteranSelisih@destroy')->middleware('AuthRole:'.getRoleMenu('veteranselisih.destroy'));
	Route::post('delete/selected', 'Admin\VeteranSelisih@deleteSelected')->middleware('AuthRole:'.getRoleMenu('veteranselisih.destroy'));
	Route::get('/{id}/edit', 'Admin\VeteranSelisih@edit')->middleware('AuthRole:'.getRoleMenu('veteranselisih.update'));
	Route::post('/export', 'Admin\VeteranSelisih@export')->name('veteranselisih.export')->middleware('AuthRole:'.getRoleMenu('veteranselisih.export'));
	Route::post('/import', 'Admin\VeteranSelisih@import')->name('veteranselisih.import')->middleware('AuthRole:'.getRoleMenu('veteranselisih.import'));
});

Route::group(['prefix' => 'peraturan'], function ()
{
	Route::get('/', 'Admin\Peraturan@index')->name('peraturan.index')->middleware('AuthRole:'.getRoleMenu('peraturan.index'));
	Route::get('/modal', 'Admin\Peraturan@modal')->name('peraturan.modal')->middleware('AuthRole:'.getRoleMenu('peraturan.index'));
	Route::post('/', 'Admin\Peraturan@store')->name('peraturan.store')->middleware('AuthRole:'.getRoleMenu('peraturan.create'));
	Route::post('/delete', 'Admin\Peraturan@delete')->name('peraturan.delete')->middleware('AuthRole:'.getRoleMenu('peraturan.destroy'));
	Route::get('/create', 'Admin\Peraturan@create')->name('peraturan.create')->middleware('AuthRole:'.getRoleMenu('peraturan.create'));
	Route::post('/create-folder', 'Admin\Peraturan@createFolder')->name('peraturan.createFolder')->middleware('AuthRole:'.getRoleMenu('peraturan.create'));
	Route::post('/upload-file', 'Admin\Peraturan@uploadFile')->name('peraturan.uploadFilePeraturan')->middleware('AuthRole:'.getRoleMenu('peraturan.uploadFilePeraturan'));
	Route::get('/list', 'Admin\Peraturan@listPeraturan')->name('peraturan.list')->middleware('AuthRole:'.getRoleMenu('peraturan.index'));
	Route::get('/{id}', 'Admin\Peraturan@show')->middleware('AuthRole:'.getRoleMenu('peraturan.show'));
	Route::put('/{id}', 'Admin\Peraturan@update')->middleware('AuthRole:'.getRoleMenu('peraturan.update'));
	Route::delete('/{id}', 'Admin\Peraturan@destroy')->middleware('AuthRole:'.getRoleMenu('peraturan.destroy'));
	Route::get('/{id}/edit', 'Admin\Peraturan@edit')->middleware('AuthRole:'.getRoleMenu('peraturan.update'));
});

Route::group(['prefix' => 'kritiksaran'], function ()
{
	Route::get('/', 'Admin\KritikSaran@index')->name('kritiksaran.index')->middleware('AuthRole:'.getRoleMenu('kritiksaran.index'));
	Route::post('/', 'Admin\KritikSaran@store')->name('kritiksaran.store')->middleware('AuthRole:'.getRoleMenu('kritiksaran.create'));
	Route::get('/create', 'Admin\KritikSaran@create')->name('kritiksaran.create')->middleware('AuthRole:'.getRoleMenu('kritiksaran.create'));
	Route::post('/list', 'Admin\KritikSaran@listData')->middleware('AuthRole:'.getRoleMenu('kritiksaran.index'));
	Route::get('/{id}', 'Admin\KritikSaran@show')->middleware('AuthRole:'.getRoleMenu('kritiksaran.show'));
	Route::put('/{id}', 'Admin\KritikSaran@update')->middleware('AuthRole:'.getRoleMenu('kritiksaran.update'));
	Route::delete('/{id}', 'Admin\KritikSaran@destroy')->middleware('AuthRole:'.getRoleMenu('kritiksaran.destroy'));
	Route::get('/{id}/edit', 'Admin\KritikSaran@edit')->middleware('AuthRole:'.getRoleMenu('kritiksaran.update'));
	Route::get('/{id}/show', 'Admin\KritikSaran@show')->middleware('AuthRole:'.getRoleMenu('kritiksaran.show'));
});

Route::group(['prefix' => 'media'], function ()
{
	Route::get('/', 'Admin\Media@index')->name('media.index')->middleware('AuthRole:'.getRoleMenu('media.index'));
	Route::get('/modal', 'Admin\Media@modal')->name('media.modal')->middleware('AuthRole:'.getRoleMenu('media.index'));
	Route::post('/', 'Admin\Media@store')->name('media.store')->middleware('AuthRole:'.getRoleMenu('media.create'));
	Route::post('/delete', 'Admin\Media@delete')->name('media.delete')->middleware('AuthRole:'.getRoleMenu('media.destroy'));
	Route::get('/create', 'Admin\Media@create')->name('media.create')->middleware('AuthRole:'.getRoleMenu('media.create'));
	Route::post('/create-folder', 'Admin\Media@createFolder')->name('media.createFolder')->middleware('AuthRole:'.getRoleMenu('media.create'));
	Route::post('/upload-file', 'Admin\Media@uploadFile')->name('media.uploadFileMedia')->middleware('AuthRole:'.getRoleMenu('media.uploadFileMedia'));
	Route::get('/list', 'Admin\Media@listMedia')->name('media.list')->middleware('AuthRole:'.getRoleMenu('media.index'));
	Route::get('/{id}', 'Admin\Media@show')->middleware('AuthRole:'.getRoleMenu('media.show'));
	Route::put('/{id}', 'Admin\Media@update')->middleware('AuthRole:'.getRoleMenu('media.update'));
	Route::delete('/{id}', 'Admin\Media@destroy')->middleware('AuthRole:'.getRoleMenu('media.destroy'));
	Route::get('/{id}/edit', 'Admin\Media@edit')->middleware('AuthRole:'.getRoleMenu('media.update'));
});

Route::group(['prefix' => 'kanminvetcad'], function ()
{
	Route::get('/', 'Admin\Kodim@index')->name('kanminvetcad.index')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.index'));
	Route::post('/', 'Admin\Kodim@store')->name('kanminvetcad.store')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.create'));
	Route::get('/create', 'Admin\Kodim@create')->name('kanminvetcad.create')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.create'));
	Route::post('/list', 'Admin\Kodim@listData')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.index'));
	Route::get('/{id}', 'Admin\Kodim@show')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.show'));
	Route::put('/{id}', 'Admin\Kodim@update')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.update'));
	Route::delete('/{id}', 'Admin\Kodim@destroy')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.destroy'));
	Route::get('/{id}/edit', 'Admin\Kodim@edit')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.update'));
	Route::post('/get', 'Admin\Kodim@getJson')->middleware('AuthRole:'.getRoleMenu('kanminvetcad.get'));
});

Auth::routes();

Route::group(['middleware' => 'auth'], function ()
{
	Route::group(['prefix' => 'babinminvetcaddam'], function ()
	{
		Route::get('/', 'Admin\Kodam@index')->name('babinminvetcaddam.index')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.index'));
		Route::post('/', 'Admin\Kodam@store')->name('babinminvetcaddam.store')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.create'));
		Route::get('/create', 'Admin\Kodam@create')->name('babinminvetcaddam.create')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.create'));
		Route::post('/list', 'Admin\Kodam@listData')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.index'));
		Route::get('/{id}', 'Admin\Kodam@show')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.show'));
		Route::put('/{id}', 'Admin\Kodam@update')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.update'));
		Route::delete('/{id}', 'Admin\Kodam@destroy')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.destroy'));
		Route::get('/{id}/edit', 'Admin\Kodam@edit')->middleware('AuthRole:'.getRoleMenu('babinminvetcaddam.update'));
	});

	Route::group(['prefix' => 'provinsi'], function ()
	{
		Route::get('/', 'Admin\Provinsi@index')->name('provinsi.index')->middleware('AuthRole:'.getRoleMenu('provinsi.index'));
		Route::post('/', 'Admin\Provinsi@store')->name('provinsi.store')->middleware('AuthRole:'.getRoleMenu('provinsi.create'));
		Route::get('/create', 'Admin\Provinsi@create')->name('provinsi.create')->middleware('AuthRole:'.getRoleMenu('provinsi.create'));
		Route::post('/list', 'Admin\Provinsi@listData')->middleware('AuthRole:'.getRoleMenu('provinsi.index'));
		Route::get('/{id}', 'Admin\Provinsi@show')->middleware('AuthRole:'.getRoleMenu('provinsi.show'));
		Route::put('/{id}', 'Admin\Provinsi@update')->middleware('AuthRole:'.getRoleMenu('provinsi.update'));
		Route::delete('/{id}', 'Admin\Provinsi@destroy')->middleware('AuthRole:'.getRoleMenu('provinsi.destroy'));
		Route::get('/{id}/edit', 'Admin\Provinsi@edit')->middleware('AuthRole:'.getRoleMenu('provinsi.update'));
		Route::post('/get', 'Admin\Provinsi@getJson')->middleware('AuthRole:'.getRoleMenu('provinsi.get'));
	});

	Route::group(['prefix' => 'kabupaten'], function ()
	{
		Route::get('/', 'Admin\Kabupaten@index')->name('kabupaten.index')->middleware('AuthRole:'.getRoleMenu('kabupaten.index'));
		Route::post('/', 'Admin\Kabupaten@store')->name('kabupaten.store')->middleware('AuthRole:'.getRoleMenu('kabupaten.create'));
		Route::get('/create', 'Admin\Kabupaten@create')->name('kabupaten.create')->middleware('AuthRole:'.getRoleMenu('kabupaten.create'));
		Route::post('/list', 'Admin\Kabupaten@listData')->middleware('AuthRole:'.getRoleMenu('kabupaten.index'));
		Route::get('/{id}', 'Admin\Kabupaten@show')->middleware('AuthRole:'.getRoleMenu('kabupaten.show'));
		Route::put('/{id}', 'Admin\Kabupaten@update')->middleware('AuthRole:'.getRoleMenu('kabupaten.update'));
		Route::delete('/{id}', 'Admin\Kabupaten@destroy')->middleware('AuthRole:'.getRoleMenu('kabupaten.destroy'));
		Route::get('/{id}/edit', 'Admin\Kabupaten@edit')->middleware('AuthRole:'.getRoleMenu('kabupaten.update'));
		Route::post('/get', 'Admin\Kabupaten@getJson')->middleware('AuthRole:'.getRoleMenu('kabupaten.get'));
	});

	Route::get('/map', 'Admin\Maps@index');

	Route::group(['prefix' => 'user'], function ()
	{
		Route::get('/', 'Admin\User@index')->name('user.index')->middleware('AuthRole:'.getRoleMenu('user.update'));
		Route::post('/', 'Admin\User@store')->name('user.store')->middleware('AuthRole:'.getRoleMenu('user.create'));
		Route::get('/create', 'Admin\User@create')->name('user.create')->middleware('AuthRole:'.getRoleMenu('user.create'));
		Route::post('/list', 'Admin\User@listData')->middleware('AuthRole:'.getRoleMenu('user.index'));
		Route::get('/{id}', 'Admin\User@show')->middleware('AuthRole:'.getRoleMenu('user.show'));
		Route::put('/{id}', 'Admin\User@update')->middleware('AuthRole:'.getRoleMenu('user.update'));
		Route::delete('/{id}', 'Admin\User@destroy')->middleware('AuthRole:'.getRoleMenu('user.destroy'));
		Route::get('/{id}/edit', 'Admin\User@edit')->middleware('AuthRole:'.getRoleMenu('user.update'));
	});

	Route::get('/setting', 'Admin\Setting@index')->name('setting.index')->middleware('AuthRole:'.getRoleMenu('setting.index'));
	Route::post('/setting', 'Admin\Setting@settingStore')->middleware('AuthRole:'.getRoleMenu('setting.update'));

	Route::group(['prefix' => 'manualbook'], function ()
	{
		Route::get('/', 'Admin\ManualBook@index')->name('manualbook.index')->middleware('AuthRole:'.getRoleMenu('manualbook.index'));
		Route::get('/modal', 'Admin\ManualBook@modal')->name('manualbook.modal')->middleware('AuthRole:'.getRoleMenu('manualbook.index'));
		Route::post('/', 'Admin\ManualBook@store')->name('manualbook.store')->middleware('AuthRole:'.getRoleMenu('manualbook.create'));
		Route::post('/delete', 'Admin\ManualBook@delete')->name('manualbook.delete')->middleware('AuthRole:'.getRoleMenu('manualbook.destroy'));
		Route::get('/create', 'Admin\ManualBook@create')->name('manualbook.create')->middleware('AuthRole:'.getRoleMenu('manualbook.create'));
		Route::post('/create-folder', 'Admin\ManualBook@createFolder')->name('manualbook.createFolder')->middleware('AuthRole:'.getRoleMenu('manualbook.create'));
		Route::post('/upload-file', 'Admin\ManualBook@uploadFile')->name('manualbook.uploadFileManualBook')->middleware('AuthRole:'.getRoleMenu('manualbook.uploadFileManualBook'));
		Route::get('/list', 'Admin\ManualBook@listManualBook')->name('manualbook.list')->middleware('AuthRole:'.getRoleMenu('manualbook.index'));
		Route::get('/{id}', 'Admin\ManualBook@show')->middleware('AuthRole:'.getRoleMenu('manualbook.show'));
		Route::put('/{id}', 'Admin\ManualBook@update')->middleware('AuthRole:'.getRoleMenu('manualbook.update'));
		Route::delete('/{id}', 'Admin\ManualBook@destroy')->middleware('AuthRole:'.getRoleMenu('manualbook.destroy'));
		Route::get('/{id}/edit', 'Admin\ManualBook@edit')->middleware('AuthRole:'.getRoleMenu('manualbook.update'));
	});

	Route::get('/edit-profile', 'Admin\Profile@profile')->name('myProfile');
	Route::post('/edit-profile', 'Admin\Profile@updateProfile');
	Route::put('/edit-profile/{id}', 'Admin\Profile@updateProfile');
});